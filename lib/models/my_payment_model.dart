import 'package:flutter/material.dart';
import 'package:infinite_health_care/models/doctor_model.dart';

class MyPaymentModel {
  int id_doctor;
  String doctor_name;
  String amount;
  String doctor_email;
  String doctor_mobile;
  List<dynamic> doctor_category;
  List<dynamic> doctor_disease;
  List<dynamic> doctor_degree;
  String payment_id;
  String appointment_date;
  String appointment_time;
  String doctor_image;
  String transaction_datetime;
  MyPaymentModel(
    this.id_doctor,
    this.doctor_name,
    this.amount,
    this.doctor_email,
    this.doctor_mobile,
    this.doctor_category,
    this.doctor_disease,
    this.doctor_degree,
    this.payment_id,
    this.appointment_date,
    this.appointment_time,
    this.doctor_image,
    this.transaction_datetime
  );

  MyPaymentModel.fromMap(Map<dynamic,dynamic> map):
      this.id_doctor = map['id_doctor'],
      this.doctor_name = map['doctor_name'],
      this.amount = map['amount'],
      this.doctor_email = map['doctor_email'],
      this.doctor_mobile = map['doctor_mobile'],
      this.doctor_category = map['doctor_category'],
      this.doctor_disease = map['doctor_disease'],
      this.doctor_degree = map['doctor_degree'],
      this.payment_id = map['payment_id'],
      this.appointment_date = map['appointment_date'],
      this.appointment_time = map['appointment_time'],
      this.doctor_image = map['doctor_image'],
      this.transaction_datetime = map['transaction_datetime']
  ;

}

