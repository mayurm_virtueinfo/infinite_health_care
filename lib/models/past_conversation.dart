
class PastConversation{
  int id_doctor ;
  int user_id ;
  String doctor_name ;
  String appointment_date ;
  String avatar_image ;
  String firebase_token;
  List<dynamic> appointment;

 PastConversation.fromMap(map):
       this.id_doctor = map['id_doctor'],
       this.user_id = map['user_id'],
       this.doctor_name = map['doctor_name'],
       this.appointment_date = map['appointment_date'],
       this.avatar_image = map['avatar_image'],
       this.firebase_token = map['doctor_firebase_token'],
  this.appointment = map['appointment'];

  Map<String,dynamic> toJson(PastConversation mConversation){
    Map<String,dynamic> map = Map();
    map['id_doctor'] = mConversation.id_doctor;
    map['user_id'] = mConversation.user_id;
    map['doctor_name'] = mConversation.doctor_name;
    map['appointment_date'] = mConversation.appointment_date;
    map['avatar_image'] = mConversation.avatar_image;
    map['doctor_firebase_token'] = mConversation.firebase_token;
    map['appointment'] = mConversation.appointment;
    return map;
  }

}

class PastConversationListModel{
  List<PastConversation> _conversationList = [];

  PastConversationListModel.fromSnapshot(snapshot) {
    List<dynamic> data = snapshot.data['data']['past_appointment'];/*['past_appointment'];*/

    data.forEach((item) {
      PastConversation appointment = PastConversation.fromMap(item);
      _conversationList.add(appointment);
    });
  }

  List<PastConversation> get pastConversation => _conversationList;

}