class SearchDoctorModel{
  int doctor_id;//": 5,
  String doctor_name;//": "DR.K M Patel",
  List<dynamic> category;//": [],
  List<dynamic> dieses;//": [],
  List<dynamic> degree;//": [],
  String description;//": "I am homeopathy doctor. I have 9 year experience in this field. ",
  String city;//": "Rajkot ",
  String address;//": "Gayatrinagar main road",
  String zipcode;//": "360002",
  String email;//": "KMpatel2875@gmail.com",
  String mobile;//": "7434012352",
  String rating;//": "4.5",
  String gtimings;//": "9:30 AM - 8:00 PM",
  String status;//": "1",
  String lat;//": "22.2756",
  String lng;//": "70.8053",
  String doctor_image;//": "http://work-updates.com/ihc/public/uploads/doctor/no_image.png"


  SearchDoctorModel.fromMap(Map<dynamic,dynamic> mapData):
      this.doctor_id = mapData['doctor_id'],
      this.doctor_name = mapData['doctor_name'],
      this.category = mapData['category'],
      this.dieses = mapData['dieses'],
      this.degree = mapData['degree'],
      this.description = mapData['description'],
      this.city = mapData['city'],
      this.address = mapData['address'],
      this.zipcode = mapData['zipcode'],
      this.email = mapData['email'],
      this.mobile = mapData['mobile'],
      this.rating = mapData['rating'],
      this.gtimings = mapData['gtimings'],
      this.status = mapData['status'],
      this.lat = mapData['lat'],
      this.lng = mapData['lng'],
      this.doctor_image = mapData['doctor_image'];


}