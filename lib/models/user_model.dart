import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/utils/app_preferences.dart';

class UserModel {
  String id;
  String first_name;
  String last_name;
  String email;
  String mobile;
  String password;
  String remember_token;
  String state;
  String city;
  String address;
  String zipcode;
  String avatar_image;
  String cover_image;
  String status;
  String created_at;
  String modified_at;
  String last_login;
  String ip;
  // String loginType;
  // bool isAuthenticated;

  UserModel(
      {this.id,
      // this.loginType,
      this.email,
      this.first_name,
      this.last_name,
      this.state,
      this.zipcode,
      this.address,
      this.avatar_image,
      this.city,
      this.cover_image,
      this.mobile,
      this.password,
      this.remember_token,
      this.status,
      this.created_at,
      this.modified_at,
      this.last_login,
      this.ip,
      // this.isAuthenticated
    });

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = Map<String, dynamic>();
    data['id'] = this.id;
    // data['loginType'] = this.loginType;
    data['first_name'] = this.first_name;
    data['last_name'] = this.last_name;
    data['email'] = this.email;
    data['mobile'] = this.mobile;
    data['password'] = this.password;
    data['remember_token'] = this.remember_token;
    data['state'] = this.state;
    data['city'] = this.city;
    data['address'] = this.address;
    data['zipcode'] = this.zipcode;
    data['avatar_image'] = this.avatar_image;
    data['cover_image'] = this.cover_image;
    data['status'] = this.status;
    data['created_at'] = this.created_at;
    data['modified_at'] = this.modified_at;
    data['last_login'] = this.last_login;
    data['ip'] = this.ip;
    // data['isAuthenticated'] = this.isAuthenticated;
    return data;
  }

  UserModel.fromMap(Map<String,dynamic> data) :
    this.id = (data['id'] is int)?"${data['id']}":"${data['id']}",
    // this.loginType = data['loginType'],
    this.first_name = data['first_name'],
    this.last_name = data['last_name'],
    this.email = data['email'],
    this.mobile = data['mobile'],
    this.password = data['password'],
    this.remember_token = data['remember_token'],
    this.state = data['state'],
    this.city = data['city'],
    this.address = data['address'],
    this.zipcode = data['zipcode'],
    this.avatar_image = data['avatar_image'],
    this.cover_image = data['cover_image'],
    this.status = data['status'],
    this.created_at = data['created_at'],
    this.modified_at = data['modified_at'],
    this.last_login = data['last_login'],
    this.ip = data['ip']
    // this.isAuthenticated = data['isAuthenticated']
  ;

  /*Future<void> saveUser({UserModel userModel}) async {
    await appPreference.setString(AppPreferences.user, jsonEncode(userModel.toJson()));
    return;
  }*/

  /*static Future<UserModel> user() async {
    String strUser = appPreference.getString(AppPreferences.user);
    if(strUser == null){
      UserModel mUser = UserModel(isAuthenticated: false);
      return Future.value(mUser);
    }
    Map user = jsonDecode(strUser);

    UserModel mUser ;
    debugPrint("check map : ${user.toString()}");
    if(user!=null) {
      if(user['isAuthenticated'] == null){
        user['isAuthenticated'] = false;
      }
      mUser = UserModel.fromMap(user);
    }else{
      user = {
        "isAuthenticated":true
      };
      mUser = UserModel(isAuthenticated: false);
    }
    return Future.value(mUser);
  }*/
}
