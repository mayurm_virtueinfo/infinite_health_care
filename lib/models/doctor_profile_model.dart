import 'package:flutter/cupertino.dart';
import 'package:infinite_health_care/models/past_conversation.dart';

class DoctorProfileModel{
  int id;
  String id_category;
  String id_disease;
  String id_degree;
  String title;
  String description;
  String city;
  String address;
  String zipcode;
  String email;
  String mobile;
  String rating;
  String gtimings;
  String avatar_image;
  String cover_image;
  String gstatus;
  String status;
  String created_at;
  String updated_at;
  int last_by;
  String last_ip;
  List album_images;
  int fees;
  List<dynamic> category_data;
  List<dynamic> dieses_data;
  List<dynamic> degree_data;
  String lat;
  String lng;
  String isVerified;
  String experience;
  String voting;
  List<dynamic> specialization;
  List<dynamic> review_detail;
  String open_today;

  DoctorProfileModel.fromPastConversation(PastConversation pastConversation){
    this.id = pastConversation.id_doctor;
    this.avatar_image = pastConversation.avatar_image;
    this.title = pastConversation.doctor_name;

  }

  DoctorProfileModel.fromMap(Map<String,dynamic> mData){
    this.id = mData['id'];
    this.id_category = mData['id_category'];
    this.id_disease = mData['id_disease'];
    this.id_degree = mData['id_degree'];
    this.title = mData['title'];
    this.description = mData['description'];
    this.city = mData['city'];
    this.address = mData['address'];
    this.zipcode = mData['zipcode'];
    this.email = mData['email'];
    this.mobile = mData['mobile'];
    this.rating = mData['rating'];
    this.gtimings = mData['gtimings'];
    this.avatar_image = mData['avatar_image'];
    this.cover_image = mData['cover_image'];
    this.gstatus = mData['gstatus'];
    this.status = mData['status'];
    this.created_at = mData['created_at'];
    this.updated_at = mData['updated_at'];
    this.last_by = mData['last_by'];
    this.last_ip = mData['last_ip'];
    this.album_images = mData['album_images'];
    this.fees = mData['fees'];
    this.category_data = mData['category_data'];
    this.dieses_data = mData['dieses_data'];
    this.degree_data = mData['degree_data'];
    this.lat = mData['lat'];
    this.lng = mData['lng'];
    this.isVerified = mData['isVerified'];
    this.experience = mData['experience'];
    this.voting = mData['voting'];
    this.specialization = mData['specialization'];
    this.review_detail = mData['review_detail'];
    this.open_today = mData['open_today'];
  }

  DoctorProfileModel.fromSnapshot(AsyncSnapshot snapshot){
    Map<String,dynamic> mData = snapshot.data['data'];
    this.id = mData['id'];
    this.id_category = mData['id_category'];
    this.id_disease = mData['id_disease'];
    this.id_degree = mData['id_degree'];
    this.title = mData['title'];
    this.description = mData['description'];
    this.city = mData['city'];
    this.address = mData['address'];
    this.zipcode = mData['zipcode'];
    this.email = mData['email'];
    this.mobile = mData['mobile'];
    this.rating = mData['rating'];
    this.gtimings = mData['gtimings'];
    this.avatar_image = mData['avatar_image'];
    this.cover_image = mData['cover_image'];
    this.gstatus = mData['gstatus'];
    this.status = mData['status'];
    this.created_at = mData['created_at'];
    this.updated_at = mData['updated_at'];
    this.last_by = mData['last_by'];
    this.last_ip = mData['last_ip'];
    this.album_images = mData['album_images'];
    this.fees = mData['fees'];
    this.category_data = mData['category_data'];
    this.dieses_data = mData['dieses_data'];
    this.degree_data = mData['degree_data'];
    this.lat = mData['lat'];
    this.lng = mData['lng'];
    this.isVerified = mData['isVerified'];
    this.experience = mData['experience'];
    this.voting = mData['voting'];
    this.specialization = mData['specialization'];
    this.review_detail = mData['review_detail'];
    this.open_today = mData['open_today'];
  }
}
