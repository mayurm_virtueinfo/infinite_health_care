

class ScheduledConversation{
  int id_doctor ;
  // int user_id ;
  String doctor_name ;
  String appointment_date ;
  String avatar_image ;
  String firebase_token;
  List<dynamic> appointment;

 ScheduledConversation.fromMap(map):
    this.id_doctor = map['id_doctor'],
    // this.user_id = map['user_id'],
    this.doctor_name = map['doctor_name'],
    this.appointment_date = map['appointment_date'],
    this.avatar_image = map['avatar_image'],
    this.firebase_token = map['doctor_firebase_token'],
    this.appointment = map['appointment']
  ;
  ScheduledConversation.fromMapDynamic(map):
        this.id_doctor = map['id_doctor']??null,
        // this.user_id = map['user_id']??null,
        this.doctor_name = map['doctor_name']??null,
        // this.appointment_date = map['appointment_date']??null,
        this.avatar_image = map['avatar_image']??null,
        this.firebase_token = map['firebase_token']??null;
        // this.appointment = map['appointment']??null;

 Map<String,dynamic> toJson(){
   return {
     'id_doctor':this.id_doctor,
     // 'user_id':this.user_id,
     'doctor_name':this.doctor_name,
     'appointment_date':this.appointment_date,
     'avatar_image':this.avatar_image,
     'doctor_firebase_token':this.firebase_token,
     'appointment':this.appointment,
   };
 }
}
class ScheduledConversationListModel{
  List<ScheduledConversation> _conversationList = [];

  ScheduledConversationListModel.fromSnapshot(snapshot) {
    List<dynamic> data = snapshot.data['data']['upcoming_appointment'];
//    data = mScheduledConversationList;
    data.forEach((item) {
      ScheduledConversation appointment = ScheduledConversation.fromMap(item);
      _conversationList.add(appointment);
    });
  }

  List<ScheduledConversation> get scheduledConversation => _conversationList;



}