import 'package:flutter/material.dart';
import 'package:infinite_health_care/pages/health.dart';
import 'package:infinite_health_care/splash_screen.dart';
import 'package:infinite_health_care/ui/account/my_appointments.dart';
import 'package:infinite_health_care/ui/account/my_doctor_notes.dart';
import 'package:infinite_health_care/ui/account/my_doctors.dart';
import 'package:infinite_health_care/ui/account/my_favorite_doctors.dart';
import 'package:infinite_health_care/ui/account/my_notes.dart';
import 'package:infinite_health_care/ui/account/my_payments.dart';
import 'package:infinite_health_care/ui/account/user_profile.dart';
import 'package:infinite_health_care/ui/conversation/personal_chat.dart';
import 'package:infinite_health_care/ui/home/all_timings_screen.dart';
import 'package:infinite_health_care/ui/home/consultancy_booked.dart';
import 'package:infinite_health_care/ui/home/credit_card_payment.dart';
import 'package:infinite_health_care/ui/home/doctor_album_images.dart';
import 'package:infinite_health_care/ui/home/doctor_book_first_step.dart';
import 'package:infinite_health_care/ui/home/doctor_book_second_step.dart';
import 'package:infinite_health_care/ui/home/doctor_profile.dart';
import 'package:infinite_health_care/ui/home/doctors_list.dart';
import 'package:infinite_health_care/ui/home/doctors_near_you.dart';
import 'package:infinite_health_care/ui/home/new_doctors.dart';
import 'package:infinite_health_care/ui/home/new_doctors_list.dart';
import 'package:infinite_health_care/ui/home/popular_doctors_list.dart';
import 'package:infinite_health_care/ui/home/search_doctors_list.dart';
import 'package:infinite_health_care/ui/home/send_feedback.dart';
import 'package:infinite_health_care/ui/home_screen.dart';
import 'package:infinite_health_care/ui/intro/intro_screen.dart';
import 'package:infinite_health_care/ui/loginorsignup/enter_valid_mobile_number.dart';
import 'package:infinite_health_care/ui/loginorsignup/patient_registration_screen.dart';
import 'package:infinite_health_care/ui/loginorsignup/signup.dart';
import 'package:infinite_health_care/ui/loginorsignup/verification_number.dart';

class RouteGenerator {
  // static String currentRoute;
  static Route<dynamic> generateRoute(RouteSettings settings) {
    // Getting arguments passed in while calling Navigator.pushNamed
    final args = settings.arguments;
    switch(settings.name){
      case SplashScreen.routeName :
        return MaterialPageRoute(builder: (_) => SplashScreen());
      case IntroScreen.routeName :
        return MaterialPageRoute(builder: (_) => IntroScreen());
      case SignUp.routeName :
        return MaterialPageRoute(builder: (_) => SignUp());
      case EnterValidMobileNumber.routeName:
        return MaterialPageRoute(builder: (_) => EnterValidMobileNumber());
      case VerificationNumber.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => VerificationNumber(mobileNumber: lstArgs[0],verificationId: lstArgs[1],forceResendingToken: lstArgs[2],));
      case HomeScreen.routeName:
        return MaterialPageRoute(builder: (_) => HomeScreen());
      case DoctorsList.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => DoctorsList(type: lstArgs[0],type_id: lstArgs[1],title: lstArgs[2],));
      case DoctorProfile.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => DoctorProfile(id_doctor: lstArgs[0],title: lstArgs[1],));
      case DoctorBookFirstStep.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => DoctorBookFirstStep(doctorProfileModel: lstArgs[0],));
      case DoctorBookSecondeStep.routeName:
        List<dynamic> lstArgs = args;
        debugPrint("arguments : ${lstArgs}");
        return MaterialPageRoute(builder: (_) => DoctorBookSecondeStep(doctorProfileModel: lstArgs[0],bookingDate: lstArgs[1],));
      case MyDoctors.routeName:
        return MaterialPageRoute(builder: (_) => MyDoctors());
      case MyFavoriteDoctors.routeName:
        return MaterialPageRoute(builder: (_) => MyFavoriteDoctors());
      case MyAppointments.routeName:
        return MaterialPageRoute(builder: (_) => MyAppointments());
      case MyPayments.routeName:
        return MaterialPageRoute(builder: (_) => MyPayments());
      case HealthTips.routeName:
        return MaterialPageRoute(builder: (_) => HealthTips());
      case UserProfile.routeName:
        return MaterialPageRoute(builder: (_) => UserProfile());
      case CreditCardPayment.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => CreditCardPayment(doctorProfileModel: lstArgs[0],));
      case ConsultancyBooked.routeName:
        return MaterialPageRoute(builder: (_) => ConsultancyBooked());
      case SearchDoctorsList.routeName:
        return MaterialPageRoute(builder: (_) => SearchDoctorsList());
      case PatientRegistrationScreen.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => PatientRegistrationScreen(user: lstArgs[0],),);
      case PersonalChat.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => PersonalChat(peerId: lstArgs[0],appointmentType: lstArgs[1],doctor: lstArgs[2],));
      case DoctorsNearYou.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => DoctorsNearYou(latitude: lstArgs[0],longitude: lstArgs[1],));
      case NewDoctors.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => NewDoctors(latitude: lstArgs[0],longitude: lstArgs[1],));
      case DoctorAlbumImages.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => DoctorAlbumImages(index: lstArgs[0],album_images: lstArgs[1],));
      case SendFeedback.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => SendFeedback(id_doctor: lstArgs[0],));
      case NewDoctorsList.routeName:
        return MaterialPageRoute(builder: (_) => NewDoctorsList());
      case PopularDoctorsList.routeName:
        return MaterialPageRoute(builder: (_) => PopularDoctorsList());
      case MyNotes.routeName:
        return MaterialPageRoute(builder: (_) => MyNotes());
      case MyDoctorNote.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => MyDoctorNote(patient_id:lstArgs[0],doctor_id:lstArgs[1] ,));
      case AllTimingsScreen.routeName:
        List<dynamic> lstArgs = args;
        return MaterialPageRoute(builder: (_) => AllTimingsScreen(doctor_id:lstArgs[0],));

      default:
        // If there is no such named route in the switch statement, e.g. /third
        return _errorRoute();
    }
  }
    static Route<dynamic> _errorRoute() {
    return MaterialPageRoute(builder: (_) {
      return Scaffold(
        appBar: AppBar(
          title: Text('Error'),
        ),
        body: Center(
          child: Text('ERROR'),
        ),
      );
    });
  }


}