class ConstUser{

  //id
  //first_name
  //last_name
  //email
  //mobile
  //password
  //remember_token
  //state
  //city
  //address
  //zipcode
  //avatar_image
  //cover_image
  //status
  //created_at
  //modified_at
  //last_login
  //ip
  static const String userId = "userId";
  static const String email = "email";
  static const String loginType = "loginType";
  static const String firstname = "firstname";
  static const String lastname = "lastname";
  static const String mobileNumber = "mobileNumber";
  static const String address = "address";
  static const String city = "city";
  static const String state = "state";
  static const String zipcode = "zipcode";
  static const String country = "country";
  static const String image = "image";
  static const String coverImage = "coverImage";
  static const String totalOrder = "totalOrder";
  static const String status = "status";
  static const String createdAt = "createdAt";
  static const String modifiedAt = "modifiedAt";
  static const String lastLogin = "lastLogin";
  static const String lastLoginIp = "lastLoginIp";
}
