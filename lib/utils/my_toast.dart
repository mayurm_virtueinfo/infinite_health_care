import 'package:flutter/material.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:toast/toast.dart';
class MyToast{
  /*static showToast(String message){
    Toast.show(
        msg: message,
        toastLength: Toast.LENGTH_SHORT,
        gravity: ToastGravity.BOTTOM,
        timeInSecForIosWeb: 1,
        backgroundColor: config.Colors().mainColor(1),
        textColor: Colors.white,
        fontSize: 16.0
    );*/

    static showToast(BuildContext context,String message,{int length}){
      Toast.show(message, context, duration: length==null?Toast.LENGTH_SHORT:Toast.LENGTH_LONG, gravity:  Toast.BOTTOM,backgroundColor: config.Colors().mainColor(1),textColor: Colors.white,);
    }


}