import 'dart:convert';
import 'dart:io';

// import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:device_info/device_info.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:geolocator/geolocator.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:image_cropper/image_cropper.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/models/appointment_type.dart';
import 'package:infinite_health_care/models/schedule_conversation.dart';
import 'package:infinite_health_care/service/navigation_service.dart';
import 'package:infinite_health_care/ui/conversation/personal_chat.dart';
import 'package:infinite_health_care/utils/app_preferences.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:intl/intl.dart';
import 'package:location/location.dart';

class Utility{
  static void checkForNotification(Map<String,dynamic> data){
    debugPrint("checkForNotification : ${data.toString()}");
    String screen = data['screen'];
    print("inside $screen");

    if (screen == 'PersonalChat') {
      try {
        print("inside personalchat1");
        String appointmentType = data['appointmentType'];
        print("inside personalchat2");
        AppointmentType apType;
        if (appointmentType == AppointmentType.SCHEDULED_APPOINTMENT.toString()) {
          print("inside personalchat3");
          apType = AppointmentType.SCHEDULED_APPOINTMENT;
        } else {
          print("inside personalchat4");
          apType = AppointmentType.PAST_APPOINTMENT;
        }
        print("inside personalchat5");
        String userId = data['id_doctor'];
        print("inside personalchat6");
        Map<String, dynamic> patient = json.decode(data['patient']);
        if (patient is String) {
          debugPrint("patient is string");
        }
        print("inside personalchat7 : $patient");
        String patientId = patient['id_doctor'].toString();
        print("inside personalchat patient id : $patientId");
        if (patient['id_doctor'] is String) {
          String pId = patient['id_doctor'];
          patient['id_doctor'] = int.parse(pId);
        }

        ScheduledConversation scheduledConversation = ScheduledConversation.fromMapDynamic(patient);
        print("inside personalchat8");
        debugPrint("isChatScreenOpen : $isChatScreenOpen");
        // debugPrint("Current Route : ${ModalRoute.of(locator<NavigationService>().navigatorKey.currentState.context).settings.name}");
        if(isChatScreenOpen){
          locator<NavigationService>().navigatorKey.currentState.pushReplacementNamed(PersonalChat.routeName, arguments: [userId, apType, scheduledConversation]);
        }else {
          locator<NavigationService>().navigatorKey.currentState.pushNamed(PersonalChat.routeName, arguments: [userId, apType, scheduledConversation]);
        }
      } catch (e) {
        debugPrint("parsing error : ${e.toString()}");
      }
      // S
    }
  }
  static Future<String> getDeviceUUID() async {
    String deviceName;
    String deviceVersion;
    String identifier;
    final DeviceInfoPlugin deviceInfoPlugin = new DeviceInfoPlugin();
    try {
      if (Platform.isAndroid) {
        var build = await deviceInfoPlugin.androidInfo;
        deviceName = build.model;
        deviceVersion = build.version.toString();
        identifier = build.androidId;  //UUID for Android
      } else if (Platform.isIOS) {
        var data = await deviceInfoPlugin.iosInfo;
        deviceName = data.name;
        deviceVersion = data.systemVersion;
        identifier = data.identifierForVendor;  //UUID for iOS
      }
    } on PlatformException {
      throw  Exception('Failed to get platform version');
    }

//if (!mounted) return;
    return identifier;
  }
  static Future<Position> requestForLocationPermission(context) async {
    bool serviceEnabled;
    LocationPermission permission;
    Location location = new Location();
    serviceEnabled = await Geolocator.isLocationServiceEnabled();
    if (!serviceEnabled) {
      serviceEnabled = await location.requestService();
      if (!serviceEnabled) {
        return Future.error('Location services are disabled.');
      }

    }

    permission = await Geolocator.checkPermission();
    if (permission == LocationPermission.deniedForever) {
      return Future.error(
          'Location permissions are permantly denied, we cannot request permissions.');
    }

    if (permission == LocationPermission.denied) {
      permission = await Geolocator.requestPermission();
      if (permission != LocationPermission.whileInUse &&
          permission != LocationPermission.always) {
        return Future.error(
            'Location permissions are denied (actual value: $permission).');
      }
    }

    return await Geolocator.getCurrentPosition();
  }
  static double getDevicePixelRatio(BuildContext context){
    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    debugPrint("devicePixelRation original : $devicePixelRatio");
    if(devicePixelRatio>2){
      devicePixelRatio = 1.75;
    }
    debugPrint("devicePixelRation new : $devicePixelRatio");
    return devicePixelRatio;
  }
  static double getCoverHeight(BuildContext context){
    return Utility.getDevicePixelRatio(context) * 140;
  }
  static double getProfilePicSize(BuildContext context){
    return Utility.getDevicePixelRatio(context) * 75;
  }
  static String getFormatedCurrentTime(){
    DateFormat format= DateFormat("yyyy-MM-dd HH:mm:ss");
    String formatedCurrentTime = format.format(DateTime.now());
    debugPrint("formattedCurrentTime : ${formatedCurrentTime}");
    return formatedCurrentTime;
  }

  static String getTodayDate(DateTime dateTime ){
    DateFormat format= DateFormat("d MMM");
    String formatedCurrentTime = format.format(dateTime);
    debugPrint("getTodayDate : ${formatedCurrentTime}");
    return formatedCurrentTime;
  }

  static String getBookingDate(DateTime dateTime ){
    DateFormat format= DateFormat("yyyy-MM-dd");
    String formatedCurrentTime = format.format(dateTime);
    debugPrint("getTodayDate : ${formatedCurrentTime}");
    return formatedCurrentTime;
  }
  static String getFormatDate(DateTime dateTime ){
    DateFormat format= DateFormat("dd-MM-yyyy hh:mm a");
    String formatedCurrentTime = format.format(dateTime);
    debugPrint("getTodayDate : ${formatedCurrentTime}");
    return formatedCurrentTime;
  }
  static String getBookingTime(DateTime dateTime ){
    DateFormat format= DateFormat("HH:mm:ss");
    String formatedCurrentTime = format.format(dateTime);
    debugPrint("getTodayDate : ${formatedCurrentTime}");
    return formatedCurrentTime;
  }
  static String getFormatFromString(String date,String time){
    DateFormat parseFormat= DateFormat("dd-MMM-yyyy HH:mm:ss");
    DateTime  dtTime = parseFormat.parse('$date $time');

    DateFormat showFormat= DateFormat("dd-MMM-yyyy hh:mm a");
    return showFormat.format(dtTime);
  }
  static String mobileNumber = "7779044446";
  static List<String> recipents = [mobileNumber];

  /*static void sendMessage(String message) async {
    String _result = await sendSMS(message: message, recipients: recipents)
        .catchError((onError) {
      print(onError);
    });
    print(_result);
  }*/

  /*static Future<String> uploadFile(imageFile, storePath,chield_path) async {
    Reference storageReference =
    FirebaseStorage.instance.ref().child(chield_path).child(storePath);
    UploadTask uploadTask = storageReference.putFile(imageFile);
    await uploadTask.snapshot;
    return await storageReference.getDownloadURL();
  }*/

  static Future<File> cropImage(image) async {
    File croppedFile = await ImageCropper.cropImage(
        sourcePath: image.path,
        aspectRatioPresets: Platform.isAndroid
            ? [
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio16x9
        ]
            : [
          CropAspectRatioPreset.original,
          CropAspectRatioPreset.square,
          CropAspectRatioPreset.ratio3x2,
          CropAspectRatioPreset.ratio4x3,
          CropAspectRatioPreset.ratio5x3,
          CropAspectRatioPreset.ratio5x4,
          CropAspectRatioPreset.ratio7x5,
          CropAspectRatioPreset.ratio16x9
        ],
        androidUiSettings: AndroidUiSettings(
            toolbarTitle: 'Cropper',
            toolbarColor: Colors.deepOrange,
            toolbarWidgetColor: Colors.white,
            initAspectRatio: CropAspectRatioPreset.original,
            lockAspectRatio: false),
        iosUiSettings: IOSUiSettings(
          title: 'Cropper',
        ));
    if (croppedFile != null) {
      image = croppedFile;

    }
    return image;
  }

  static String getProfileString(address,city,state,zipCode){
    /*var buffer = new StringBuffer();
    buffer.write(address);
    buffer.write(city);
    buffer.write(state);
    buffer.write(zipcode);

    return buffer.toString().split('').join(',');*/
    List<String> listString = [];
    debugPrint('address $address');
    if((address != null) && (address != '') && (address != 'null')){
      listString.add(address);
    }
    if((city != null) && (city != '') && (city != 'null')){
      listString.add(city);
    }
    if((state != null) && (state != '') && (state != 'null')){
      listString.add(state);
    }
    if((zipCode != null) && (zipCode != '') && (zipCode != 'null')){
      listString.add(zipCode);
    }
    String profile = listString.join(', ');
    debugPrint('profile $profile');
    return profile;
//    return strProfile;
  }
  static Future<bool> logoutUser() async {
    await FirebaseAuth.instance.signOut();
    await FacebookLogin().logOut();
    await GoogleSignIn().signOut();
    return Future.value(true);
  }
  static Future<bool> isUserSignInWithMobile() async {
    User fUser = FirebaseAuth.instance.currentUser;
    bool isMobileSignIn = false;
    if (fUser != null && fUser.phoneNumber != null) {
      isMobileSignIn = true;
    }
    return Future.value(isMobileSignIn);
  }
  static Future<bool> isUserSignInWithGmail() async {
    GoogleSignIn googleSignIn = GoogleSignIn();
    bool isGoogleSignIn = await googleSignIn.isSignedIn();
    return Future.value(isGoogleSignIn);
  }
  static Future<bool> isUserSignInWithFacebook() async {

    FacebookLogin facebookSignIn = FacebookLogin();
    bool isFacebookSignIn = await facebookSignIn.isLoggedIn;

    return Future.value(isFacebookSignIn);

  }
}