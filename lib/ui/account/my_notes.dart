import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/models/appointment_type.dart';
import 'package:infinite_health_care/models/my_doctor_model.dart' as myDoctorModel;
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/ui/account/my_doctor_notes.dart';
import 'package:infinite_health_care/ui/home_screen.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/conversation_item_widget.dart';
import 'package:infinite_health_care/widgets/my_doctors_card_widget.dart';
class MyNotes extends StatefulWidget {
  static const routeName = '/MyNotesList';
  @override
  _MyNotesState createState() => _MyNotesState();
}

class _MyNotesState extends State<MyNotes> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Theme.of(context).primaryColor,
          ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop();
          },

        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Notes',
          style: TextStyle(
            fontSize:22.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child:ConstrainedBox(
              constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                         color: Colors.transparent,
                        ),
                        child: FutureBuilder(
                          future: ApiRequest.getNoteListing(patient_id: appUserModel.id),
                          builder: (context, snapshot) {

                            if(snapshot.hasData) {
                              locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                              List<dynamic> lstNotes = snapshot.data['data'];

                              debugPrint("lstNotes : ${lstNotes}");
                              if(lstNotes.length>0){
                                return ListView.separated(
                                  shrinkWrap: true,
                                  primary: false,
                                  itemCount: lstNotes.length,
                                  separatorBuilder: (context, index) {
                                    return SizedBox(height: 4.0);
                                  },
                                  itemBuilder: (context, index) {
                                    Map<dynamic,dynamic> mapNoteData =lstNotes[index];
                                    debugPrint("mapNoteData : ${mapNoteData.toString()}");
                                    return GestureDetector(
                                      onTap: (){
                                        debugPrint("mapNoteData : ${mapNoteData.toString()}");
                                        // Navigator.of(context).pushNamed(MyDoctorNote.routeName,arguments: ['${appUserModel.id}','${mapNoteData['id_doctor']}']);
                                      },
                                      child: Card(
                                        elevation: 1,
                                        // color: MyColors.colorConvert(e.value.color),
                                        shape: RoundedRectangleBorder(
                                            borderRadius: BorderRadius.circular(16)),
                                        child: Container(
                                          // padding: EdgeInsets.all(12),
                                          child: Row(
                                            crossAxisAlignment: CrossAxisAlignment.start,
                                            children: [
                                              Expanded(
                                                flex:2,
                                                child: Container(
                                                  alignment: Alignment.center,
                                                  child: Container(
                                                    margin: EdgeInsets.only(top: 20,bottom: 20),
                                                    width: 80,
                                                    height: 80,
                                                    child: ClipOval(
                                                      child: FancyShimmerImage(
                                                        imageUrl: mapNoteData['noteImagePath']??Const.defaultProfileUrl,
                                                        shimmerBaseColor: Colors.white,
                                                        shimmerHighlightColor: config.Colors().mainColor(1),
                                                        shimmerBackColor: Colors.green,
                                                        errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                                        boxFit: BoxFit.cover,
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                              Expanded(
                                                  flex: 4,
                                                  child: Container(
                                                    padding: EdgeInsets.only(top: 10,bottom: 10),
                                                    child: Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Text(mapNoteData['note_title']??'',style: TextStyle(
                                                          fontSize: 12.0,

                                                          fontWeight: FontWeight.bold,
                                                          color: Colors.black,
                                                        ),),

                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Text(mapNoteData['note'],style: TextStyle(
                                                            fontSize: 10.0,

                                                            color: Colors.grey,
                                                            fontStyle: FontStyle.italic
                                                        ),),
                                                        SizedBox(
                                                          height: 10,
                                                        ),
                                                        Text('By Dr. ${mapNoteData['doctor_name']}',style: TextStyle(
                                                            fontSize: 10.0,

                                                            color: Colors.black,
                                                            fontStyle: FontStyle.normal,
                                                            fontWeight: FontWeight.bold
                                                        ),),
                                                      ],
                                                    ),
                                                  )
                                              )
                                            ],
                                          ),
                                        ),
                                      ),
                                    );
                                  },
                                );
                              }else{
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: MediaQuery.of(context).size.height,
                                  child: Center(
                                    child: Text('No notes found',style: TextStyle(
                                        fontSize: 18
                                    ),),
                                  ),
                                );
                              }
                            } else if(snapshot.hasError){
                              locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.height,
                                child: Center(
                                  child: Text('No notes found',style: TextStyle(
                                      fontSize: 18
                                  ),),
                                ),
                              );
                            } else{
                              locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                              return Container();
                            }
                          }
                          ,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          LoadingWidget()
        ],
      ),
    );
  }
  
}
