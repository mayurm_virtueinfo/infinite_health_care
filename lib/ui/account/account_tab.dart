import 'dart:convert';

import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:infinite_health_care/bloc/user_model_bloc.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/pages/health.dart';
import 'package:infinite_health_care/ui/account/my_appointments.dart';
import 'package:infinite_health_care/ui/account/my_doctors.dart';
import 'package:infinite_health_care/ui/account/my_favorite_doctors.dart';
import 'package:infinite_health_care/ui/account/my_notes.dart';
import 'package:infinite_health_care/ui/account/my_payments.dart';
import 'package:infinite_health_care/ui/account/user_profile.dart';
import 'package:infinite_health_care/ui/loginorsignup/signup.dart';

import 'package:infinite_health_care/utils/app_preferences.dart';
import 'package:infinite_health_care/utils/dialog_mgr.dart';
import 'package:infinite_health_care/utils/utility.dart';

class AccountTab extends StatefulWidget {
  const AccountTab({Key key}) : super(key: key);

  @override
  _AccountTabState createState() => _AccountTabState();
}

class _AccountTabState extends State<AccountTab> {
  @override
  void initState() {
    debugPrint("UserID : ${appUserModel.id}");
    super.initState();
  }

  @override
  Widget build(BuildContext context) {

    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight = MediaQuery.of(context).size.height;
    double headerSize = (deviceHeight / 3.75);
    double devicePixelRatio = Utility.getDevicePixelRatio(context);

    double coverHeight = Utility.getCoverHeight(context);
    double profilePicSize = Utility.getProfilePicSize(context);
    double topPadding = coverHeight - (profilePicSize / 2);
    return Scaffold(
        body: SingleChildScrollView(
      child: Column(
        children: <Widget>[
          Stack(
            children: [
              StreamBuilder<UserModel>(
                  initialData: appUserModel,
                  stream: locator<UserModelBloc>().userModelStream,
                  builder: (context, AsyncSnapshot<UserModel> snapshot) {
                    UserModel nUserModel = snapshot.data;
                  return Container(
                    constraints: BoxConstraints(
                      minWidth: MediaQuery.of(context).size.width,
                      minHeight: coverHeight,
                    ),
                    child: Stack(
                      alignment: Alignment.bottomRight,
                      children: [
                        Container(
                          height: coverHeight,
                          width: MediaQuery.of(context).size.width,
                          child: FancyShimmerImage(
                            imageUrl: nUserModel.cover_image ?? Const.defaultCoverUrl,
                            shimmerBaseColor: Colors.white,
                            shimmerHighlightColor: config.Colors().mainColor(1),
                            shimmerBackColor: Colors.green,
                            boxFit: BoxFit.cover,
                          ),
                        ),
                      ],
                    ),
                  );
                }
              ),
              StreamBuilder<UserModel>(
                initialData: appUserModel,
                stream: locator<UserModelBloc>().userModelStream,
                builder: (context, AsyncSnapshot<UserModel> snapshot) {
                  UserModel nUserModel = snapshot.data;
                  return Padding(
                    padding: EdgeInsets.only(top: topPadding, left: 10, right: 10),
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        ball(nUserModel.avatar_image, Colors.transparent, profilePicSize, context),
                        SizedBox(
                          height: 10,
                        ),
                        Column(
                          children: <Widget>[
                            Text(
                              "${nUserModel.first_name} ${nUserModel.last_name}",
                              style: TextStyle(color: Theme.of(context).accentColor,  fontSize: 14, fontWeight: FontWeight.bold),
                            ),

                            Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                Utility.getProfileString(nUserModel.address, nUserModel.city, nUserModel.state, nUserModel.zipcode) == ''
                                    ? Container()
                                    : Icon(
                                        Icons.location_on,
                                        color: Theme.of(context).accentColor,
                                        size: 18,
                                      ),
                                SizedBox(
                                  width: 5,
                                ),
                                Text(Utility.getProfileString(nUserModel.address, nUserModel.city, nUserModel.state, nUserModel.zipcode))
                                /*Text(
                                      "${currentUser.address??""}, ${currentUser
                                          .city??""}, ${currentUser.state??""}, ${currentUser.zipcode??""}",
                                      style: TextStyle(
                                          color: Theme
                                              .of(context)
                                              .accentColor,

                                          fontWeight: FontWeight.bold),
                                    )*/
                                ,
                              ],
                            ),
                          ],
                        ),
                        Container(
                          width: 170,
                          // height: 30,
                          decoration: BoxDecoration(
                            border: Border.all(width: 2, color: Theme.of(context).primaryColor),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                          child: ElevatedButton(
                            style: ElevatedButton.styleFrom(
                              primary: Theme.of(context).accentColor,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30.0),
                              ),
                            ),

                            onPressed: () {
                              /*Navigator.pushNamed(
                                    context, UserProfile.routeName);*/
                              Navigator.of(context).push(
                                PageRouteBuilder(
                                  transitionDuration: Duration(milliseconds: 500),
                                  pageBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation) {
                                    return UserProfile();
                                  },
                                  transitionsBuilder: (BuildContext context, Animation<double> animation, Animation<double> secondaryAnimation, Widget child) {
                                    return Align(
                                      child: FadeTransition(
                                        opacity: animation,
                                        child: child,
                                      ),
                                    );
                                  },
                                ),
                              );
                            },

                            child: Container(
                              child: Center(
                                child: Text(
                                  'Edit your profile',
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Theme.of(context).primaryColor,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                      ],
                    ),
                  );
                }
              ),
            ],
          ),
          Container(
            padding: EdgeInsets.all(12.0),
            margin: EdgeInsets.all(12.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(16.0),
              border: Border.all(width: 1, color: Colors.grey.withOpacity(0.2)),
              color: Theme.of(context).primaryColor,
            ),
            child: Column(
              children: <Widget>[
                _dropDownListe(
                    Icon(
                      Icons.bubble_chart,
                      color: Theme.of(context).accentColor,
                    ),
                    'My Doctors',
                    1, () {
                  Navigator.of(context).pushNamed(MyDoctors.routeName);
                }),
                /*_dropDownListe(
                    Icon(
                      Icons.favorite,
                      color: Theme.of(context).accentColor,
                    ),
                    'My Favorite Doctors',
                    1,
                    MyFavoriteDoctors.routeName,
                    context, () {
                  Navigator.of(context).pushNamed(MyFavoriteDoctors.routeName);
                }),*/

                _dropDownListe(
                    Icon(
                      Icons.calendar_today,
                      color: Theme.of(context).accentColor,
                    ),
                    'Appointments',
                    1, () {
                  Navigator.of(context).pushNamed(MyAppointments.routeName);
                }),
                _dropDownListe(
                    Icon(
                      Icons.favorite,
                      color: Theme.of(context).accentColor,
                    ),
                    'My Notes',
                    1, () {
                  Navigator.of(context).pushNamed(MyNotes.routeName);
                }),
                /* _dropDownListe(
                    Icon(
                      Icons.card_giftcard,
                      color: Theme.of(context).accentColor,
                    ),
                    'Health Interest',
                    1,
                    context, () {
                  Navigator.of(context).pushNamed(HealthTips.routeName);
                }),*/
                _dropDownListe(
                    Icon(
                      Icons.payment,
                      color: Theme.of(context).accentColor,
                    ),
                    'My Payments',
                    1,
                    () {
                      Navigator.of(context).pushNamed(MyPayments.routeName);
                    }),
                /*_dropDownListe(
                    Icon(
                      Icons.local_offer,
                      color: Theme.of(context).accentColor,
                    ),
                    'Offers',
                    1,
                    context,
                    () {
                      // OffersList.routeName
                    }),*/
                _dropDownListe(
                    Icon(
                      Icons.arrow_upward,
                      color: Theme.of(context).accentColor,
                    ),
                    'Logout',
                    0, () async {
                  String result = await DialogMgr.showDialogLogout(context);
                  if(result != null && result =='yes'){
                    bool result = await Utility.logoutUser();
                    if (result) {
                      Navigator.of(context).pushNamedAndRemoveUntil(SignUp.routeName, (route) => false);
                    }
                  }

                }),
              ],
            ),
          ),
        ],
      ),
    ));
  }



  Widget _dropDownListe(Icon icon, String title, double borderWidth, Function onClick) {
    return Container(
      decoration: BoxDecoration(
        border: Border(bottom: BorderSide(width: borderWidth, color: Colors.grey.withOpacity(0.2))),
      ),
      child: TextButton(
        onPressed: onClick,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Row(
              children: <Widget>[
                Container(
                  margin: const EdgeInsets.only(right: 25.0),
                  child: icon,
                ),
                Container(
                  child: Text(
                    title,
                    style: TextStyle(
                      color: Colors.grey,

                      fontSize: 16.0,
                    ),
                  ),
                ),
              ],
            ),
            Container(
              child: Icon(
                Icons.chevron_right,
                color: Colors.grey,
                size: 20,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

Widget ball(String image, Color color, profileSize, context) {
  return Container(
    alignment: Alignment.center,
    height: profileSize,
    width: profileSize,
    decoration: BoxDecoration(color: Theme.of(context).accentColor, shape: BoxShape.circle),
    child: Container(
      height: profileSize - 2,
      width: profileSize - 2,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image ?? Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
        ),
      ),
    ),
  );
}
