import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/models/appointment_type.dart';
import 'package:infinite_health_care/models/my_doctor_model.dart' as myDoctorModel;
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/ui/home_screen.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/conversation_item_widget.dart';
import 'package:infinite_health_care/widgets/my_doctors_card_widget.dart';
class MyDoctorNote extends StatefulWidget {
  static const routeName = '/MyDoctorNoteList';
  String patient_id,doctor_id;
  MyDoctorNote({this.patient_id,this.doctor_id});
  @override
  _MyDoctorNoteState createState() => _MyDoctorNoteState();
}

class _MyDoctorNoteState extends State<MyDoctorNote> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Theme.of(context).primaryColor,
          ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop();
          },

        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Notes',
          style: TextStyle(
            fontSize:22.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child:ConstrainedBox(
          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                     color: Colors.transparent,
                    ),
                    child: FutureBuilder(
                      future: ApiRequest.getDoctorNoteListing(patient_id: widget.patient_id,doctor_id: widget.doctor_id),
                      builder: (context, snapshot) {

                        if(snapshot.hasData) {

                          List<dynamic> lstNotes = snapshot.data['data'];

                          debugPrint("lstNotes : ${lstNotes}");
                          return ListView.separated(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: lstNotes.length,
                            separatorBuilder: (context, index) {
                              return SizedBox(height: 4.0);
                            },
                            itemBuilder: (context, index) {
                              Map<dynamic,dynamic> mapNoteData =lstNotes[index];
                              debugPrint("mapNoteData : ${mapNoteData.toString()}");
                              return Card(

                                elevation: 1,
                                // color: MyColors.colorConvert(e.value.color),
                                shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(16)),
                                child: Container(
                                  // padding: EdgeInsets.all(12),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: [
                                      Expanded(
                                        flex:2,
                                        child: Container(
                                          alignment: Alignment.center,
                                          child: Container(
                                            margin: EdgeInsets.only(top: 20,bottom: 20),
                                            width: 80,
                                            height: 80,
                                            child: ClipOval(
                                              child: FancyShimmerImage(
                                                imageUrl: mapNoteData['noteImagePath']??Const.defaultProfileUrl,
                                                shimmerBaseColor: Colors.white,
                                                shimmerHighlightColor: config.Colors().mainColor(1),
                                                shimmerBackColor: Colors.green,
                                                errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                                boxFit: BoxFit.cover,
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                      Expanded(
                                          flex: 4,
                                          child: Container(
                                            padding: EdgeInsets.only(top: 10,bottom: 10),
                                            child: Column(
                                              crossAxisAlignment: CrossAxisAlignment.start,
                                              children: [
                                                Text(mapNoteData['note_title']??'',style: TextStyle(
                                                  fontSize: 12.0,

                                                  fontWeight: FontWeight.bold,
                                                  color: Colors.black,
                                                ),),

                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text(mapNoteData['note'],style: TextStyle(
                                                    fontSize: 10.0,

                                                    color: Colors.grey,
                                                    fontStyle: FontStyle.italic
                                                ),),
                                                SizedBox(
                                                  height: 10,
                                                ),
                                                Text('By Dr. ${mapNoteData['doctor_name']}',style: TextStyle(
                                                    fontSize: 10.0,

                                                    color: Colors.black,
                                                    fontStyle: FontStyle.normal,
                                                    fontWeight: FontWeight.bold
                                                ),),
                                              ],
                                            ),
                                          )
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          );
                        } else if(snapshot.hasError){
                          MyToast.showToast(context,snapshot.error.toString());
                          return Container();
                        } else{
                          return Center(
                            child:CircularProgressIndicator() ,
                          );
                        }
                      }
                      ,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),      
      ),
    );
  }
  
}
