import 'package:flutter/material.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/models/appointment_type.dart';
import 'package:infinite_health_care/models/my_doctor_model.dart' as myDoctorModel;
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/ui/home_screen.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/conversation_item_widget.dart';
import 'package:infinite_health_care/widgets/my_doctors_card_widget.dart';
class MyFavoriteDoctors extends StatefulWidget {
  static const routeName = '/MyFavoriteDoctors';
  @override
  _MyFavoriteDoctorsState createState() => _MyFavoriteDoctorsState();
}

class _MyFavoriteDoctorsState extends State<MyFavoriteDoctors> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Theme.of(context).primaryColor,
          ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop();
          },

        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'My Favorite Doctors',
          style: TextStyle(
            fontSize:22.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child:ConstrainedBox(
          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          child: Container(
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                     color: Colors.transparent,
                    ),
                    child: FutureBuilder(
                      future: ApiRequest.getMyDoctors(userID: appUserModel.id),
                      builder: (context, snapshot) {

                        if(snapshot.hasData) {
                          debugPrint("hasData");

                          myDoctorModel.MyDoctorModelList dList = myDoctorModel.MyDoctorModelList.fromSnapshot(snapshot);

                          return ListView.separated(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: dList.myDoctorList.length,
                            separatorBuilder: (context, index) {
                              return SizedBox(height: 4.0);
                            },
                            itemBuilder: (context, index) {
                              AppointmentType aType = AppointmentType.SCHEDULED_APPOINTMENT;
                              return MyDoctorsCardWidget(
                                appointmentType: aType,
                                doctors: dList.myDoctorList.elementAt(index),
                              );
                            },
                          );
                        } else if(snapshot.hasError){
                          MyToast.showToast(context,snapshot.error.toString());
                          return Container();
                        } else{
                          return Center(
                            child:CircularProgressIndicator() ,
                          );
                        }
                      }
                      ,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),      
      ),
    );
  }
  
}
