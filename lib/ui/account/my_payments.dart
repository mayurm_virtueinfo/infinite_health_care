import 'package:flutter/material.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/models/my_payment_model.dart';
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/item_my_payments.dart';
class MyPayments extends StatefulWidget {
  static const String routeName = '/MyPayments';
  @override
  _MyPaymentsState createState() => _MyPaymentsState();
}

class _MyPaymentsState extends State<MyPayments> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Payments',
          style: TextStyle(
            fontSize:22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child:Container(
              padding: EdgeInsets.all(20),
              constraints: BoxConstraints(
                minHeight: MediaQuery.of(context).size.height,
                minWidth: MediaQuery.of(context).size.width,
              ),
              child: Column(
                children: <Widget>[
                  FutureBuilder(
                    future: ApiRequest.getMyPayments(userID: appUserModel.id),
                    builder: (context, snapshot) {
                      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                      if(snapshot.hasData) {

                        List<dynamic> data = snapshot.data['data'] as List;
                        List<MyPaymentModel> mPaymentList = [];
                        data.forEach((element) {
                          MyPaymentModel paymentModel = MyPaymentModel.fromMap(element);
                          mPaymentList.add(paymentModel);
                        });
                        if(mPaymentList.length>0){
                          return ListView.separated(
                            shrinkWrap: true,
                            primary: false,
                            itemCount: mPaymentList.length,
                            separatorBuilder: (context, index) {
                              return SizedBox(height: 4.0);
                            },
                            itemBuilder: (context, index) {
                              return ItemMyPayments(
                                myPaymentModel: mPaymentList.elementAt(index),
                              );
                            },
                          );
                        }else{
                          return Container(
                            width: MediaQuery.of(context).size.width,
                            height: MediaQuery.of(context).size.height,
                            child: Center(
                              child: Text('No payments found',style: TextStyle(
                                  fontSize: 18
                              ),),
                            ),
                          );
                        }




                      } else if(snapshot.hasError){
                        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height,
                          child: Center(
                            child: Text('No payments found',style: TextStyle(
                                fontSize: 18
                            ),),
                          ),
                        );
                      } else{
                        locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                        return Container();
                      }
                    }
                    ,
                  ),
                ],
              ),
            ),
          ),
          LoadingWidget()
        ],
      ),
    );
  }
  
}
