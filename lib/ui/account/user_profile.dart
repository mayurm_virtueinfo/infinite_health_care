import 'dart:io';

import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/bloc/user_model_bloc.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/utils/app_style.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/utils/utility.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/widgets/profile_header_update_profile.dart';

class UserProfile extends StatefulWidget {
  static const String routeName = "/UserProfile";

  @override
  _UserProfileState createState() => _UserProfileState();
}

enum PhotoUpload { none, profile, cover }

class _UserProfileState extends State<UserProfile> {
  // bool isProfileUpdating = false;
  PhotoUpload photoUpload;
  var coverPhoto;
  File photo;
  bool isImageUploading = false;
  bool isCoverUploading = false;
  String errorText = '';
  bool isError = false;
  bool enableEmail = true;
  bool enablePhone = true;
  bool isEmailRequired = false;


  FocusNode fnFirstName = FocusNode();
  FocusNode fnLastName = FocusNode();
  FocusNode fnEmailName = FocusNode();
  FocusNode fnMobileNumber = FocusNode();
  FocusNode fnState = FocusNode();
  FocusNode fnCity = FocusNode();
  FocusNode fnAddress = FocusNode();
  FocusNode fnZipcode = FocusNode();

  TextEditingController _firstnameController = TextEditingController();
  TextEditingController _lastnameController = TextEditingController();
  TextEditingController _emailController = TextEditingController();
  TextEditingController _mobileNumberController = TextEditingController();
  TextEditingController _stateController = TextEditingController();
  TextEditingController _cityController = TextEditingController();
  TextEditingController _addressController = TextEditingController();
  TextEditingController _zipController = TextEditingController();

  GlobalKey<FormState> _formState = GlobalKey<FormState>();
  @override
  void initState() {
    photoUpload = PhotoUpload.none;

    initData();
    super.initState();
  }
  void initData()async{

    _firstnameController.text = appUserModel.first_name;
    _lastnameController.text = appUserModel.last_name;
    _emailController.text = appUserModel.email;
    _mobileNumberController.text = appUserModel.mobile;
    _stateController.text = appUserModel.state;
    _cityController.text = appUserModel.city;
    _addressController.text = appUserModel.address;
    _zipController.text = appUserModel.zipcode;

    bool isMobile = await Utility.isUserSignInWithMobile();
    bool isGmail = await Utility.isUserSignInWithGmail();
    bool isFacebook = await Utility.isUserSignInWithFacebook();
    if(isGmail || isFacebook){
      enableEmail = false;
    }
    if(isMobile){
      enablePhone = false;
      isEmailRequired = true;
    }



    setState(() {});
  }


  String labelAddress ='Address';
  String labelFName = 'First Name';
  String labelLName = 'Last Name';
  String labelEmail ='Email ID';
  String labelMobile = 'Mobile Number';
  String labelZipcode = 'Zipcode';
  String labelState = 'State';
  String labelCity = 'City';

  @override
  Widget build(BuildContext context) {
    double deviceWidth = MediaQuery.of(context).size.width;
    double deviceHeight = MediaQuery.of(context).size.height;
    double headerSize = (deviceHeight / 3.75);
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    double coverHeight = Utility.getCoverHeight(context);
    double profilePicSize = Utility.getProfilePicSize(context);
    double topPadding = coverHeight - (profilePicSize / 2);

    return Scaffold(
      extendBodyBehindAppBar: true,
//      backgroundColor: Theme.of(context).accentColor,
      appBar: AppBar(
//        backgroundColor: Colors.transparent,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Color(0x44000000),
        elevation: 0,
        title: Text(
          "Update Profile",
          style: TextStyle(
            fontSize: 22.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: [
                Stack(
                  children: <Widget>[
                    Container(
                      constraints: BoxConstraints(
                        minWidth: MediaQuery.of(context).size.width,
                        minHeight: coverHeight,
                      ),
                      child: Stack(
                        alignment: Alignment.bottomRight,
                        children: [
                          Container(
                            height: coverHeight,
                            width: MediaQuery.of(context).size.width,
                            child: appUserModel.cover_image != null && coverPhoto == null
                                ? FancyShimmerImage(
                                    boxFit: BoxFit.cover,
                                    imageUrl: appUserModel.cover_image,
                                    shimmerBaseColor: Colors.white,
                                    shimmerHighlightColor: Theme.of(context).accentColor,
                                    //                          shimmerBackColor: Colors.blue,
                                  )
                                : (coverPhoto == null && appUserModel.cover_image == null)
                                    ? FancyShimmerImage(
                                        boxFit: BoxFit.cover,
                                        imageUrl: Const.defaultCoverUrl,
                                        shimmerBaseColor: Colors.white,
                                        shimmerHighlightColor: Theme.of(context).accentColor,
                                        //                          shimmerBackColor: Colors.blue,
                                      )
                                    : (coverPhoto is File)
                                        ? Image.file(
                                            coverPhoto,
                                            fit: BoxFit.cover,
                                          )
                                        : FancyShimmerImage(
                                            boxFit: BoxFit.cover,
                                            imageUrl: coverPhoto,
                                            shimmerBaseColor: Colors.white,
                                            shimmerHighlightColor: Theme.of(context).accentColor,
                                            //                          shimmerBackColor: Colors.blue,
                                          ),
                          ),
                          Container(
                              margin: EdgeInsets.only(right: 10, bottom: 5),
                              alignment: Alignment.bottomRight,
                              child: InkWell(
                                onTap: () {
                                  photoUpload = PhotoUpload.cover;
                                  _settingModalBottomSheet(context);
                                },
                                child: Container(
                                  decoration: BoxDecoration(shape: BoxShape.circle, color: Theme.of(context).accentColor),
                                  child: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Icon(
                                      Icons.photo_camera,
                                      color: Colors.white,
                                    ),
                                  ),
                                ),
                              )),
                        ],
                      ),
                    ),
                    Padding(
                      padding: EdgeInsets.only(top: topPadding, left: 10, right: 10),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          ProfileHeaderUpdagteProfile(
                            imagePath: photo ?? appUserModel.avatar_image,
                            onTap: () {
                              photoUpload = PhotoUpload.profile;
                              _settingModalBottomSheet(context);
                            },
                            isImageUploading: isImageUploading,
                            showCameraIcon: true,
                          ),
                          SizedBox(
                            height: 10,
                          ),
                          Column(
                            children: <Widget>[
                              Text(
                                "${appUserModel.first_name} ${appUserModel.last_name}",
                                style: TextStyle(color: Theme.of(context).accentColor,  fontSize: 14, fontWeight: FontWeight.bold),
                              ),
                              /*Text(Utility.getProfileString(currentUser.address
                                  , currentUser.city
                                  , currentUser.state
                                  , currentUser.zipcode)),*/
                              Text(Utility.getProfileString(appUserModel.address
                                  , appUserModel.city
                                  , appUserModel.state
                                  , appUserModel.zipcode)),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
                Stack(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Form(
                          key: _formState,
                          child: Container(
                            padding: EdgeInsets.only(top: 5),
                            margin: EdgeInsets.only(top: 33.0, left: 14.0, right: 14.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              color: Theme.of(context).primaryColor,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                /*LabelText(
                                  leadingIcon: Icons.account_circle,
                                  label: labelFName,
                                  labelValue: currentUser.first_name,
                                  actionIcon: Icons.edit,
                                  onTapEdit: () {
                                    profileModalBottomSheet(labelFName, currentUser.first_name,
                                        Icons.account_circle,
                                        labelFName);
                                  },
                                ),*/
                                Container(
                                  child: TextFormField(
                                    focusNode: fnFirstName,
                                    textInputAction: TextInputAction.next,
                                    onFieldSubmitted: (term){
                                      fnLastName.requestFocus();
                                    },
                                    validator: (text) {
                                      if (text.length == 0) {
                                        return "Fullname is required";
                                      }

                                      return null;
                                    },
//                                            maxLength: 20,
                                    controller: _firstnameController,
                                    decoration: InputDecoration(
                                        errorStyle: AppStyles.errorStyle,
                                        focusedBorder: AppStyles.focusBorder,
                                        prefixIcon: Icon(
                                          Icons.account_circle,
                                          color: config.Colors().mainColor(1),
                                        ),
                                        labelStyle: TextStyle(
                                            color:
                                            config.Colors().mainColor(1)),
                                        border: OutlineInputBorder(),
                                        labelText: "Enter First Name"),
                                  ),
                                  padding: EdgeInsets.all(20),
                                ),
                                Container(
                                    margin: EdgeInsets.only(left: 5, right: 5),
                                    child: Divider(
                                      height: 1,
                                      color: config.Colors().mainColor(1),
                                    )),
                                /*LabelText(
                                  leadingIcon: Icons.account_circle,
                                  label: labelLName,
                                  labelValue: currentUser.last_name,
                                  actionIcon: Icons.edit,
                                  onTapEdit: () {
                                    profileModalBottomSheet(labelLName, currentUser.last_name,
                                        Icons.account_circle,
                                        labelLName);
                                  },
                                ),*/
                                Container(
                                  child: TextFormField(
                                    focusNode: fnLastName,
                                    textInputAction: TextInputAction.next,
                                    onFieldSubmitted: (term){
                                      fnEmailName.requestFocus();
                                    },
                                    validator: (text) {
                                      if (text.length == 0) {
                                        return "Fullname is required";
                                      }

                                      return null;
                                    },
//                                            maxLength: 20,
                                    controller: _lastnameController,
                                    decoration: InputDecoration(
                                        errorStyle: AppStyles.errorStyle,
                                        focusedBorder: AppStyles.focusBorder,
                                        prefixIcon: Icon(
                                          Icons.account_circle,
                                          color: config.Colors().mainColor(1),
                                        ),
                                        labelStyle: TextStyle(
                                            color:
                                            config.Colors().mainColor(1)),
                                        border: OutlineInputBorder(),
                                        labelText: "Enter Last Name"),
                                  ),
                                  padding: EdgeInsets.all(20),
                                ),
                                Container(
                                    margin: EdgeInsets.only(left: 5, right: 5),
                                    child: Divider(
                                      height: 1,
                                      color: config.Colors().mainColor(1),
                                    )),
                                /*LabelText(
                                  leadingIcon: Icons.email,
                                  label: labelEmail,
                                  labelValue: currentUser.email,

                                ),*/
                                Container(
                                  child: TextFormField(

                                    enabled: enableEmail,
                                    validator: (text) {
                                      if (isEmailRequired &&
                                          text.length == 0) {
                                        return "Email is required";
                                      }

                                      return null;
                                    },
//                                        maxLength: 15,
                                    controller: _emailController,
                                    decoration: InputDecoration(
                                      errorStyle: AppStyles.errorStyle,
                                      focusedBorder: AppStyles.focusBorder,
                                      prefixIcon: Icon(
                                        Icons.mail,
                                        color: config.Colors().mainColor(1),
                                      ),
                                      labelStyle: TextStyle(
                                          color:
                                          config.Colors().mainColor(1)),
                                      border: OutlineInputBorder(),
                                      labelText: "Enter Email",
                                    ),
                                  ),
                                  padding: EdgeInsets.all(20),
                                ),

                                /*LabelText(
                                    leadingIcon: Icons.mobile_screen_share,
                                    label: labelMobile,
                                    labelValue: currentUser.mobile,
                                    // actionIcon: Icons.edit,
                                    // onChanged: widget.onChangeMobileNumber
                                ),*/
                                Container(
                                    margin:
                                    EdgeInsets.only(left: 5, right: 5),
                                    child: Divider(
                                      height: 1,
                                      color: config.Colors().mainColor(1),
                                    )),
                                Container(
                                  child: TextFormField(
                                    focusNode: fnMobileNumber,
                                    textInputAction: TextInputAction.next,
                                    onFieldSubmitted: (term){
                                      fnState.requestFocus();
                                    },
                                    enabled: enablePhone,
                                    validator: (text) {
                                      if (text.length == 0) {
                                        return "Mobile Number is required";
                                      }
                                      /*else if(text.length < 10){
                                                  return "Invalid Mobile Number";
                                                }*/
                                      return null;
                                    },
                                    controller: _mobileNumberController,
                                    keyboardType: TextInputType.number,
//                                        maxLength: 10,
                                    decoration: InputDecoration(
                                      errorStyle: AppStyles.errorStyle,
                                      focusedBorder: AppStyles.focusBorder,
                                      prefixIcon: Icon(
                                        Icons.call,
                                        color: config.Colors().mainColor(1),
                                      ),
                                      labelStyle: TextStyle(
                                          color:
                                          config.Colors().mainColor(1)),
                                      border: OutlineInputBorder(),
                                      labelText: "Enter Mobile Number",
                                    ),
                                  ),
                                  padding: EdgeInsets.all(20),
                                ),

                                Container(
                                    margin:
                                    EdgeInsets.only(left: 5, right: 5),
                                    child: Divider(
                                      height: 1,
                                      color: config.Colors().mainColor(1),
                                    )),
                                Container(
                                  child: TextFormField(
                                    focusNode: fnState,
                                    textInputAction: TextInputAction.next,
                                    onFieldSubmitted: (term){
                                      fnCity.requestFocus();
                                    },
                                    validator: (text){
                                      if(text.length == 0){
                                        return "State is required";
                                      }
                                      return null;
                                    },
                                    controller: _stateController,
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                      errorStyle: AppStyles.errorStyle,
                                      focusedBorder: AppStyles.focusBorder,
                                      prefixIcon: Icon(
                                        Icons.location_on,
                                        color: config.Colors().mainColor(1),
                                      ),
                                      labelStyle: TextStyle(
                                          color:
                                          config.Colors().mainColor(1)),
                                      border: OutlineInputBorder(),
                                      labelText: "Enter State",
                                    ),
                                  ),
                                  padding: EdgeInsets.all(20),
                                ),
//                              city
                                Container(
                                    margin:
                                    EdgeInsets.only(left: 5, right: 5),
                                    child: Divider(
                                      height: 1,
                                      color: config.Colors().mainColor(1),
                                    )),
                                Container(
                                  child: TextFormField(
                                    focusNode: fnCity,
                                    textInputAction: TextInputAction.next,
                                    onFieldSubmitted: (term){
                                      fnAddress.requestFocus();
                                    },
                                    validator: (text){
                                      if(text.length == 0){
                                        return "City is required";
                                      }
                                      return null;
                                    },
                                    controller: _cityController,
                                    keyboardType: TextInputType.text,
                                    decoration: InputDecoration(
                                      errorStyle: AppStyles.errorStyle,
                                      focusedBorder: AppStyles.focusBorder,
                                      prefixIcon: Icon(
                                        Icons.location_city,
                                        color: config.Colors().mainColor(1),
                                      ),
                                      labelStyle: TextStyle(
                                          color:
                                          config.Colors().mainColor(1)),
                                      border: OutlineInputBorder(),
                                      labelText: "Enter City",
                                    ),
                                  ),
                                  padding: EdgeInsets.all(20),
                                ),
//                              address
                                Container(
                                    margin:
                                    EdgeInsets.only(left: 5, right: 5),
                                    child: Divider(
                                      height: 1,
                                      color: config.Colors().mainColor(1),
                                    )),
                                Container(
                                  child: TextFormField(
                                    focusNode: fnAddress,
                                    textInputAction: TextInputAction.newline,
                                    /*onFieldSubmitted: (term){
                                                fnZipcode.requestFocus();
                                              },*/
                                    textAlign: TextAlign.start,
                                    controller: _addressController,
                                    validator: (text){
                                      if(text.length == 0){
                                        return "Address is required";
                                      }
                                      return null;
                                    },
                                    maxLines: 3,
                                    minLines: 3,
                                    keyboardType: TextInputType.multiline,
                                    decoration: InputDecoration(
                                        errorStyle: AppStyles.errorStyle,
                                        focusedBorder: AppStyles.focusBorder,
                                        // contentPadding: EdgeInsets.symmetric(horizontal: 0,vertical: 0),
                                        /*prefix: Icon(
                                                    Icons.location_on,
                                                    color: config.Colors().mainColor(1),
                                                  ),*/
                                        labelStyle: TextStyle(
                                            color:
                                            config.Colors().mainColor(1)),
                                        border: OutlineInputBorder(),
                                        labelText: "Enter Address",
                                        alignLabelWithHint: true
                                    ),
                                  ),
                                  padding: EdgeInsets.all(20),
                                ),
//                              zipcode
                                Container(
                                    margin:
                                    EdgeInsets.only(left: 5, right: 5),
                                    child: Divider(
                                      height: 1,
                                      color: config.Colors().mainColor(1),
                                    )),
                                Container(
                                  child: TextFormField(
                                    focusNode: fnZipcode,
                                    textInputAction: TextInputAction.done,
                                    onFieldSubmitted: (term){
                                      // fnDescription.requestFocus();
                                    },
                                    validator: (text){
                                      if(text.length == 0){
                                        return "Zip Code is required";
                                      }
                                      return null;
                                    },
                                    controller: _zipController,
                                    keyboardType: TextInputType.number,
//                                        maxLength: 10,
                                    decoration: InputDecoration(
                                      errorStyle: AppStyles.errorStyle,
                                      focusedBorder: AppStyles.focusBorder,
                                      prefixIcon: Icon(
                                        Icons.home,
                                        color: config.Colors().mainColor(1),
                                      ),
                                      labelStyle: TextStyle(
                                          color:
                                          config.Colors().mainColor(1)),
                                      border: OutlineInputBorder(),
                                      labelText: "Enter Zipcode",
                                    ),
                                  ),
                                  padding: EdgeInsets.all(20),
                                ),
                              ],
                            ),
                          ),
                        ),
//                        Center(child:ball(currentDoctor.avatar, Theme.of(context).primaryColor,)),
                      ],
                    ),
                  ],
                ),
                InkWell(
                    onTap: () {
                      if(_formState.currentState.validate()) {
                        updateProfile();
                      }

                    },
                    child: ButtonSave())
              ],
            ),
          ),
          LoadingWidget()
        ],
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              direction: Axis.horizontal,
              children: <Widget>[
                /*ListTile(
                    leading: new Icon(Icons.delete),
                    title: new Text('Remove'),
                    onTap: () {
                      _onClickRemovePhoto();
                    }),*/
                ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _onClickCameraPhoto();
                    }),
                ListTile(
                  leading: new Icon(Icons.photo),
                  title: new Text('Gallery'),
                  onTap: () {
                    _onClickGalleryPhoto();
                  },
                ),
              ],
            ),
          );
        });
  }

  void _onClickCameraPhoto() async {
    Navigator.pop(context);

    PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    File image;
    if (pickedFile != null) {
      image = File(pickedFile.path);
    } else {
      image = null;
    }
    if (image != null) {
      File cropped = await Utility.cropImage(image);
      debugPrint("cropped Image : ${cropped.toString()}");
      setState(() {
        if (photoUpload == PhotoUpload.profile) {
          photo = cropped;
        } else if (photoUpload == PhotoUpload.cover) {
          coverPhoto = cropped;
        }
      });
    }
  }

  void _onClickGalleryPhoto() async {
    Navigator.pop(context);
    PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    File image;
    if (pickedFile != null) {
      image = File(pickedFile.path);
    } else {
      image = null;
    }
    if (image != null) {
      File cropped = await Utility.cropImage(image);
      debugPrint("cropped Image : ${cropped.toString()}");
      setState(() {
        if (photoUpload == PhotoUpload.profile) {
          photo = cropped;
        } else if (photoUpload == PhotoUpload.cover) {
          coverPhoto = cropped;
        }
      });
    }
  }

  void _onClickRemovePhoto() {
    Navigator.pop(context);
    setState(() {
      if (photoUpload == PhotoUpload.profile) {
        photo = null;
      } else if (photoUpload == PhotoUpload.cover) {
        coverPhoto = null;
      }
    });
  }

  Widget ball(String image, Color color, profileSize) {
    return Container(
      alignment: Alignment.center,
      height: profileSize,
      width: profileSize,
      decoration: BoxDecoration(color: Colors.white, shape: BoxShape.circle),
      child: Container(
        height: profileSize - 5,
        width: profileSize - 5,
        child: ClipOval(
          child: FancyShimmerImage(
            imageUrl: image ?? Const.defaultProfileUrl,
            shimmerBaseColor: Colors.white,
            shimmerHighlightColor: config.Colors().mainColor(1),
            shimmerBackColor: Colors.green,
          ),
        ),
      ),
    );
  }

  void updateProfile() async {
    try {
      locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());

      Map<String, dynamic> mapResult = await ApiRequest.postCustomerUpdate(
          first_name: _firstnameController.text,
          last_name: _lastnameController.text,
          id: "${appUserModel.id}",
          mobile: _mobileNumberController.text,
          state: _stateController.text,
          email: _emailController.text,
          city: _cityController.text,
          address: _addressController.text,
          zipcode: _zipController.text,
          image: photo,
          cover_image: coverPhoto);

      Map<String, dynamic> mapUpdateUser = mapResult['data'];
      debugPrint("Result Pos : ${mapUpdateUser}");
      UserModel user = UserModel(
        zipcode: mapUpdateUser['zipcode'],
        address: mapUpdateUser['address'],
        city: mapUpdateUser['city'],
        email: mapUpdateUser['email'],
        mobile: mapUpdateUser['mobile'],
        // isAuthenticated: previousNonUpdateUser.isAuthenticated,
        cover_image: mapUpdateUser['cover_image'],
        avatar_image: mapUpdateUser['avatar_image'],
        state: mapUpdateUser['state'],
        first_name: mapUpdateUser['first_name'],
        last_name: mapUpdateUser['last_name'],
        // loginType: previousNonUpdateUser.loginType,
        created_at: mapUpdateUser['created_at'],
        id: "${mapUpdateUser['id']}",
        ip: mapUpdateUser['ip'],
        last_login: mapUpdateUser['last_login'],
        modified_at: mapUpdateUser['modified_at'],
        password: mapUpdateUser['password'],
        remember_token: mapUpdateUser['remeber_token'],
        status: mapUpdateUser['status'],
      );
      debugPrint("updated user : ${user.toJson().toString()}");
      // await user.saveUser(userModel: user);
      locator<UserModelBloc>().userModelEventSink.add(user);
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      MyToast.showToast(context,"Profile Updated Successfully");
    } catch (e) {
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      debugPrint("Error : ${e.toString()}");
    }
  }
}

class ButtonSave extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(30.0),
      child: Container(
        height: 55.0,
        width: 600.0,
        child: Text(
          "Save",
          style: TextStyle(color: Colors.white, letterSpacing: 0.2, fontFamily: "Sans", fontSize: 18.0, fontWeight: FontWeight.w800),
        ),
        alignment: FractionalOffset.center,
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(30.0), gradient: LinearGradient(colors: <Color>[config.Colors().mainColor(1), config.Colors().mainColor(1)])),
      ),
    );
  }
}
