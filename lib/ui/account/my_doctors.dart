import 'package:flutter/material.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/models/appointment_type.dart';
import 'package:infinite_health_care/models/my_doctor_model.dart' as myDoctorModel;
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/ui/home_screen.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/conversation_item_widget.dart';
import 'package:infinite_health_care/widgets/my_doctors_card_widget.dart';

class MyDoctors extends StatefulWidget {
  static const routeName = '/MyDoctorsList';
  @override
  _MyDoctorsState createState() => _MyDoctorsState();
}

class _MyDoctorsState extends State<MyDoctors> {
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Theme.of(context).primaryColor,
          ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop();
          },

        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Doctors',
          style: TextStyle(
            fontSize:22.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child:ConstrainedBox(
              constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
              child: Container(
                child: Column(
                  children: <Widget>[
                    Expanded(
                      child: Container(
                        padding: EdgeInsets.all(20),
                        decoration: BoxDecoration(
                         color: Colors.transparent,
                        ),
                        child: FutureBuilder(
                          future: ApiRequest.getMyDoctors(userID: appUserModel.id),
                          builder: (context, snapshot) {

                            if(snapshot.hasData) {
                              locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                              debugPrint("hasData");

                              myDoctorModel.MyDoctorModelList dList = myDoctorModel.MyDoctorModelList.fromSnapshot(snapshot);
                              if(dList.myDoctorList.length>0){
                                return ListView.separated(
                                  shrinkWrap: true,
                                  primary: false,
                                  itemCount: dList.myDoctorList.length,
                                  separatorBuilder: (context, index) {
                                    return SizedBox(height: 0);
                                  },
                                  itemBuilder: (context, index) {
                                    AppointmentType aType = AppointmentType.SCHEDULED_APPOINTMENT;
                                    return MyDoctorsCardWidget(
                                      appointmentType: aType,
                                      doctors: dList.myDoctorList.elementAt(index),
                                    );
                                  },
                                );
                              }
                              else{
                                return Container(
                                  width: MediaQuery.of(context).size.width,
                                  height: MediaQuery.of(context).size.height,
                                  child: Center(
                                    child: Text('No doctors found',style: TextStyle(
                                      fontSize: 18
                                    ),),
                                  ),
                                );
                              }

                            } else if(snapshot.hasError){
                              locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                              // MyToast.showToast(context,snapshot.error.toString());
                              return Container(
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.height,
                                child: Center(
                                  child: Text('No doctors found',style: TextStyle(
                                      fontSize: 18
                                  ),),
                                ),
                              );
                            } else{
                              locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                              return Container();
                            }
                          }
                          ,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          LoadingWidget()
        ],
      ),
    );
  }
  
}
