import 'package:flutter/material.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/models/doctor_profile_model.dart';
import 'package:infinite_health_care/models/my_appointment_model.dart' as myAppointmentModel;
import 'package:infinite_health_care/models/appointment_type.dart';
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/ui/home/doctor_book_first_step.dart';
import 'package:infinite_health_care/ui/home_screen.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/appointments_card_widget.dart';
import 'package:infinite_health_care/widgets/conversation_item_widget.dart';
class MyAppointments extends StatefulWidget {
  static const String routeName = '/appointment';
  @override
  _MyAppointmentsState createState() => _MyAppointmentsState();
}

class _MyAppointmentsState extends State<MyAppointments> {
  bool isGettingDoctor = false;
  @override
  void initState() {
    super.initState();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Appointments',
          style: TextStyle(
            fontSize:22.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child:ConstrainedBox(
              constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
              child: Stack(
                children: [
                  Container(
                    child: Column(
                      children: <Widget>[
                        Expanded(
                          child: Container(
                            padding: EdgeInsets.all(20),
                            decoration: BoxDecoration(
                             color: Colors.transparent,
                            ),
                            child: FutureBuilder(
                              future: ApiRequest.getMyAppointments(userID: appUserModel.id),
                              builder: (context, snapshot) {

                                if(snapshot.hasData) {

                                  locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                  myAppointmentModel.MyApointmentListModel dList = myAppointmentModel.MyApointmentListModel.fromSnapshot(snapshot);
                                  if(dList.myAppointment.length>0){
                                    return ListView.separated(
                                      shrinkWrap: true,
                                      primary: false,
                                      itemCount: dList.myAppointment.length,
                                      separatorBuilder: (context, index) {
                                        return SizedBox(height: 4.0);
                                      },
                                      itemBuilder: (context, index) {
                                        AppointmentType aType = AppointmentType.SCHEDULED_APPOINTMENT;
                                        return AppointmentsWidget(
                                          appointmentType: aType,
                                          appointment: dList.myAppointment.elementAt(index),
                                          onClickSchedule: ()async{
                                            setState(() {
                                              isGettingDoctor = true;
                                            });
                                            Map<String, dynamic> mapSingleDoctor =await ApiRequest.getSingleDoctors(id_doctors: '${dList.myAppointment.elementAt(index).id_doctor}');
                                            DoctorProfileModel doctorProfileModel = DoctorProfileModel.fromMap(mapSingleDoctor['data']);
                                            debugPrint("doctor : ${mapSingleDoctor}");
                                            setState(() {
                                              isGettingDoctor = false;
                                            });
                                            Navigator.of(context).pushNamed(DoctorBookFirstStep.routeName, arguments: [doctorProfileModel]);
                                          },
                                        );
                                      },
                                    );
                                  }
                                  else{
                                    return Container(
                                      width: MediaQuery.of(context).size.width,
                                      height: MediaQuery.of(context).size.height,
                                      child: Center(
                                        child: Text('No appointments Found',style: TextStyle(
                                            fontSize: 18
                                        ),),
                                      ),
                                    );
                                  }
                                } else if(snapshot.hasError){
                                  locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                                  return Container(
                                    width: MediaQuery.of(context).size.width,
                                    height: MediaQuery.of(context).size.height,
                                    child: Center(
                                      child: Text('No appointments Found',style: TextStyle(
                                          fontSize: 18
                                      ),),
                                    ),
                                  );
                                } else{
                                  locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                                  return Container();
                                }
                              }
                              ,
                            )/*ListView.separated(
                              shrinkWrap: true,
                              primary: false,
                              itemCount: appointmentList.appointment.length,
                              separatorBuilder: (context,index){
                                return SizedBox(height: 4.0);
                              },
                              itemBuilder: (context,index){

                                return AppointmentsWidget(
                                  appointment: appointmentList.appointment.elementAt(index),
                                );
                              },
                            )*/,
                          ),
                        ),
                      ],
                    ),
                  ),
                  isGettingDoctor?Container(
                    color: Colors.transparent,
                    height: MediaQuery.of(context).size.height,
                    width: MediaQuery.of(context).size.width,
                    child: Center(
                        child: CircularProgressIndicator()
                    ),
                  ):Container()
                ],
              ),
            ),
          ),
          LoadingWidget()
        ],
      ),
    );
  }
  
}
