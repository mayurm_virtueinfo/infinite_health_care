import 'dart:async';
import 'dart:convert';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/bloc/user_model_bloc.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:intl/intl.dart';
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/ui/home_screen.dart';
import 'package:infinite_health_care/utils/app_preferences.dart';
import 'package:infinite_health_care/utils/app_style.dart';
import 'package:infinite_health_care/utils/my_colors.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/utils/utility.dart';
import 'package:infinite_health_care/utils/width_sizes.dart';
import 'package:infinite_health_care/widgets/app_button.dart';
import 'package:infinite_health_care/widgets/profile_header.dart';
import 'package:infinite_health_care/widgets/profile_header_register.dart';
import 'package:shared_preferences/shared_preferences.dart';

class PatientRegistrationScreen extends StatefulWidget {
  UserModel user;
  static const String routeName = "/PatientRegistrationScreen";

  PatientRegistrationScreen({this.user});
  @override
  _PatientRegistrationScreenState createState() => _PatientRegistrationScreenState();
}

enum PhotoUpload { none, profile, cover }

class _PatientRegistrationScreenState extends State<PatientRegistrationScreen> {

  GlobalKey<FormState> _formState= GlobalKey<FormState>();

  // bool isProfileUpdating = false;

  bool isImageUploading = false;
  bool isCoverUploading = false;

  String firstName = '';
  String lastName = '';
  String email = '';
  String mobileNumber = '';
  String state = '';
  String city = '';
  String address = '';
  String zip = '';
  File photo = null;
  bool enableEmail = true;
  bool enablePhone = true;
  bool isEmailRequired = false;
  @override
  void initState() {
    super.initState();
    initData();

  }
  void initData() async{

    firstName = widget.user.first_name??"";
    lastName = widget.user.last_name??"";
    email = widget.user.email??"";
    mobileNumber = widget.user.mobile??"";
    state = widget.user.state??"";
    city = widget.user.city??"";
    address = widget.user.address??"";
    zip = widget.user.zipcode??"";

    _firstNameController.text = firstName;
    _lastNameController.text = lastName;
    _emailController.text = email;
    _mobileNumberController.text = mobileNumber;
    _stateController.text = state;
    _cityController.text = city;
    _addressController.text = address;
    _zipController.text = zip;

    bool isMobile = await Utility.isUserSignInWithMobile();
    bool isGmail = await Utility.isUserSignInWithGmail();
    bool isFacebook = await Utility.isUserSignInWithFacebook();

    if(isGmail || isFacebook){
      enableEmail = false;
    }


    if(isMobile){
      enablePhone = false;
      isEmailRequired = true;
    }


    setState(() {

    });
  }



  final _firstNameController = TextEditingController();
  final _lastNameController = TextEditingController();
  final _emailController = TextEditingController();
  final _mobileNumberController = TextEditingController();
  final _stateController = TextEditingController();
  final _cityController = TextEditingController();
  final _addressController = TextEditingController();
  final _zipController = TextEditingController();

  Future<bool> _onBackPressed() {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text('Are you sure?'),
        content: new Text('Do you want to exit an App'),
        actions: <Widget>[
          new GestureDetector(
            onTap: () => Navigator.of(context).pop(false),
            child: Text("NO"),
          ),
          SizedBox(height: 16),
          new GestureDetector(
            onTap: () async {
              bool result = await Utility.logoutUser();
              if(result) {
                Navigator.of(context).pop(true);
              }
            },
            child: Text("YES"),
          ),
        ],
      ),
    ) ??
        false;
  }

  @override
  Widget build(BuildContext context) {
    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    final img_bapasitaram_width = WidthSizes.splash_screen_img_bapasitaram_width*devicePixelRatio;
    final img_savealife_width = WidthSizes.splash_screen_img_savealife_width*devicePixelRatio;
    return WillPopScope(
      onWillPop:_onBackPressed ,
      child: Scaffold(
        appBar: AppBar(
          elevation: 0,
          leading: Container(),
          /*leading: IconButton(
            icon: Icon(Icons.arrow_back, color: Theme
                .of(context)
                .primaryColor),
            onPressed: () {
              Navigator.pop(context);
            },
          ),*/
          /*title: Text(
            name,
            style: TextStyle(
              fontSize: 22.0,

              fontWeight: FontWeight.bold,
              color: Theme
                  .of(context)
                  .primaryColor,
            ),
          ),*/
          backgroundColor: config.Colors().mainColor(1),
        ),
        body: Stack(
          children: [
            SingleChildScrollView(
              child: Column(
                children: <Widget>[

                  Stack(
                    children: <Widget>[
                      Container(
                        height: 150,
                        padding: const EdgeInsets.only(top: 40),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.only(
                              bottomLeft: Radius.circular(25.0),
                              bottomRight: Radius.circular(25.0)),
                          color: config.Colors().mainColor(1),
                        ),
                      ),
                      Stack(
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.only(left: 14.0, right: 14.0),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              color: Theme
                                  .of(context)
                                  .primaryColor,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
//                        photo
                                SizedBox(height: 30,),
                                ProfileHeaderRegister(
                                  imagePath: photo,
                                  onTap: () {
                                  _settingModalBottomSheet(context);
                                },
                                  isImageUploading: isImageUploading,
                                  showCameraIcon: true,
                                ),
                                SizedBox(
                                  height: 20,
                                ),
                                Form(
                                  key: _formState,
                                  child: Container(
                                      child: Wrap(
                                            direction: Axis.horizontal,
                                        children: <Widget>[

//                              first_name
                                      Container(
                                          child: TextFormField(
                                            validator: (text){
                                              if(text.length == 0){
                                                return "First Name is required";
                                              }

                                              return null;
                                            },
//                                            maxLength: 20,
                                            controller: _firstNameController,
                                            decoration: InputDecoration(
                                              errorStyle: AppStyles.errorStyle,
                                              focusedBorder: AppStyles.focusBorder,
                                              prefixIcon: Icon(
                                                Icons.account_circle,
                                                color: config.Colors().mainColor(1),

                                              ),
                                              labelStyle: TextStyle(color: config.Colors().mainColor(1)),
                                              border: OutlineInputBorder(),
                                              labelText: "Enter First Name"
                                            ),
                                          ),
                                          padding: EdgeInsets.all(20),
                                  ),

//                              last_name
                                      Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 5),
                                          child: Divider(
                                            height: 1,
                                            color: config.Colors().mainColor(1),
                                          )),
                                      Container(
                                            child: TextFormField(
                                              validator: (text){
                                                if(text.length==0){
                                                  return "Last Name is required";
                                                }/*else if(text.length < 6){
                                                  return "Last Name should have atleast 6 charecter'";
                                                }*/
                                                return null;
                                              },
//                                        maxLength: 15,
                                              controller: _lastNameController,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixIcon: Icon(
                                                  Icons.verified_user,
                                                  color: config.Colors().mainColor(1),

                                                ),
                                                labelStyle: TextStyle(color: config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter Last Name",

                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),

//                              email
                                      Container(
                                              margin: EdgeInsets.only(
                                                  left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                      Container(
                                            child: TextFormField(
                                              enabled: enableEmail,
                                              validator: (text){
                                                if(isEmailRequired && text.length==0){
                                                  return "Email is required";
                                                }/*else if(text.length<6){
                                                  return "Password should have atleast 6 charecter";
                                                }*/
                                                return null;
                                              },
//                                        maxLength: 15,
                                              controller: _emailController,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                  prefixIcon: Icon(
                                                    Icons.mail,
                                                    color: config.Colors().mainColor(1),

                                                  ),
                                                  labelStyle: TextStyle(color: config.Colors().mainColor(1)),
                                                  border: OutlineInputBorder(),
                                                  labelText: "Enter Email",

                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),

//                              mobile_number
                                      Container(
                                              margin: EdgeInsets.only(
                                                  left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                      Container(
                                            child: TextFormField(
                                              enabled: enablePhone,
                                              validator: (text){
                                                if(text.length == 0){
                                                  return "Mobile Number is required";
                                                }/*else if(text.length < 10){
                                                  return "Invalid Mobile Number";
                                                }*/
                                                return null;
                                              },
                                              controller: _mobileNumberController,
                                              keyboardType: TextInputType.number,
//                                        maxLength: 10,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixIcon: Icon(
                                                  Icons.call,
                                                  color: config.Colors().mainColor(1),

                                                ),
                                                labelStyle: TextStyle(color: config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter Mobile Number",

                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),

//                              state
                                      Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 5),
                                          child: Divider(
                                            height: 1,
                                            color: config.Colors().mainColor(1),
                                          )),
                                      Container(
                                            child: TextFormField(
                                              /*validator: (text){
                                                if(text.length == 0){
                                                  return "State is required";
                                                }
                                                return null;
                                              },*/
//                                        maxLength: 10,
                                              controller: _stateController,
                                              keyboardType: TextInputType.text,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixIcon: Icon(
                                                  Icons.location_on,
                                                  color: config.Colors().mainColor(1),

                                                ),
                                                labelStyle: TextStyle(color: config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter State",

                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                          ),

//                              city
                                      Container(
                                              margin: EdgeInsets.only(
                                                  left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                      Container(
                                            child: TextFormField(
                                              /*validator: (text){
                                                if(text.length == 0){
                                                  return "City is required";
                                                }
                                                return null;
                                              },*/
//                                        maxLength: 10,
                                              controller: _cityController,
                                              keyboardType: TextInputType.text,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixIcon: Icon(
                                                  Icons.location_city,
                                                  color: config.Colors().mainColor(1),

                                                ),
                                                labelStyle: TextStyle(color: config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter City",

                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),

//                              address
                                      Container(
                                          margin: EdgeInsets.only(
                                              left: 5, right: 5),
                                          child: Divider(
                                            height: 1,
                                            color: config.Colors().mainColor(1),
                                          )),
                                      Container(
                                            child: TextFormField(
                                              controller: _addressController,
                                              /*validator: (text){
                                                if(text.length == 0){
                                                  return "Address is required";
                                                }
                                                return null;
                                              },*/
                                              maxLines: null,
                                              keyboardType: TextInputType.multiline,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixIcon: Icon(
                                                  Icons.location_on,
                                                  color: config.Colors().mainColor(1),

                                                ),
                                                labelStyle: TextStyle(color: config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter Address",

                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),

//                              zipcode
                                      Container(
                                              margin: EdgeInsets.only(
                                                  left: 5, right: 5),
                                              child: Divider(
                                                height: 1,
                                                color: config.Colors().mainColor(1),
                                              )),
                                      Container(
                                            child: TextFormField(
                                              /*validator: (text){
                                                if(text.length == 0){
                                                  return "Zip Code is required";
                                                }*//*else if(text.length < 10){
                                                  return "Invalid Mobile Number";
                                                }*//*
                                                return null;
                                              },*/
                                              controller: _zipController,
                                              keyboardType: TextInputType.number,
//                                        maxLength: 10,
                                              decoration: InputDecoration(
                                                errorStyle: AppStyles.errorStyle,
                                                focusedBorder: AppStyles.focusBorder,
                                                prefixIcon: Icon(
                                                  Icons.home,
                                                  color: config.Colors().mainColor(1),

                                                ),
                                                labelStyle: TextStyle(color: config.Colors().mainColor(1)),
                                                border: OutlineInputBorder(),
                                                labelText: "Enter Zip Code",

                                              ),
                                            ),
                                            padding: EdgeInsets.all(20),
                                          ),
                                        ],
                                      )),
                                ),
                              ],
                            ),
                          ),
//                        Center(child:ball(currentDoctor.avatar, Theme.of(context).primaryColor,)),
                        ],
                      ),
                    ],
                  ),
                  InkWell(
                      onTap: () async {

                        if(_formState.currentState.validate()){
                          _saveProfile(context);
                        }
                      },
                      child: AppButton(text: "Register",))
                ],
              ),
            ),
            LoadingWidget()
          ],
        ),
      ),
    );
  }

  void _saveProfile(context) async {

    try {
      locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());

      widget.user.first_name = _firstNameController.text;
      widget.user.last_name = _lastNameController.text;
      widget.user.email = _emailController.text;
      widget.user.mobile = _mobileNumberController.text;
      widget.user.state = _stateController.text;
      widget.user.city = _cityController.text;
      widget.user.address = _addressController.text;
      widget.user.zipcode = _zipController.text;
//      widget.user.avatar_image = photo;

      UserModel user = widget.user;
      debugPrint("--- ${user.first_name}");
    Map<String, dynamic> mapResult = await ApiRequest.postCustomerCreate(
        first_name: widget.user.first_name,
        last_name: widget.user.last_name,
        id_firebase: "${widget.user.id}",
        mobile: widget.user.mobile,
        state: widget.user.state,
        email: widget.user.email,
        city: widget.user.city,
        address: widget.user.address,
        zipcode: widget.user.zipcode,
        image: photo,
      );

      Map<String,dynamic> mapCreateUser = mapResult['data'];
      debugPrint("mapCreateUser : ${mapCreateUser}");
      debugPrint("old user : ${user.toJson().toString()}");
      user = UserModel(
        zipcode: mapCreateUser['zipcode'],
        address: mapCreateUser['address'],
        city: mapCreateUser['city'],
        email: mapCreateUser['email'],
        mobile: mapCreateUser['mobile'],
        // isAuthenticated: user.isAuthenticated,
        cover_image: mapCreateUser['cover_image'],
        avatar_image: mapCreateUser['avatar_image'],
        state: mapCreateUser['state'],
        first_name: mapCreateUser['first_name'],
        last_name: mapCreateUser['last_name'],
        // loginType: user.loginType,
        created_at: mapCreateUser['created_at'],
        id: "${mapCreateUser['id']}",
        ip: mapCreateUser['ip'],
        last_login: mapCreateUser['last_login'],
        modified_at: mapCreateUser['modified_at'],
        password: mapCreateUser['password'],
        remember_token: mapCreateUser['remeber_token'],
        status: mapCreateUser['stats'],
      );
      debugPrint("updated user : ${user.toJson().toString()}");
      // await user.saveUser(userModel: user);

      locator<UserModelBloc>().userModelEventSink.add(user);
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      _navigateToHomeScreen(user);
      /*setState(() {
        isProfileUpdating = false;
      });*/
    } catch (e) {
      debugPrint("Error saving prifile : ${e.message}");
      MyToast.showToast(context,"Error updating profile ${e.toString()}");
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      /*setState(() {
        isProfileUpdating = false;
      });*/
    }
  }

  void _navigateToHomeScreen(user) async {

      Navigator.of(context).pushNamedAndRemoveUntil(HomeScreen.routeName, (route) => false);
  }

  void _settingModalBottomSheet(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return Container(
            child: new Wrap(
              direction: Axis.horizontal,
              children: <Widget>[
                /*ListTile(
                    leading: new Icon(Icons.delete),
                    title: new Text('Remove'),
                    onTap: (){
                      _onClickRemovePhoto();
                    }
                ),*/
                ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _onClickCameraPhoto();
                    }),
                ListTile(
                  leading: new Icon(Icons.photo),
                  title: new Text('Gallery'),
                  onTap: () {
                    _onClickGalleryPhoto();
                  },
                ),
              ],
            ),
          );
        });
  }

  void _onClickCameraPhoto() async {
    Navigator.pop(context);

    PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.camera);
    File image;
    if (pickedFile != null) {
      image = File(pickedFile.path);
    } else {
      image = null;
    }
    if (image != null) {
      File cropped = await  Utility.cropImage(image);
      debugPrint("cropped Image : ${cropped.toString()}");
      setState(() {
        photo = cropped;
      });
//      setImagePath(cropped);
    }
  }

  /*void setImagePath(imgPath) async {
    setState(() {
      isImageUploading = true;
    });
    String fileURL;
    if (imgPath == null) {
      fileURL = Const.defaultProfileUrl;
      setState(() {
        photo = fileURL;
        isImageUploading = false;

      });
    } else {
      String storePath = "img_profile_${widget.user.id}";
      fileURL = await Utility.uploadFile(imgPath, storePath,"user_profile_photos");

      setState(() {
        photo = fileURL;
        isImageUploading = false;
      });
    }
  }*/





  void _onClickGalleryPhoto() async {
    Navigator.pop(context);
    PickedFile pickedFile = await ImagePicker().getImage(source: ImageSource.gallery);
    File image;
    if (pickedFile != null) {
      image = File(pickedFile.path);
    } else {
      image = null;
    }
    if (image != null) {
      File cropped = await  Utility.cropImage(image);
      debugPrint("cropped Image : ${cropped.toString()}");
      setState(() {
        photo = cropped;
      });
//      setImagePath(cropped);
    }
  }


  void _onClickRemovePhoto() {
    Navigator.pop(context);
//    setImagePath(null);
    setState(() {
      photo = null ;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }
}





