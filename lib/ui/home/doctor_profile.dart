import 'dart:async';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/main.dart';
import 'dart:math';
import 'dart:ui' as ui;
import 'dart:typed_data';
import 'package:flutter/services.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/ui/home/all_timings_screen.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/models/doctor_model.dart';
import 'package:infinite_health_care/models/doctor_profile_model.dart';
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/ui/home/doctor_album_images.dart';
import 'package:infinite_health_care/ui/home/doctor_book_first_step.dart';
import 'package:infinite_health_care/ui/home/send_feedback.dart';
import 'package:infinite_health_care/utils/my_colors.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/utils/strings.dart';
import 'package:maps_launcher/maps_launcher.dart';

class DoctorProfile extends StatefulWidget {
  static const String routeName = '/DoctorProfile';
  String title;
  String id_doctor;

  DoctorProfile({Key key, this.id_doctor, this.title}) : super(key: key);

  @override
  _DoctorProfileState createState() => _DoctorProfileState();
}

class _DoctorProfileState extends State<DoctorProfile> {
  DoctorProfileModel doctorProfileModel;
  Completer<GoogleMapController> _controller = Completer();

  static final CameraPosition _kLake = CameraPosition(bearing: 192.8334901395799, target: LatLng(37.43296265331129, -122.08832357078792), tilt: 59.440717697143555, zoom: 19.151926040649414);

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double devicePixelRatio = MediaQuery.of(context).devicePixelRatio;
    double coverHeight = devicePixelRatio * 150;

    Future<Uint8List> getBytesFromAsset(String path, int width) async {
      ByteData data = await rootBundle.load(path);
      ui.Codec codec = await ui.instantiateImageCodec(data.buffer.asUint8List(), targetWidth: width);
      ui.FrameInfo fi = await codec.getNextFrame();
      return (await fi.image.toByteData(format: ui.ImageByteFormat.png)).buffer.asUint8List();
    }

    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft: Radius.circular(16.0), bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          widget.title,
          style: TextStyle(
            fontSize: 22.0,
            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Container(
          constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height,
            minWidth: MediaQuery.of(context).size.width,
          ),
          child: FutureBuilder(
            future: ApiRequest.getSingleDoctors(id_doctors: widget.id_doctor),
            builder: (context, snapshot) {
              if (snapshot.hasData) {
                locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                doctorProfileModel = DoctorProfileModel.fromSnapshot(snapshot);

                debugPrint("review detail : ${doctorProfileModel.review_detail}");
                debugPrint("feedback length : ${doctorProfileModel.review_detail.length}");
                debugPrint("experience : ${doctorProfileModel.experience}");
                debugPrint("voting : ${doctorProfileModel.voting}");
                ScrollPhysics mScrollPhysics = doctorProfileModel.review_detail.length > 1 ? ScrollPhysics() : NeverScrollableScrollPhysics();
                bool mAutoPlay = doctorProfileModel.review_detail.length > 1 ? true : false;
                return Column(
                  children: <Widget>[
                    Stack(
                      children: <Widget>[
                        Container(
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.only(bottomLeft: Radius.circular(25.0), bottomRight: Radius.circular(25.0)),
                            color: Theme.of(context).accentColor,
                          ),
                          height: coverHeight,
                          width: MediaQuery.of(context).size.width,
                          child: FancyShimmerImage(
                            boxFit: BoxFit.cover,
                            imageUrl: doctorProfileModel.cover_image,
                            shimmerBaseColor: Colors.white,
                            shimmerHighlightColor: Theme.of(context).accentColor,
                            //                          shimmerBackColor: Colors.blue,
                          ),
                        ),
                        Stack(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(26.0),
                              margin: EdgeInsets.only(top: 175.0, left: 14.0, right: 14.0),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20.0),
                                boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.4), offset: Offset(2, 4), blurRadius: 10)],
                                color: Theme.of(context).primaryColor,
                              ),
                              child: Column(
                                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      (doctorProfileModel.isVerified == '1')
                                          ? Container(
                                              child: Column(
                                                children: [
                                                  Icon(
                                                    Icons.verified_user,
                                                    color: Colors.green,
                                                  ),
                                                  Text(
                                                    'Verified',
                                                    style: TextStyle(color: Colors.green),
                                                  )
                                                ],
                                              ),
                                            )
                                          : Container() /*Container(
                                              child: Column(
                                                children: [
                                                  Icon(
                                                    Icons.verified_user,
                                                    color: Colors.red,
                                                  ),
                                                  Text(
                                                    'Not Verified',
                                                    style: TextStyle(color: Colors.red),
                                                  )
                                                ],
                                              ),
                                            )*/
                                      ,
                                      Row(
                                        children: <Widget>[
                                          (doctorProfileModel.rating == null || doctorProfileModel.rating == '')?Container(): Row(
                                                  children: [
                                                    Icon(
                                                      Icons.star,
                                                      color: Colors.amber,
                                                      size: 22,
                                                    ),
                                                    Text(
                                                      "${doctorProfileModel.rating}",
                                                      style: TextStyle(
                                                        fontSize: 16,
                                                        color: Colors.grey,
                                                      ),
                                                    ),
                                                  ],
                                                )
                                        ],
                                      ),
                                    ],
                                  ),
                                  SizedBox(height: 30,),
                                  Column(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    children: <Widget>[
                                      Text(
                                        "${doctorProfileModel.title}",
                                        style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold,
                                          color: Theme.of(context).hintColor,
                                        ),
                                      ),
                                      Html(
                                        data: '${doctorProfileModel.description ?? ''}',
                                        shrinkWrap: true,
                                        style: {
                                          'p': Style(
                                            textAlign: TextAlign.center,
                                          )
                                        },
                                      ),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 30,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      (doctorProfileModel.experience != null && doctorProfileModel.experience != '')
                                          ? Text(
                                              '${doctorProfileModel.experience} yrs. experience ',
                                              style: TextStyle(
                                                fontSize: 14.0,
                                                color: Theme.of(context).hintColor,
                                              ),
                                            )
                                          : Container(),
                                      (doctorProfileModel.voting != null && doctorProfileModel.voting != '')
                                          ? Text(
                                              '( ${doctorProfileModel.voting} votes )',
                                              style: TextStyle(
                                                fontSize: 14.0,
                                                color: Theme.of(context).hintColor,
                                              ),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                  SizedBox(
                                    height: 10,
                                  ),
                                  Row(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: doctorProfileModel.album_images.asMap().entries.map((entry) {
                                      if (entry.key == 3) {
                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).pushNamed(DoctorAlbumImages.routeName, arguments: [0, doctorProfileModel.album_images]);
                                          },
                                          child: Container(
                                            margin: EdgeInsets.only(right: 7, left: 7),
                                            height: (MediaQuery.of(context).size.width / 4) - (MediaQuery.of(context).devicePixelRatio * 20),
                                            width: (MediaQuery.of(context).size.width / 4) - (MediaQuery.of(context).devicePixelRatio * 20),
                                            child: Container(
                                              child: Stack(
                                                children: [
                                                  FancyShimmerImage(
                                                    boxFit: BoxFit.fill,
                                                    imageUrl: entry.value['album_images'],
                                                    shimmerBaseColor: Colors.white,
                                                    shimmerHighlightColor: Theme.of(context).accentColor,
                                                    //                          shimmerBackColor: Colors.blue,
                                                  ),
                                                  Container(
                                                    decoration: BoxDecoration(
                                                      color: Theme.of(context).accentColor.withOpacity(0.4),
                                                    ),
                                                    child: Center(
                                                      child: Text(
                                                        "+${doctorProfileModel.album_images.length - 4}",
                                                        style: TextStyle(
                                                          fontSize: 22.0,
                                                          fontWeight: FontWeight.bold,
                                                          color: Theme.of(context).primaryColor,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        );
                                      } else if (entry.key < 4) {
                                        return GestureDetector(
                                          onTap: () {
                                            Navigator.of(context).pushNamed(DoctorAlbumImages.routeName, arguments: [entry.key, doctorProfileModel.album_images]);
                                          },
                                          child: Container(
                                            decoration: BoxDecoration(
                                              borderRadius: BorderRadius.circular(10.0),
                                              shape: BoxShape.rectangle,
                                            ),
                                            height: (MediaQuery.of(context).size.width / 4) - (MediaQuery.of(context).devicePixelRatio * 20),
                                            width: (MediaQuery.of(context).size.width / 4) - (MediaQuery.of(context).devicePixelRatio * 20),
                                            margin: EdgeInsets.only(right: 7, left: 7),
                                            child: FancyShimmerImage(
                                              boxFit: BoxFit.fill,
                                              imageUrl: entry.value['album_images'],
                                              shimmerBaseColor: Colors.white,
                                              shimmerHighlightColor: Theme.of(context).accentColor,
                                              //                          shimmerBackColor: Colors.blue,
                                            ),
                                          ),
                                        );
                                      } else {
                                        return Container();
                                      }
                                    }).toList(),
                                  ),
                                ],
                              ),
                            ),
                          ],
                        ),
                        Container(
                            margin: EdgeInsets.only(top: 100.0, left: 14.0, right: 14.0),
                            child: Center(
                                child: ball(
                              doctorProfileModel.avatar_image,
                              Theme.of(context).primaryColor,
                            ))),
                      ],
                    ),
                    Container(
                      alignment: Alignment.topLeft,
                      padding: EdgeInsets.only(left: 10, right: 10),
                      child: FutureBuilder(
                        future: ApiRequest.getDoctorNoteListing(patient_id: appUserModel.id, doctor_id: widget.id_doctor),
                        builder: (context, snapshot) {
                          if (snapshot.hasData) {
                            List<dynamic> lstNotes = snapshot.data['data'];
                            debugPrint("lstNotes api : ${lstNotes}");
                            return Column(
                              children: [
                                lstNotes.length > 0
                                    ? Container(
                                        height: 40,
                                        margin: EdgeInsets.only(left: 5, right: 5, top: 20),
                                        padding: EdgeInsets.only(left: 20, top: 5, bottom: 5),
                                        decoration: BoxDecoration(
                                          borderRadius: BorderRadius.all(Radius.circular(100)),
                                          color: Colors.grey.withOpacity(0.1),
                                        ),
                                        child: Row(
                                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                          children: [
                                            Text(
                                              "Notes",
                                              style: TextStyle(fontSize: 14, fontWeight: FontWeight.bold),
                                            ),
                                          ],
                                        ),
                                      )
                                    : Container(),
                                Wrap(
                                  alignment: WrapAlignment.start,
                                  direction: Axis.horizontal,
                                  children: lstNotes.asMap().entries.map((e) {
                                    return GestureDetector(
                                      onTap: () {
                                        showNoteDialog(context, devicePixelRatio: devicePixelRatio, title: e.value['note_title'], image: e.value['noteImagePath'], description: e.value['note']);
                                      },
                                      child: Container(
                                        decoration: BoxDecoration(shape: BoxShape.circle),
                                        constraints: BoxConstraints(maxWidth: 150),
                                        margin: EdgeInsets.only(right: 10),
                                        child: Chip(
                                          avatar: CircleAvatar(
                                            backgroundColor: Colors.grey.shade800,
                                            child: ClipOval(
                                              child: FancyShimmerImage(
                                                imageUrl: e.value['noteImagePath'] ?? Const.defaultProfileUrl,
                                                shimmerBaseColor: Colors.white,
                                                shimmerHighlightColor: config.Colors().mainColor(1),
                                                shimmerBackColor: Colors.green,
                                                boxFit: BoxFit.cover,
                                                errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                              ),
                                            ),
                                          ),
                                          label: Text(e.value['note_title']),
                                        ),
                                      ),
                                    );
                                  }).toList(),
                                ),
                              ],
                            );
                          }
                          return Center(
                            child: SizedBox(
                              width: 40,
                              height: 40,
                              child: CircularProgressIndicator(
                                strokeWidth: 1.5,
                                valueColor: AlwaysStoppedAnimation<Color>(
                                  config.Colors().mainColor(1),
                                ),
                              ),
                            ),
                          );
                        },
                      ),
                    ),
                    // error from hear
                    Container(
                      padding: EdgeInsets.all(26.0),
                      width: double.maxFinite,
                      margin: EdgeInsets.only(top: 20.0, left: 14.0, right: 14.0, bottom: 30),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(20.0),
                        boxShadow: [BoxShadow(color: Colors.grey.withOpacity(0.4), offset: Offset(2, 4), blurRadius: 10)],
                        color: Theme.of(context).primaryColor,
                      ),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text(
                                "Fees : \₹ ${doctorProfileModel.fees}",
                                style: TextStyle(
                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).hintColor,
                                ),
                              ),
                              Container(
                                height: 30.0,
                                decoration: BoxDecoration(border: Border.all(width: 1.5, color: Theme.of(context).accentColor), borderRadius: BorderRadius.circular(20.0)),
                                child: TextButton(
                                  style: TextButton.styleFrom(
                                    padding: EdgeInsets.all(0),

                                  shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(20.0)),
                                  ),
                                  onPressed: () {
                                    if (doctorProfileModel != null) {
                                      Navigator.of(context).pushNamed(DoctorBookFirstStep.routeName, arguments: [doctorProfileModel]);
                                    }
                                  },
                                  child: Text(
                                    'Book',
                                    style: TextStyle(
                                      fontSize: 12.0,
                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context).accentColor,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                              height: 30.0,
                              child: Center(
                                child: Container(
                                  height: 1.0,
                                  color: Colors.grey[400].withOpacity(0.1),
                                ),
                              )),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              (doctorProfileModel.open_today == '0')
                                  ? Text(
                                      Strings.CLOSED_TODAY,
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.red,
                                      ),
                                    )
                                  : Text(
                                      Strings.OPEN_TODAY,
                                      style: TextStyle(
                                        fontSize: 12.0,
                                        fontWeight: FontWeight.bold,
                                        color: Colors.green,
                                      ),
                                    ),
                              Text(
                                doctorProfileModel.gtimings ?? "",
                                style: TextStyle(
                                  fontSize: 10.0,
                                  fontWeight: FontWeight.bold,
                                  color: Theme.of(context).hintColor,
                                ),
                              ),
                              GestureDetector(
                                onTap: () {
                                  Navigator.of(context).pushNamed(AllTimingsScreen.routeName, arguments: ['${widget.id_doctor}']);
                                },
                                child: Text(
                                  'All Timing',
                                  style: TextStyle(fontSize: 14.0, fontWeight: FontWeight.bold, color: Theme.of(context).accentColor),
                                ),
                              ),
                            ],
                          ),
                          SizedBox(
                              height: 20.0,
                              child: Center(
                                child: Container(
                                  height: 1.0,
                                  color: Colors.grey[400].withOpacity(0.1),
                                ),
                              )),
                          RichText(
                            text: TextSpan(
                              children: [
                                WidgetSpan(
                                  child: Icon(
                                    Icons.location_on,
                                    color: Theme.of(context).hintColor.withOpacity(0.5),
                                    size: 18,
                                  ),
                                ),
                                TextSpan(
                                  style: TextStyle(
                                    fontSize: 14.0,
                                    color: Colors.grey,
                                  ),
                                  text: '  ${doctorProfileModel.address}, ${doctorProfileModel.city}, ${doctorProfileModel.zipcode}',
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: 15,
                          ),
                          (doctorProfileModel.lat != null && doctorProfileModel.lng != null)
                              ? FutureBuilder(
                                  future: getBytesFromAsset('images/icon_marker_pin.png', 50),
                                  builder: (context, snapshot) {
                                    if (snapshot.hasData) {
                                      Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
                                      LatLng latLng = LatLng(double.parse(doctorProfileModel.lat), double.parse(doctorProfileModel.lng));

                                      CameraPosition _kGooglePlex = CameraPosition(
                                        target: latLng,
                                        zoom: 14.0,
                                      );

                                      var markerIdVal = doctorProfileModel.id.toString();
                                      final MarkerId markerId = MarkerId(markerIdVal);
                                      final Uint8List markerIcon = snapshot.data;

                                      // creating a new MARKER
                                      final Marker marker = Marker(
                                        icon: BitmapDescriptor.fromBytes(markerIcon),
                                        markerId: markerId,
                                        position: latLng,
                                        infoWindow: InfoWindow(
                                            title: '${doctorProfileModel.title}',
                                            snippet: '${doctorProfileModel.description}',
                                            onTap: () {
                                              MapsLauncher.launchCoordinates(latLng.latitude, latLng.longitude);
                                            }),
                                      );

                                      // adding a new marker to map
                                      markers[markerId] = marker;

                                      return Container(
                                        constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.width - (MediaQuery.of(context).size.width / 3), maxWidth: MediaQuery.of(context).size.width),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.only(
                                            topLeft: Radius.circular(10),
                                            topRight: Radius.circular(10),
                                            bottomRight: Radius.circular(10),
                                            bottomLeft: Radius.circular(10),
                                          ),
                                          child: GoogleMap(
                                            myLocationEnabled: true,
                                            compassEnabled: true,
                                            mapToolbarEnabled: true,
                                            mapType: MapType.terrain,
                                            initialCameraPosition: _kGooglePlex,
                                            onMapCreated: (GoogleMapController controller) {
                                              _controller.complete(controller);
                                            },
                                            markers: Set<Marker>.of(markers.values),
                                          ),
                                        ),
                                      );
                                    } else {
                                      return Container();
                                    }
                                  },
                                )
                              : Container(
                                  constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.width - (MediaQuery.of(context).size.width / 3), maxWidth: MediaQuery.of(context).size.width),
                                  child: ClipRRect(
                                    borderRadius: BorderRadius.only(
                                      topLeft: Radius.circular(10),
                                      topRight: Radius.circular(10),
                                      bottomRight: Radius.circular(10),
                                      bottomLeft: Radius.circular(10),
                                    ),
                                    child: GoogleMap(
                                      myLocationEnabled: true,
                                      compassEnabled: true,
                                      mapToolbarEnabled: true,
                                      mapType: MapType.terrain,
                                      initialCameraPosition: CameraPosition(
                                        target: LatLng(0.0, 0.0),
                                        zoom: 14.0,
                                      ),
                                      onMapCreated: (GoogleMapController controller) {
                                        _controller.complete(controller);
                                      },
                                      // markers: Set<Marker>.of(markers.values),
                                    ),
                                  ),
                                ),
                          SizedBox(
                              height: 30.0,
                              child: Center(
                                child: Container(
                                  height: 1.0,
                                  color: Colors.grey[400].withOpacity(0.1),
                                ),
                              )),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                'FEEDBACK',
                                style: TextStyle(
                                  fontSize: 14.0,
                                  color: Colors.grey,
                                ),
                              ),
                              (doctorProfileModel.review_detail != null && doctorProfileModel.review_detail.length > 0)
                                  ? CarouselSlider(
                                      options: CarouselOptions(
                                        height: 110,
                                        autoPlayInterval: Duration(seconds: 3),
                                        autoPlay: mAutoPlay,
                                        scrollPhysics: mScrollPhysics,
                                        viewportFraction: 1.0,
                                        autoPlayAnimationDuration: Duration(milliseconds: 800),
                                      ),
                                      items: doctorProfileModel.review_detail.asMap().entries.map((e) {
                                        return Card(
                                          elevation: 1,
                                          // color: MyColors.colorConvert(e.value.color),
                                          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(16)),
                                          child: Container(
                                            // padding: EdgeInsets.all(12),
                                            child: Row(
                                              children: [
                                                Expanded(
                                                  flex: 2,
                                                  child: Container(
                                                    alignment: Alignment.center,
                                                    child: Container(
                                                      width: 50,
                                                      height: 50,
                                                      child: ClipOval(
                                                        child: FancyShimmerImage(
                                                          imageUrl: e.value['patient_image'] ?? Const.defaultProfileUrl,
                                                          shimmerBaseColor: Colors.white,
                                                          shimmerHighlightColor: config.Colors().mainColor(1),
                                                          shimmerBackColor: Colors.green,
                                                          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
                                                          boxFit: BoxFit.cover,
                                                        ),
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                                Expanded(
                                                    flex: 4,
                                                    child: Container(
                                                      padding: EdgeInsets.only(top: 20, bottom: 10),
                                                      child: Column(
                                                        crossAxisAlignment: CrossAxisAlignment.start,
                                                        children: [
                                                          Text(
                                                            '${e.value['customer_name']}',
                                                            style: TextStyle(
                                                              fontSize: 12.0,
                                                              fontWeight: FontWeight.bold,
                                                              color: Colors.black,
                                                            ),
                                                          ),
                                                          Container(
                                                            child: RatingBarIndicator(
                                                              itemPadding: EdgeInsets.only(right: 5),
                                                              rating: double.parse(e.value['rating']),
                                                              itemBuilder: (context, index) => Container(
                                                                height: 20,
                                                                width: 20,
                                                                child: Icon(
                                                                  Icons.star,
                                                                  color: Colors.amber,
                                                                ),
                                                              ),
                                                              itemCount: 5,
                                                              itemSize: 20.0,
                                                              unratedColor: Colors.amber.withAlpha(50),
                                                              direction: Axis.horizontal,
                                                            ),
                                                          ),
                                                          SizedBox(
                                                            height: 10,
                                                          ),
                                                          Flexible(
                                                            child: Wrap(
                                                              direction: Axis.horizontal,
                                                              children: [
                                                                Text(
                                                                  e.value['description'],
                                                                  overflow: TextOverflow.ellipsis,
                                                                  style: TextStyle(fontSize: 10.0, color: Colors.grey, fontStyle: FontStyle.italic),
                                                                ),
                                                              ],
                                                            ),
                                                          ),
                                                        ],
                                                      ),
                                                    ))
                                              ],
                                            ),
                                          ),
                                        );
                                      }).toList(),

                                    )
                                  : Container(
                                      margin: EdgeInsets.only(top: 10),
                                      child: Text('No feedback available',
                                          style: TextStyle(
                                            fontSize: 14.0,
                                            color: Colors.grey,
                                          )),
                                    ),
                            ],
                          ),
                          SizedBox(
                              height: 30.0,
                              child: Center(
                                child: Container(
                                  height: 1.0,
                                  color: Colors.grey[400].withOpacity(0.1),
                                ),
                              )),
                          Text(
                            'SPECIALIZATION',
                            style: TextStyle(
                              fontSize: 14.0,
                              color: Colors.grey,
                            ),
                          ),
                          SizedBox(
                            height: 6.0,
                          ),
                          Wrap(
                            alignment: WrapAlignment.start,
                            direction: Axis.horizontal,
                            children: doctorProfileModel.specialization.asMap().entries.map((e) {
                              return GestureDetector(
                                onTap: () {
                                  // showNoteDialog(context,devicePixelRatio: devicePixelRatio,title: e.value['note_title'],image: e.value['noteImagePath'],description: e.value['note']);
                                },
                                child: Container(
                                  decoration: BoxDecoration(shape: BoxShape.circle),
                                  constraints: BoxConstraints(maxWidth: 150),
                                  margin: EdgeInsets.only(right: 10),
                                  child: Chip(
                                    label: Text(e.value['title']),
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                          SizedBox(
                              height: 30.0,
                              child: Center(
                                child: Container(
                                  height: 1.0,
                                  color: Colors.grey[400].withOpacity(0.1),
                                ),
                              )),
                        ],
                      ),
                    ),
                  ],
                );
              } else if (snapshot.hasError) {
                locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                return Center(
                  child: Text(snapshot.error.toString()),
                );
              } else {
                locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                return LoadingWidget(
                  initialData: true,
                );
              }
            },
          ),
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        color: Colors.white,
        child: Container(
            margin: EdgeInsets.all(12.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(30.0),
              border: Border.all(width: 1, color: Colors.grey.withOpacity(0.6)),
            ),
            child: Row(
              // mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                ElevatedButton(


                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                    primary: Colors.white,
                  ),
                  onPressed: () {
                    Navigator.of(context).pushNamed(SendFeedback.routeName, arguments: [widget.id_doctor]);
                  },

                  child: Container(
                    alignment: Alignment.center,
                    height: 50,
                    child: Text(
                      'GIVE FEEDBACK',
                      style: TextStyle(fontSize: 12.0, color: Theme.of(context).accentColor, fontWeight: FontWeight.bold),
                    ),
                  ),
                ),
                ElevatedButton(


                  style: ElevatedButton.styleFrom(
                    elevation: 0,
                    shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(30.0)),
                    primary: Theme.of(context).accentColor,
                  ),
                  onPressed: () {
                    if (doctorProfileModel != null) {
                      Navigator.of(context).pushNamed(DoctorBookFirstStep.routeName, arguments: [doctorProfileModel]);
                    }
                  },
                  child: Container(
                    alignment: Alignment.center,
                    height: 50,
                    width: MediaQuery.of(context).size.width / 2.5,
                    child: Text(
                      'Book',
                      style: TextStyle(fontSize: 12.0, color: Theme.of(context).primaryColor, fontWeight: FontWeight.bold),
                    ),
                  ),
                )
              ],
            )),
      ),
    );
  }

  void showNoteDialog(BuildContext context, {double devicePixelRatio, String image, title, description}) async {
    return showDialog(
      context: context,
      builder: (context) => new AlertDialog(
        title: new Text(title),
        content: SingleChildScrollView(
          child: Container(
            color: Colors.white,
            constraints: BoxConstraints(
              minHeight: MediaQuery.of(context).size.height,
              minWidth: MediaQuery.of(context).size.width,
            ),
            child: Column(
              children: [
                Container(
                  width: double.infinity,
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(10),
                    child: FancyShimmerImage(
                      boxFit: BoxFit.fill,
                      imageUrl: image,
                      shimmerBaseColor: Colors.white,
                      shimmerHighlightColor: Theme.of(context).accentColor,
                      shimmerBackColor: Colors.green,
                    ),
                  ),
                ),
                Text(description),
              ],
            ),
          ),
        ),
        actions: <Widget>[
          ElevatedButton(
            onPressed: () {
              Navigator.of(context).pop(false);
            },
            child: Text("Cancel"),
            style: ElevatedButton.styleFrom(
              primary: Theme.of(context).accentColor,
            ),
          ),
        ],
      ),
    );
  }

  Widget ball(
    String image,
    Color color,
  ) {
    return Container(
      height: 115,
      width: 115.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image ?? Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
        ),
      ),
    );
  }

  Widget card(String image, String title, String subTitle, String rank) {
    return Container(
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          ball(image, Colors.transparent),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                title,
                textAlign: TextAlign.left,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 12.0),
              ),
              Text(
                subTitle,
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 10.0),
              ),
              Text(
                "\₹ 50",
                textAlign: TextAlign.left,
                style: TextStyle(fontSize: 10.0),
              ),
            ],
          ),
          Row(
            children: <Widget>[
              Icon(
                Icons.star,
                color: Colors.yellow,
              ),
              Text(
                rank,
                style: TextStyle(),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
