import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:image_picker/image_picker.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/utils/app_style.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/utils/utility.dart';
import 'package:infinite_health_care/utils/width_sizes.dart';
import 'package:infinite_health_care/widgets/app_button.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:intl/intl.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SendFeedback extends StatefulWidget {
  static const String routeName = "/SendFeedback";

  String id_doctor;
  SendFeedback({this.id_doctor});
  @override
  _SendFeedbackState createState() => _SendFeedbackState();
}


class _SendFeedbackState extends State<SendFeedback> {

  GlobalKey<FormState> _formState= GlobalKey<FormState>();

  final _writeReviewController = TextEditingController();

  @override


  Timestamp sender_message_time;
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  SharedPreferences prefs;
  String userid = '';
  // bool isSendingMessage = false;
  double userRating;
  List<String> adminIdList= [];


  void initState() {
    super.initState();

  }

  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies

    super.didChangeDependencies();
  }
  @override
  Widget build(BuildContext context) {



    double devicePixelRatio = Utility.getDevicePixelRatio(context);
    final img_bapasitaram_width = WidthSizes.splash_screen_img_bapasitaram_width*devicePixelRatio;
    final img_savealife_width = WidthSizes.splash_screen_img_savealife_width*devicePixelRatio;
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Theme
              .of(context)
              .primaryColor),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Text(
          "Send Feedback",
          style: TextStyle(
            fontSize: 22.0,

            fontWeight: FontWeight.bold,
            color: Theme
                .of(context)
                .primaryColor,
          ),
        ),
        backgroundColor: Theme.of(context).accentColor,
      ),
      body: Stack(
        children: [
          SingleChildScrollView(
            child: Column(
              children: <Widget>[

                Stack(
                  children: <Widget>[
                    Container(
                      height: 150,
                      padding: const EdgeInsets.only(top: 40),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(25.0),
                            bottomRight: Radius.circular(25.0)),
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                    Stack(
                      children: <Widget>[
                        Form(
                          key: _formState,
                          child: Container(
                            width: double.infinity,
                            margin: EdgeInsets.only(left: 14.0, right: 14.0,top: 50),
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(20.0),
                              color: Theme
                                  .of(context)
                                  .primaryColor,
                            ),
                            child: Column(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                /*Container(
                                  width: img_bapasitaram_width- (devicePixelRatio*30),
                                  child: Center(
                                    child: Image.asset(
                                      "assets/icon_bapasitaram.png",
                                      fit: BoxFit.fill,
                                    ),
                                  ),
                                ),*/

                                SizedBox(height: 20,),
                               Text("Give your ratings",style: TextStyle(
                                 fontSize: 16,
                                 color: Colors.black
                               ),),
                                userRating==0?Text("Ratings Required",style: AppStyles.errorStyle,):Container(),
                                SizedBox(height: 20,),
                               Container(
                                  child: RatingBar.builder(
                                    initialRating: 0,
                                    minRating: 0,
                                    direction: Axis.horizontal,
                                    allowHalfRating: true,
                                    itemCount: 5,
                                    itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                                    itemBuilder: (context, _) => Icon(
                                      Icons.star,
                                      color: Colors.amber,
                                    ),
                                    onRatingUpdate: (rating) {
                                      print(rating);
                                      userRating = rating;
                                      setState(() {

                                      });
                                    },
                                  ),
                                ),

//                              address
                                Container(
                                  child: TextFormField(
                                    controller: _writeReviewController,
                                    validator: (text){
                                      if(text.length == 0){
                                        return "Review Required";
                                      }
                                      return null;
                                    },
                                    maxLines: 5,
                                    minLines: 5,
                                    keyboardType: TextInputType.multiline,
                                    decoration: InputDecoration(
                                      errorStyle: AppStyles.errorStyle,
                                      focusedBorder: AppStyles.focusBorder,
                                      alignLabelWithHint: true,
                                      /*prefixIcon: Icon(
                                        Icons.location_on,
                                        color: Theme.of(context).accentColor,

                                      ),*/
                                      labelStyle: TextStyle(color: Theme.of(context).accentColor),
                                      border: OutlineInputBorder(),
                                      labelText: "Write you review",


                                    ),
                                  ),
                                  padding: EdgeInsets.all(20),
                                ),
                                /*
                                   ,photo, userid,
                                  */
                                InkWell(
                                    onTap: () async {

                                      FocusScope.of(context).requestFocus(FocusNode());
                                      if(_formState.currentState.validate()){
                                        if(userRating == 0){
                                          return;
                                        }
                                        _sendReview();
                                      }
                                      setState(() {});
                                    },
                                    child: AppButton(text: "Send",)),
                                SizedBox(height: 10,),

//                          photo
                                SizedBox(height: 30,),
                              ],
                            ),
                          ),
                        ),
//                        Center(child:ball(currentDoctor.avatar, Theme.of(context).primaryColor,)),
                      ],
                    ),
                  ],
                ),

              ],
            ),
          ),
          LoadingWidget()
        ],
      ),
    );
  }

  void _sendReview() async {
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());

    try{
        final result = await ApiRequest.postAddReview(doctor_id: widget.id_doctor,patient_id: appUserModel.id,rating: userRating,review: _writeReviewController.text);
        debugPrint("add review result : ${result}");
        if(result['data'] !=null){
          MyToast.showToast(context, 'Review added. Thank your!');
          Navigator.of(context).pop();
        }

    }catch(e){
      debugPrint("${e.toString()}");
    }

    locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
  }



  @override
  void dispose() {
    super.dispose();
  }
}





