import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/main.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/models/doctor_profile_model.dart';
import 'package:infinite_health_care/ui/home/doctor_book_second_step.dart';
import 'package:infinite_health_care/ui/home/send_feedback.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/utils/utility.dart';
import 'package:intl/intl.dart';
class DoctorBookFirstStep extends StatefulWidget {
  DoctorProfileModel doctorProfileModel;
  static const String routeName = '/DoctorBookFirstStep';

  DoctorBookFirstStep({this.doctorProfileModel});
  @override
  _DoctorBookFirstStepState createState() => _DoctorBookFirstStepState();
}

class _DoctorBookFirstStepState extends State<DoctorBookFirstStep> {
  DateTime dateTime;
  List<String> morningList=[];
  List<String> afternoonList=[];
  List<String> nightList=[];
  String selectedChoice = "";
  List<dynamic> weeklyData=[];
  Map<String,dynamic> currentSlot;
  @override
  void initState() {
    // TODO: implement initState
    dateTime = DateTime.now();

    getDoctorTimings();

    super.initState();
  }
  bool isGettingTimings = false;
  void getDoctorTimings()async{
    setState(() {
      isGettingTimings = true;
    });
    try {
      final result = await ApiRequest.getDoctorTimings(doctor_id: '${widget.doctorProfileModel.id}');
      List data = result['data'];
      if (data != null ) {

        debugPrint("result getDoctorTimings : ${result['data']}");
        if(result['data'].length == 0){
          setState(() {
            isGettingTimings = false;
            return;
          });
        }
        weeklyData = result['data'];
        calculateSlots();
      }
      setState(() {
        isGettingTimings = false;
      });
    }catch(e){
      setState(() {
        isGettingTimings = false;
        MyToast.showToast(context,'Error getting slots : ${e.toString()}');
      });
    }
  }
  bool isTodaysDate(DateTime currentDate){
    DateTime now = DateTime.now();
    if(now.year == currentDate.year && now.month == currentDate.month && now.day == currentDate.day){
      return true;
    }
    return false;
  }
  Future<void> processAllSlots() async{
    if(currentSlot == null){
      return;
    }
    String first_start_time = currentSlot['first_start_time'];
    first_start_time = first_start_time.replaceAll(' ', '');
    String first_end_time = currentSlot['first_end_time'];
    String second_start_time = currentSlot['second_start_time'];
    String second_end_time = currentSlot['second_end_time'];
    int duration = currentSlot['duration'];

    DateFormat format= DateFormat("yyyy-MM-dd HH:mm");

    String dateStringFirstStart = '${dateTime.year}-${dateTime.month}-${dateTime.day} ${first_start_time}';
    DateTime dateFirstStart = format.parse(dateStringFirstStart);


    String dateStringFirstEnd = '${dateTime.year}-${dateTime.month}-${dateTime.day} ${first_end_time}';
    DateTime dateFirstEnd = format.parse(dateStringFirstEnd);

    String dateStringSecondStart = '${dateTime.year}-${dateTime.month}-${dateTime.day} ${second_start_time}';
    DateTime dateSecondStart = format.parse(dateStringSecondStart);

    String dateStringSecondEnd = '${dateTime.year}-${dateTime.month}-${dateTime.day} ${second_end_time}';
    DateTime dateSecondEnd = format.parse(dateStringSecondEnd);

    DateTime tmpFirstStart,tmpFirstEnd;
    DateTime tmpSecondStart,tmpSecondEnd;
    if(dateFirstStart.isBefore(dateFirstEnd)){
      tmpFirstStart = dateFirstStart;
      tmpFirstEnd = dateFirstEnd;
    }else{
      tmpFirstStart = dateFirstEnd;
      tmpFirstEnd = dateFirstStart;
    }
    debugPrint("dateFirstStart : ${tmpFirstStart.toString()}");
    debugPrint("dateFirstEnd : ${tmpFirstEnd.toString()}");
    int minutes = tmpFirstEnd.difference(tmpFirstStart).inMinutes;
    int availableSlots = minutes ~/ duration;
    List<String> mListFirstSlots=[];
    List<String> allSlots = [];
    DateTime tmpDformat = tmpFirstStart;
    for(int i=0;i<availableSlots;i++){
      DateFormat format= DateFormat("HH:mm");
      String mSlot = format.format(tmpDformat);
      mListFirstSlots.add(mSlot);
      allSlots.add(mSlot);
      tmpDformat = tmpDformat.add(Duration(minutes: (duration)));
    }

    debugPrint("First Slots : ${mListFirstSlots}");

    if(dateSecondStart.isBefore(dateSecondEnd)){
      tmpSecondStart = dateSecondStart;
      tmpSecondEnd = dateSecondEnd;
    }else{
      tmpSecondStart = dateSecondEnd;
      tmpSecondEnd = dateSecondStart;
    }

    minutes = tmpSecondEnd.difference(tmpSecondStart).inMinutes;
    availableSlots = minutes ~/ duration;
    List<String> mListSecondSlots=[];
    tmpDformat = tmpSecondStart;
    for(int i=0;i<availableSlots;i++){
      DateFormat format= DateFormat("HH:mm");
      String mSlot = format.format(tmpDformat);
      mListSecondSlots.add(mSlot);
      allSlots.add(mSlot);
      tmpDformat = tmpDformat.add(Duration(minutes: (duration)));
    }

    debugPrint("Second Slots : ${mListSecondSlots}");

    debugPrint("All Slots : ${allSlots}");

    debugPrint("dateSecondStart : ${tmpSecondStart.toString()}");
    debugPrint("dateSecondEnd : ${tmpSecondEnd.toString()}");

    String morningStringStart = '${dateTime.year}-${dateTime.month}-${dateTime.day} 00:00';
    DateTime morningStart = format.parse(morningStringStart);

    String morningStringEnd = '${dateTime.year}-${dateTime.month}-${dateTime.day} 12:00';
    DateTime morningEnd = format.parse(morningStringEnd);

    String afternoodStringStart = '${dateTime.year}-${dateTime.month}-${dateTime.day} 11:59';
    DateTime afternoonstart = format.parse(afternoodStringStart);

    String afternoodStringEnd = '${dateTime.year}-${dateTime.month}-${dateTime.day} 18:00';
    DateTime afternoonEnd = format.parse(afternoodStringEnd);

    String eveningStringStart = '${dateTime.year}-${dateTime.month}-${dateTime.day} 17:59';
    DateTime eveningStart = format.parse(eveningStringStart);

    String eveningStringEnd = '${dateTime.year}-${dateTime.month}-${dateTime.day} 23:59';
    DateTime eveningEnd = format.parse(eveningStringEnd);

    morningList.clear();
    allSlots.forEach((element) {
      String slotString = '${dateTime.year}-${dateTime.month}-${dateTime.day} $element';
      DateTime currentTime = format.parse(slotString);
      debugPrint("morning current : ${currentTime.toString()}");
      if(currentTime.isAfter(morningStart) && currentTime.isBefore(morningEnd)) {
        if(currentTime.isAfter(DateTime.now())) {
          morningList.add(element);
        }
      }
    });

    afternoonList.clear();
    allSlots.forEach((element) {
      String slotString = '${dateTime.year}-${dateTime.month}-${dateTime.day} $element';
      DateTime currentTime = format.parse(slotString);
      if(currentTime.isAfter(afternoonstart) && currentTime.isBefore(afternoonEnd)) {
        if(currentTime.isAfter(DateTime.now())) {
          afternoonList.add(element);
        }
      }
    });

    nightList.clear();
    allSlots.forEach((element) {
      String slotString = '${dateTime.year}-${dateTime.month}-${dateTime.day} $element';
      DateTime currentTime = format.parse(slotString);
      if(currentTime.isAfter(eveningStart) && currentTime.isBefore(eveningEnd)) {
        if(currentTime.isAfter(DateTime.now())) {
          nightList.add(element);
        }
      }
    });

    debugPrint("Morning : ${morningList}");
    debugPrint("Afternoon : ${afternoonList}");
    debugPrint("Night : ${nightList}");
    setState(() {

    });

  }
  void calculateSlots() async{
    if(weeklyData.length == 0){
      // Fluttertoast.showToast(msg: "No slots available for the date");
      return;
    }
    setState(() {
      isGettingTimings = true;
    });
    try {
      currentSlot = await getSlotsByDay(weeklyData, dateTime);
      debugPrint("slots : ${currentSlot.toString()}");
      if(currentSlot == null){
        // Fluttertoast.showToast(msg: "No slots available for the date");
        morningList = [];
        afternoonList = [];
        nightList = [];
        setState(() {
          isGettingTimings = false;
        });
        return;
      }
      await processAllSlots();
    }catch(e){
      debugPrint(e.toString());
      MyToast.showToast(context,'Error calculating slots');
      setState(() {
        isGettingTimings = false;
      });
    }
    setState(() {
      isGettingTimings = false;
    });
  }
  Future<Map<String,dynamic>> getSlotsByDay(List<dynamic> mData,DateTime dateTime) async{
      debugPrint("dateTime.weekday : ${dateTime.weekday}");
      if(dateTime.weekday == 7){
        List<dynamic> result = mData.where((element) => (element['day']=='Sunday' || element['day']=='sunday')).toList();
        if(result.length == 0){
          return null;
        }
        return result[0];
      } else if(dateTime.weekday == 1){
        List<dynamic> result = mData.where((element) => (element['day']=='Monday' || element['day']=='monday')).toList();
        if(result.length == 0){
          return null;
        }
        return result[0];
      } else if(dateTime.weekday == 2){
        List<dynamic> result = mData.where((element) => (element['day']=='Tuesday' || element['day']=='tuesday')).toList();
        if(result.length == 0){
          return null;
        }
        return result[0];
      } else if(dateTime.weekday == 3){
        List<dynamic> result = mData.where((element) => (element['day']=='Wednesday' || element['day']=='wednesday')).toList();
        if(result.length == 0){
          return null;
        }
        return result[0];
      } else if(dateTime.weekday == 4){
        List<dynamic> result = mData.where((element) => (element['day']=='Thursday' || element['day']=='thursday')).toList();
        if(result.length == 0){
          return null;
        }
        return result[0];
      } else if(dateTime.weekday == 5){
        List<dynamic> result = mData.where((element) => (element['day']=='Friday' || element['day']=='friday')).toList();
        if(result.length == 0){
          return null;
        }
        return result[0];
      } else if(dateTime.weekday == 6){
        List<dynamic> result = mData.where((element) => (element['day']=='Saturday' || element['day']=='saturday')).toList();
        if(result.length == 0){
          return null;
        }
        return result[0];
      }
      return null;
  }
  @override
  Widget build(BuildContext context) {
   return Scaffold(
     appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Select a time slot',
          style: TextStyle(
            fontSize:20.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                    height: 40,
                    padding: const EdgeInsets.only(left:0.0,right: 0.0,bottom: 8.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(bottomLeft:Radius.circular(25.0),bottomRight: Radius.circular(25.0)),
                      color: Theme.of(context).accentColor,
                    ),
                  ),
                Container(
                  padding:EdgeInsets.only(top:12.0,right: 12.0,left: 12.0,bottom: 12.0),
                  margin:EdgeInsets.only(right: 12.0,left: 12.0,bottom: 12.0,top: 0),
                  width: double.maxFinite,
                  decoration: BoxDecoration(            
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          ball(widget.doctorProfileModel.avatar_image, Colors.transparent),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                widget.doctorProfileModel.title,
                                style: TextStyle(

                                  fontSize: 14.0,

                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                width: 200,
                                child:Html(
                                  data:'Address : ${widget.doctorProfileModel.address}, ${widget.doctorProfileModel.city}, ${widget.doctorProfileModel.hashCode}' ,
                                  shrinkWrap: true,
                                  style: {
                                    'p':Style(
                                      textAlign: TextAlign.left,
                                    )
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      SizedBox(height: 15.0,),
                      Container(
                        alignment: Alignment.center,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            GestureDetector(
                              onTap:(){
                                setState(() {
                                  dateTime = dateTime.subtract(Duration(days: 1));
                                  calculateSlots();
                                });
                              },
                              child: Icon(Icons.chevron_left,size: 45,)
                            ),

                            Text(
                              '${Utility.getTodayDate(dateTime)}',
                              style: TextStyle(

                                  fontSize: 16.0,
                                  fontWeight: FontWeight.bold,
                                ),
                            ),
                            GestureDetector(
                              onTap:(){
                                setState(() {
                                  dateTime = dateTime.add(Duration(days: 1));
                                  calculateSlots();
                                });
                              },
                              child: Icon(Icons.chevron_right,size: 45,)
                            ),

                          ],
                        ),
                      ),
                      SizedBox(height: 20.0,),
                      morningList.length>0?Container(
                        width: MediaQuery.of(context).size.width,
                        child: Stack(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width,
                              padding: const EdgeInsets.all(12),
                              margin: const EdgeInsets.only(top: 14),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                border: Border.all(width: 1,color: Colors.grey.withOpacity(0.8)),
                                color: Colors.grey[100].withOpacity(0.4),
                              ),
                              child: Wrap(
                                children: _buildChoiceList(morningList),
                              ),
                            ),
                            Container(
                              padding: const EdgeInsets.only(top: 6,bottom: 6,left: 16,right: 16,),
                              margin: const EdgeInsets.only(left: 30),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                //color: Colors.grey,
                                gradient: LinearGradient(
                                  colors: [Colors.red[200],Colors.blue[200]],
                                  begin: const FractionalOffset(0.0, 0.0),
                                  end: const FractionalOffset(0.5, 0.0),
                                  stops: [0.0,1.0],
                                  tileMode: TileMode.clamp,
                                )
                              ),
                              child: Text(
                                "Morning",
                                style: TextStyle(

                                  fontSize: 10.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ):Container(),
                      afternoonList.length>0?Container(
                        width: MediaQuery.of(context).size.width,
                        margin: const EdgeInsets.only(top: 14),
                        child: Stack(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width,
                              padding: const EdgeInsets.all(12),
                              margin: const EdgeInsets.only(top: 14),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                border: Border.all(width: 1,color: Colors.grey.withOpacity(0.8)),
                                color: Colors.grey[100].withOpacity(0.4),
                              ),

                              child:Wrap(
                                children: _buildChoiceList(afternoonList),
                              ), 
                            ),
                            Container(
                              padding: const EdgeInsets.only(top: 6,bottom: 6,left: 16,right: 16,),
                              margin: const EdgeInsets.only(left: 30),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                //color: Colors.grey,
                                gradient: LinearGradient(
                                  colors: [Colors.lightBlue[200],Colors.lightGreen[200]],
                                  begin: const FractionalOffset(0.0, 0.0),
                                  end: const FractionalOffset(0.5, 0.0),
                                  stops: [0.0,1.0],
                                  tileMode: TileMode.clamp,
                                )
                              ),
                              child: Text(
                                "Afternoon",
                                style: TextStyle(

                                  fontSize: 10.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ):Container(),
                      nightList.length>0?Container(
                        width: MediaQuery.of(context).size.width,
                        margin: const EdgeInsets.only(top: 14),
                        child: Stack(
                          children: <Widget>[
                            Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width,
                              padding: const EdgeInsets.all(12),
                              margin: const EdgeInsets.only(top: 14),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(16),
                                border: Border.all(width: 1,color: Colors.grey.withOpacity(0.8)),
                                color: Colors.grey[100].withOpacity(0.4),
                              ),
                              child:Wrap(
                                children: _buildChoiceList(nightList)
                                  
                              ) 
                            ),
                            Container(
                              padding: const EdgeInsets.only(top: 6,bottom: 6,left: 16,right: 16,),
                              margin: const EdgeInsets.only(left: 30),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(20),
                                //color: Colors.grey,
                                gradient: LinearGradient(
                                  colors: [Colors.yellow[100],Colors.green[200]],
                                  begin: const FractionalOffset(0.0, 0.0),
                                  end: const FractionalOffset(0.5, 0.0),
                                  stops: [0.0,1.0],
                                  tileMode: TileMode.clamp,
                                )
                              ),
                              child: Text(
                                "Evening & Night",
                                style: TextStyle(

                                  fontSize: 10.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ):Container(),

                      (!isGettingTimings && morningList.length==0 && afternoonList.length == 0 && nightList.length == 0)?
                      Container(
                        width: MediaQuery.of(context).size.width,
                        height: 200,
                        padding: const EdgeInsets.only(left: 20,right: 20,top: 10,bottom: 10),
                        margin: const EdgeInsets.only(top: 14),
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(16),
                          border: Border.all(width: 1,color: Colors.grey.withOpacity(0.8)),
                          color: Colors.grey[100].withOpacity(0.4),
                        ),
                        child: Center(child: Text('No slots available for the date',
                          style: TextStyle(
                            color: Theme.of(context).accentColor,
                            fontSize: 24,
                          ),
                          textAlign: TextAlign.center,
                        )),
                      ):
                      Container()
                    ],
                  ),
                ),
                isGettingTimings?LoadingWidget(initialData: true,):Container(),
              ],
            ),

          ],
        ),
      ),
     bottomNavigationBar: BottomAppBar(

       elevation: 0,
       color: Colors.transparent,
       child: (morningList.length==0 && afternoonList.length == 0 && nightList.length == 0)?Container(width: MediaQuery.of(context).size.width,height: 0,):Container(
         padding: EdgeInsets.only(left: 75,right: 75),
           margin:EdgeInsets.all(12.0),
           child: ElevatedButton(

             onPressed: (){
               if(isGettingTimings){
                 return;
               }
               debugPrint("Selected Choice : $selectedChoice");
               if(selectedChoice == ""){
                 MyToast.showToast(context,"Please selecte time slot");
                 return;
               }
               debugPrint("yes");
               debugPrint(dateTime.toString());
               String strDate ='${dateTime.year}-${dateTime.month}-${dateTime.day} ${selectedChoice}';
               debugPrint("strDate : ${strDate}");
               DateTime newDate = new DateFormat("yyyy-MM-dd HH:mm").parse(strDate);
               debugPrint(strDate);
               debugPrint("New date : ${newDate.toString()}");
               Navigator.of(context).pushNamed(DoctorBookSecondeStep.routeName, arguments: [widget.doctorProfileModel,newDate]);
             },

             style: ElevatedButton.styleFrom(
               elevation: 0,
               shape: RoundedRectangleBorder(
                   borderRadius: BorderRadius.circular(30.0)
               ),
               primary: Theme.of(context).accentColor,
             ),

             child:Container(
               alignment: Alignment.center,
               height: 40,
               child:Text(
                 'BOOK',
                 style: TextStyle(

                     fontSize: 14.0,
                     color: Theme.of(context).primaryColor,
                     fontWeight: FontWeight.bold
                 ),
               ),
             ),
           )
       ),
     ),
    );
  }
  Widget ball(String image,Color color){
    return Container(
      height: 80,width: 80.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image??Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
        ),
      )
      ,

    );
  }
  _buildChoiceList(List<String> list) {
    List<Widget> choices = [];
    list.forEach((item) {
      choices.add(
        Container(
          padding: const EdgeInsets.all(2.0),
          child: ChoiceChip(
            label: Text(DateFormat('hh:mm a').format(DateFormat('HH:mm').parse(item))),
            selectedColor: config.Colors().mainColor(1),
            selected: selectedChoice == item,
            onSelected: (selected) {
              setState(() {
                selectedChoice = item;
              });
            },
          ),
        )
      );
    }
    );return choices;
  }
}