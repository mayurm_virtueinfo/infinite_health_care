import 'package:flutter/services.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/main.dart';
import 'package:geolocator/geolocator.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:carousel_slider/carousel_slider.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/ui/home/doctor_profile.dart';
import 'package:infinite_health_care/ui/home/doctors_list.dart';
import 'package:infinite_health_care/ui/home/doctors_near_you.dart';
import 'package:infinite_health_care/ui/home/new_doctors.dart';
import 'package:infinite_health_care/ui/home/new_doctors_list.dart';
import 'package:infinite_health_care/ui/home/popular_doctors_list.dart';
import 'package:infinite_health_care/ui/home/search_doctors_list.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/placeholder_widget.dart';
import 'package:infinite_health_care/widgets/searchWidget.dart';
import 'package:infinite_health_care/models/top.dart' as topModel;
import 'package:infinite_health_care/models/future_banner.dart' as futureBannerModel;

class HomeTab extends StatefulWidget {
  static final String routeName = "HomeTab";

  const HomeTab({Key key}) : super(key: key);
//  final bool androidFusedLocation;

  @override
  _HomeTabState createState() => _HomeTabState();
}

class _HomeTabState extends State<HomeTab> {
  String latitude = '';
  String longitude = '';
  Position _lastKnownPosition;
  Position _currentPosition;
  bool androidFusedLocation = true;
  @override
  void initState() {
    super.initState();
    _initLastKnownLocation();
    _initCurrentLocation();
  }

  @override
  void didUpdateWidget(Widget oldWidget) {
    super.didUpdateWidget(oldWidget);

    setState(() {
      _lastKnownPosition = null;
      _currentPosition = null;
    });

    _initLastKnownLocation().then((_) => _initCurrentLocation());
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  Future<void> _initLastKnownLocation() async {
    Position position;
    // Platform messages may fail, so we use a try/catch PlatformException.
    try {
      position = await Geolocator.getLastKnownPosition(forceAndroidLocationManager:!androidFusedLocation);
    } on PlatformException {
      position = null;
    }

    // If the widget was removed from the tree while the asynchronous platform
    // message was in flight, we want to discard the reply rather than calling
    // setState to update our non-existent appearance.
    if (!mounted) {
      return;
    }

    setState(() {
      _lastKnownPosition = position;
    });
  }

  // Platform messages are asynchronous, so we initialize in an async method.
  _initCurrentLocation() async {
    Position position = await Geolocator.getCurrentPosition(desiredAccuracy: LocationAccuracy.high);
    if (!mounted) {
      return;
    }
    setState(() {
      _currentPosition = position;
    });
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.

    super.dispose();
  }


  @override
  Widget build(BuildContext context) {


    return Scaffold(
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 120,
                  padding: const EdgeInsets.only(left: 20.0, right: 20.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(25.0),
                        bottomRight: Radius.circular(25.0)),
                    color: Theme.of(context).accentColor,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          GestureDetector(
                            onTap: (){
                            },
                            child: Text(
                              'IHC',
                              style: TextStyle(
                                fontSize: 22.0,

                                fontWeight: FontWeight.bold,
                                color: Theme.of(context)
                                    .primaryColor
                                    .withOpacity(0.8),
                              ),
                            ),
                          ),
                          Text(
                            "${appUserModel.first_name} ${appUserModel.last_name}",
                            style: TextStyle(

                              fontSize: 16.0,
                              color: Theme.of(context).primaryColor,
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
                // Top
                Container(
                  margin: const EdgeInsets.only(top: 80.0),
//                  padding: const EdgeInsets.only(left: 30.0, right: 30.0),
                  width: MediaQuery.of(context).size.width,
                  height: 110,
                  child: FutureBuilder(
                    future: ApiRequest.getTop(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData) {
                        topModel.TopListModel dList =
                            topModel.TopListModel.fromSnapshot(snapshot);
                        return ListView.separated(
                          scrollDirection: Axis.horizontal,
                          shrinkWrap: true,
                          primary: false,
                          itemCount: dList.topList.length,
                          separatorBuilder: (context, index) {
                            return SizedBox(height: 4.0);
                          },
                          itemBuilder: (context, index) {
                            return Container(
                              width: MediaQuery.of(context).size.width / 3,
                              child: Column(
                                children: <Widget>[
                                  TextButton(
                                    style: TextButton.styleFrom(
                                    padding: EdgeInsets.all(0),
                                    shape: RoundedRectangleBorder(
                                        borderRadius:
                                        BorderRadius.circular(150)),
                                    ),

                                    onPressed: () {
                                      Navigator.of(context).pushNamed(
                                          DoctorsList.routeName,
                                          arguments: [
                                            "category",
                                            dList.topList[index].id,
                                            dList.topList[index].title
                                          ]);
                                    },
                                    child: ball(
                                        '${dList.topList[index].IconPath}',
                                        Theme.of(context)
                                            .scaffoldBackgroundColor),
                                  ),
                                  Text(
                                    '${dList.topList[index].title}',
                                    style: TextStyle(
                                      fontSize: 12.0,

                                      fontWeight: FontWeight.bold,
                                      color: Theme.of(context).focusColor,
                                    ),
                                  ),
                                ],
                              ),
                            );
                          },
                        );
                      } else if (snapshot.hasError) {
                        MyToast.showToast(context,snapshot.error.toString());
                        return Container();
                      } else {
                        return Container(
                          width: MediaQuery.of(context).size.width,
                          height: 40,
                          child: Center(
                            child: SizedBox(
                              width: 40,
                              height: 40,
                              child: CircularProgressIndicator(
                                strokeWidth: 1.5,
                                valueColor: AlwaysStoppedAnimation<Color>(config.Colors().mainColor(1),),
                              ),
                            ),
                          ),
                        );
                      }
                    },
                  ),
                ),
              ],
            ),
            // Search Bar
            Padding(
              padding: const EdgeInsets.all(12),
              child: SearchBarWidget(
                enabled: false,
                onTap: () {
//                  Navigator.of(context).pushNamed(SearchDoctorsList.routeName);
                  Navigator.of(context).push(
                    PageRouteBuilder(
                      transitionDuration: Duration(milliseconds: 500),
                      pageBuilder: (BuildContext context,
                          Animation<double> animation,
                          Animation<double> secondaryAnimation) {
                        return SearchDoctorsList();
                      },
                      transitionsBuilder: (BuildContext context,
                          Animation<double> animation,
                          Animation<double> secondaryAnimation,
                          Widget child) {
                        return Align(
                          child: FadeTransition(
                            opacity: animation,
                            child: child,
                          ),
                        );
                      },
                    )  ,
                  );
                },
              ),
            ),
            // Future Banner
            Container(
              width: double.infinity,
              margin: EdgeInsets.symmetric(vertical: 6, horizontal: 0),
              child: FutureBuilder(
                future: ApiRequest.getFuture(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
                    futureBannerModel.FutureBannerListModel dList =
                        futureBannerModel.FutureBannerListModel.fromSnapshot(
                            snapshot);
                    final lBanner = dList.futureBannerList;
                    return CarouselSlider(
                      options: CarouselOptions(
                          initialPage: 0
                      ),
                      items: lBanner.asMap().entries.map((entries) {
                        int index = entries.key;
                        futureBannerModel.FutureBanner futureBanner =
                            entries.value;
                        return Builder(
                          builder: (context) {
                            return GestureDetector(
                              onTap: () {
                                Navigator.of(context).pushNamed(
                                    DoctorsList.routeName,
                                    arguments: [
                                      "category",
                                      futureBanner.id,
                                      futureBanner.title
                                    ]);
                              },
                              child: futureBanner.image == ""
                                  ? Container(
                                      height: 200.0,
                                      margin: const EdgeInsets.only(left: 12.0),
                                      decoration: BoxDecoration(
                                        border: Border.all(
                                            width: 1.0,
                                            color:
                                                Colors.grey.withOpacity(0.2)),
                                        borderRadius:
                                            BorderRadius.circular(16.0),
                                        image: DecorationImage(
                                          image: AssetImage(
                                              'images/doctor-productivity.jpg'),
                                          fit: BoxFit.cover,
                                        ),
                                      ),
                                    )
                                  : Container(
                                      child: FancyShimmerImage(
                                        imageUrl: futureBanner.ImagePath,
                                        shimmerBaseColor: Colors.white,
                                        shimmerHighlightColor:
                                            config.Colors().mainColor(1),
                                        shimmerBackColor: Colors.green,
                                        boxFit: BoxFit.cover,
                                      ),
                                    ),
                            );
                          },
                        );
                      }).toList()
                      ,
                    );
                  } else if (snapshot.hasError) {
                    MyToast.showToast(context,snapshot.error.toString());
                    return Container();
                  } else {
                    return Container(
                      width: MediaQuery.of(context).size.width,
                      height: 40,
                      child: Center(
                        child: SizedBox(
                          width: 40,
                          height: 40,
                          child: CircularProgressIndicator(
                            strokeWidth: 1.5,
                            valueColor: AlwaysStoppedAnimation<Color>(config.Colors().mainColor(1),),
                          ),
                        ),
                      ),
                    );
                  }
                },
              )
              ,
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    onPressed: () async{
                      debugPrint("yes");

                    },
                    child: Text(
                      'Doctors nearly you ',
                      style: TextStyle(
                          fontSize: 12.0,

                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).focusColor),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(DoctorsNearYou.routeName,arguments: [latitude,longitude]);
                    },
                    child: Text(
                      'See All',
                      style: TextStyle(
                        fontSize: 12.0,

                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 180,
              alignment: Alignment.center,
              child: FutureBuilder(
                  future: Geolocator.checkPermission(),
                  builder: (BuildContext context,
                      AsyncSnapshot<LocationPermission> snapshot) {
                    if (!snapshot.hasData) {
                      return SizedBox(
                        width: 40,
                        height: 40,
                        child: CircularProgressIndicator(
                          strokeWidth: 1.5,
                          valueColor: AlwaysStoppedAnimation<Color>(config.Colors().mainColor(1),),
                        ),
                      );
                    }

                    if (snapshot.data == LocationPermission.denied) {
                      return const PlaceholderWidget('Access to location denied',
                          'Allow access to the location services for this App using the device settings.');
                    }

                    if((snapshot.hasData && snapshot.data == LocationPermission.whileInUse) || snapshot.hasData && snapshot.data == LocationPermission.always){
                      String lat = '',lng = '';
                      debugPrint("currentPosition : ${_currentPosition.toString()}");
                      debugPrint("lastKnownPosition : ${_lastKnownPosition.toString()}");
                      if(_currentPosition != null && _currentPosition.latitude !=null && _currentPosition.longitude !=null ){
                        debugPrint("1");
                        lat = _currentPosition.latitude.toString();
                        lng = _currentPosition.longitude.toString();
                      }
                      else if(_lastKnownPosition != null && _lastKnownPosition.latitude !=null && _lastKnownPosition.longitude !=null ){
                        debugPrint("2");
                        lat = _lastKnownPosition.latitude.toString();
                        lng = _lastKnownPosition.longitude.toString();
                      }
                      latitude = lat;
                      longitude = lng;
                      debugPrint("mLat : ${latitude} , mLng ${longitude}");
                      return (latitude != "" && longitude != "")?
                      Container(
                        alignment: Alignment.center,
                        height: 130,
                        child: FutureBuilder(
                          future: ApiRequest.postDoctorsNearYou(
                              lat: latitude,
                              lng: longitude,
                              radius: "500",
                              limit: "10",
                              page: "0"),
                          builder: (context, snapshot) {
                            if (snapshot.hasData) {
//                  debugPrint(snapshot.data);
                              Map<String, dynamic> map = snapshot.data;
                              List<dynamic> dataList = map['data'];
                              if(dataList.length == 0) {
                                return Container(
                                    alignment: Alignment.center,
                                    margin: EdgeInsets.only(bottom: 30),
                                    height: 50,
                                    child: Text('No nearly doctors found',style: TextStyle(
                                        fontSize: 16
                                    ),)
                                );
                              }else{
                                debugPrint("Data doctors nearyly your : ${dataList.toString()}");
                                int itemCount = dataList.length<3?dataList.length:3;
                                return ListView.separated(
                                    scrollDirection: Axis.horizontal,
                                    itemBuilder: (context, index) {
                                      Map<String, dynamic> mapItem = dataList[index];
                                      return ItemDoctors(mapItem['id'],
                                          mapItem['originalPath'],
                                          mapItem['title'],
                                          mapItem['degree'],
                                          mapItem['rating']);
                                    },
                                    separatorBuilder: (context, index) {
                                      return SizedBox(
                                        width: 10,
                                      );
                                    },
                                    itemCount: itemCount);
                              }

                            } else if (snapshot.hasError) {
                              return Container(
                                child: Center(
                                  child: Text(snapshot.error.toString()),
                                ),
                              );
                            } else {
                              return SizedBox(
                                width: 40,
                                height: 40,
                                child: CircularProgressIndicator(
                                  strokeWidth: 1.5,
                                  valueColor: AlwaysStoppedAnimation<Color>(config.Colors().mainColor(1),),
                                ),
                              );
                            }
                          },
                        ),
                      ):Container();
                    }else {
                      return Container();
                    }

                  }),
            ),
            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      'New Doctors',
                      style: TextStyle(
                          fontSize: 12.0,

                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).focusColor),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(NewDoctorsList.routeName);
                    },
                    child: Text(
                      'See All',
                      style: TextStyle(
                        fontSize: 12.0,

                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  )
                ],
              ),
            ),
            Container(
              height: 180,
              alignment: Alignment.center,
              child: FutureBuilder(
                future: ApiRequest.getLatestDoctorList(),
                builder: (context, snapshot) {
                  if (snapshot.hasData) {
//                  debugPrint(snapshot.data);
                    Map<String, dynamic> map = snapshot.data;
                    List<dynamic> dataList = map['data'];
                    if(dataList.length == 0) {
                      return Container(
                          alignment: Alignment.center,
                          margin: EdgeInsets.only(bottom: 30),
                          height: 50,
                          child: Text('No new doctors found',style: TextStyle(
                              fontSize: 16
                          ),)
                      );
                    }else{
                      int itemCount = dataList.length<3?dataList.length:3;
                      debugPrint("Data new doctors : ${dataList.toString()}");
                      return ListView.separated(
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            Map<String, dynamic> mapItem = dataList[index];
                            return ItemDoctors(mapItem['doctor_id'],
                                mapItem['doctor_image'],
                                mapItem['doctor_name'],
                                mapItem['degree'],
                                mapItem['rating']);
                          },
                          separatorBuilder: (context, index) {
                            return SizedBox(
                              width: 10,
                            );
                          },
                          itemCount: itemCount);
                    }

                  } else if (snapshot.hasError) {
                    return Container(
                      child: Center(
                        child: Text(snapshot.error.toString()),
                      ),
                    );
                  } else {
                    return SizedBox(
                      width: 40,
                      height: 40,
                      child: CircularProgressIndicator(
                        strokeWidth: 1.5,
                        valueColor: AlwaysStoppedAnimation<Color>(config.Colors().mainColor(1),),
                      ),
                    );
                  }
                },
              ),
            ),

            Container(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  TextButton(
                    onPressed: () {},
                    child: Text(
                      'Popular Doctors',
                      style: TextStyle(
                          fontSize: 12.0,

                          fontWeight: FontWeight.bold,
                          color: Theme.of(context).focusColor),
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      Navigator.of(context).pushNamed(PopularDoctorsList.routeName);
                    },
                    child: Text(
                      'See All',
                      style: TextStyle(
                        fontSize: 12.0,

                        fontWeight: FontWeight.bold,
                        color: Theme.of(context).accentColor,
                      ),
                    ),
                  )
                ],
              ),
            ),
            FutureBuilder(
              future: ApiRequest.getPopularDoctorList(),
              builder: (context, snapshot) {
                if (snapshot.hasData) {
//                  debugPrint(snapshot.data);
                  Map<String, dynamic> map = snapshot.data;
                  List<dynamic> dataList = map['data'];
                  if(dataList.length==0){
                    return Container(
                      alignment: Alignment.center,
                      margin: EdgeInsets.only(bottom: 30),
                      height: 50,
                      child: Text('No popular doctors found',style: TextStyle(
                        fontSize: 16
                      ),)
                    );
                  }else {
                    debugPrint("Data popular doctors : ${dataList.toString()}");
                    int itemCount = dataList.length < 3 ? dataList.length : 3;
                    return Container(
                      alignment: Alignment.center,
                      height: 180,
                      child: ListView.separated(
                          scrollDirection: Axis.horizontal,
                          itemBuilder: (context, index) {
                            Map<String, dynamic> mapItem = dataList[index];
                            return ItemDoctors(mapItem['doctor_id'],
                                mapItem['doctor_image'],
                                mapItem['doctor_name'],
                                mapItem['degree'],
                                mapItem['rating']);
                          },
                          separatorBuilder: (context, index) {
                            return SizedBox(
                              width: 10,
                            );
                          },
                          itemCount: itemCount),
                    );
                  }
                } else if (snapshot.hasError) {
                  return Container(
                    child: Center(
                      child: Text(snapshot.error.toString()),
                    ),
                  );
                } else {
                  return SizedBox(
                    width: 40,
                    height: 40,
                    child: CircularProgressIndicator(
                      strokeWidth: 1.5,
                      valueColor: AlwaysStoppedAnimation<Color>(config.Colors().mainColor(1),),
                    ),
                  );
                }
              },
            )


          ],
        ),
      ),
    );
  }

  String _fusedLocationNote() {
    if (androidFusedLocation) {
      return 'Geolocator is using the Android FusedLocationProvider. This requires Google Play Services to be installed on the target device.';
    }

    return 'Geolocator is using the raw location manager classes shipped with the operating system.';
  }

  List<Widget> getCarouselSliderList(dList) {
    List<Widget> wList = [];
    dList.forEach((Banner banner) {
      wList.add(Container(
        height: 200.0,
        margin: const EdgeInsets.only(left: 12.0),
        decoration: BoxDecoration(
          border: Border.all(width: 1.0, color: Colors.grey.withOpacity(0.2)),
          borderRadius: BorderRadius.circular(16.0),
          image: DecorationImage(
            image: AssetImage('images/doctor-productivity.jpg'),
            fit: BoxFit.cover,
          ),
        ),
      ));
    });
    return wList; //Future.value(wList);
  }

  Widget ball(String image, Color color) {
    return Container(
      height: 80,
      width: 80.0,
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(100.0),
      ),
      child: Center(
        child: image == ""
            ? Image(
                image: AssetImage("images/nurse.png"), //----1
                fit: BoxFit.cover,
              )
            : ClipOval(
                child: FancyShimmerImage(
                  imageUrl: image,
                  shimmerBaseColor: Colors.white,
                  shimmerHighlightColor: config.Colors().mainColor(1),
                  shimmerBackColor: Colors.green,
                ),
              ),
      ),
    );
  }

  Widget ballcard(String image, Color color) {
    print("----image -111: $image");
    return Container(
      height: 60,
      width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image??Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
        ),
      ),
    );
  }

  Widget ItemDoctors(int id,String image, String title, List<dynamic> degrees, String rank) {
    List<String> lstDegree = [];
    if(degrees!=null){
      degrees.forEach((element) {
        lstDegree.add(element['degree_title']);
      });
    }

    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(DoctorProfile.routeName,arguments: ["${id}",title]);
      },
      child: Stack(

        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(top: 20.0),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(8.0),
              boxShadow: [
                BoxShadow(
                    color: Theme.of(context).primaryColor.withOpacity(0.1),
                    offset: Offset(0, 4),
                    blurRadius: 10)
              ],
            ),
            width: 140.0,
            height: 140.0,
            child: Card(
              elevation: 0.2,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(height: 50,),
                  Text(
                    title,
                    textAlign: TextAlign.center,
                    style: TextStyle(

                      fontSize: 10.0,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                  SizedBox(height: 10,),
                  lstDegree.length>0?Text(
                    '( ${lstDegree.join(', ')} )',
                    textAlign: TextAlign.left,
                    style:
                    TextStyle( fontSize: 10.0),
                  ):Container(),
                  SizedBox(height: 10,),
                  (rank==null || rank == '')?Container():Row(
                    mainAxisAlignment: MainAxisAlignment.end,
                    children: <Widget>[
                      Icon(
                        Icons.star,
                        color: Colors.yellow,
                        size: 20,
                      ),
                      Text(
                        rank ?? "",
                        style: TextStyle(

                        ),
                      ),
                      SizedBox(width: 10,)
                    ],
                  )
                ],
              ),
            ),
          ),
          Container(
            width: 140,
            height: 60,
            alignment: Alignment.center,
            child: ballcard(image, Colors.transparent),
          ),
        ],
      ),
    );
  }
}
