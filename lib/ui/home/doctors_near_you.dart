import 'package:flutter/material.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/models/medecine.dart';
import 'package:infinite_health_care/ui/home_screen.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/doctors_card_widget.dart';
import 'package:infinite_health_care/widgets/doctors_near_you_widget.dart';
import 'package:infinite_health_care/widgets/searchWidget.dart';
import 'package:infinite_health_care/models/doctor_near_you_model.dart' as doctorNearYouModel;
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/main.dart';
class DoctorsNearYou extends StatefulWidget {
  static const String routeName = '/DoctorsNearYou';
  final String latitude;
  final String longitude;
  DoctorsNearYou({this.latitude,this.longitude});
  @override
  _DoctorsNearYouState createState() => _DoctorsNearYouState();
}

class _DoctorsNearYouState extends State<DoctorsNearYou> {
  @override
  void initState() {
    super.initState();
  }
  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop(context);
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text("Doctors Near You",
          style: TextStyle(
            fontSize:22.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: Container(
        constraints: BoxConstraints(
          minHeight: MediaQuery.of(context).size.height,
          minWidth: MediaQuery.of(context).size.width
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Container(
                margin: EdgeInsets.all(20),
                decoration: BoxDecoration(
                 color: Colors.transparent,
                ),
                child: FutureBuilder(
                  future: ApiRequest.postDoctorsNearYou(
                      lat: widget.latitude,
                      lng: widget.longitude,
                      radius: "500",
                      limit: "10",
                      page: "0"
                  ),
                  builder: (context, snapshot) {

                    if(snapshot.hasData) {
                      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

                      doctorNearYouModel.DoctorNearYouModelList dList = doctorNearYouModel.DoctorNearYouModelList.fromSnapshot(snapshot);
                      if(dList.doctorsNearYou.length == 0) {
                        return Container(
                          alignment: Alignment.center,
                          width: MediaQuery.of(context).size.width,
                          height: MediaQuery.of(context).size.height-(AppBar().preferredSize.height+50),
                          child: Center(
                            child: Text('No nearly doctors found',style: TextStyle(
                                fontSize: 16
                            ),)
                          ),
                        );
                      }else{
                        return ListView.builder(
                          shrinkWrap: true,
                          primary: false,
                          itemCount: dList.doctorsNearYou.length,

                          itemBuilder: (context, index) {
                            debugPrint("ID : ${dList.doctorsNearYou.elementAt(index).id}");
                            return DoctorsNearYouWidget(
                              doctorsNearYou: dList.doctorsNearYou.elementAt(index),
                            );
                          },
                        );
                      }


                    } else if(snapshot.hasError){
                      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                      MyToast.showToast(context,snapshot.error.toString());
                      return Container();
                    } else{
                      locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                      return LoadingWidget();
                    }
                  }
                  ,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}
