import 'package:flutter/material.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/models/doctor_model.dart' as doctorModel;
import 'package:infinite_health_care/ui/home_screen.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/doctors_card_widget.dart';
import 'package:infinite_health_care/widgets/searchWidget.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/main.dart';
class DoctorsList extends StatefulWidget {
  static const String routeName = '/DoctorsList';
  final int type_id;
  final String type;
  final String title;
  DoctorsList({this.title,this.type,this.type_id});
  @override
  _DoctorsListState createState() => _DoctorsListState();
}

class _DoctorsListState extends State<DoctorsList> {
  @override
  void initState() {
    super.initState();
  }
  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop(context);
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          widget.title??"",
          style: TextStyle(
            fontSize:22.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child:ConstrainedBox(
          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height-(AppBar().preferredSize.height*2)),
          child: Container(
            height: double.infinity,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    margin: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                     color: Colors.transparent,
                    ),
                    child: FutureBuilder(
                      future: ApiRequest.getDoctors(type: widget.type,type_id: widget.type_id),
                      builder: (context, snapshot) {

                        if(snapshot.hasData) {
                          locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                          doctorModel.DoctorModelList dList = doctorModel.DoctorModelList.fromSnapshot(snapshot);

                          if(dList.doctors.length == 0) {
                            return Container(
                              alignment: Alignment.center,
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height-(AppBar().preferredSize.height+50),
                              child: Center(
                                  child: Text('No doctors found',style: TextStyle(
                                      fontSize: 16
                                  ),)
                              ),
                            );
                          }else{
                            return ListView.builder(
                              shrinkWrap: true,
                              primary: false,
                              itemCount: dList.doctors.length,
                              itemBuilder: (context, index) {
                                return DoctorsCardWidget(
                                  title: dList.doctors.elementAt(index).title,
                                  doctors: dList.doctors.elementAt(index),
                                );
                              },
                            );
                          }

                        } else if(snapshot.hasError){
                          locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                          MyToast.showToast(context,snapshot.error.toString());
                          return Container();
                        } else{
                          locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                          return LoadingWidget();
                        }
                      }
                      ,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}
