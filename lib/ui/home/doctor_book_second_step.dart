import 'dart:convert' show base64Encode, json, utf8;
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/main.dart';
import 'package:toast/toast.dart';
import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/style.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:flutter/material.dart';
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/models/doctor_model.dart';
import 'package:infinite_health_care/models/doctor_profile_model.dart';
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/ui/home/consultancy_booked.dart';
import 'package:infinite_health_care/ui/home/credit_card_payment.dart';
import 'package:infinite_health_care/ui/home/doctor_book_first_step.dart';
import 'package:infinite_health_care/ui/home/send_feedback.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/utils/rozarpay_config.dart';
import 'package:infinite_health_care/utils/utility.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';


class DoctorBookSecondeStep extends StatefulWidget {
  static const String routeName = '/DoctorBookSecondeStep';
  DoctorProfileModel doctorProfileModel;
  DateTime bookingDate;
  DoctorBookSecondeStep({this.doctorProfileModel,this.bookingDate});
  @override
  _DoctorBookSecondeStepState createState() => _DoctorBookSecondeStepState();
}

class _DoctorBookSecondeStepState extends State<DoctorBookSecondeStep> {
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();
  int fees ;
  TextEditingController _nameController;
  TextEditingController _emailController;
  TextEditingController _phoneNumberController;
  bool isBookingAppointment = false;
  Razorpay _razorpay;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    fees = widget.doctorProfileModel.fees;
    _nameController = TextEditingController();
    // _nameController.text = widget.doctorProfileModel.title;
    _emailController = TextEditingController();
    // _emailController.text = widget.doctorProfileModel.email;
    _phoneNumberController = TextEditingController();
    // _phoneNumberController.text = widget.doctorProfileModel.mobile;


    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);

    setState(() {

    });
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }


  @override
  void didChangeDependencies() {
    // TODO: implement didChangeDependencies

    super.didChangeDependencies();
  }
  void _handlePaymentSuccess(PaymentSuccessResponse response) async{
    Map<String,dynamic> mapResuponse = Map();
    mapResuponse['orderId']=response.orderId;
    mapResuponse['paymentId']=response.paymentId;
    mapResuponse['signature']=response.signature;
    debugPrint("SUCCESS: rozarpay : ${mapResuponse}");

    /*Navigator.of(context)
        .pushNamed(CreditCardPayment.routeName, arguments: [widget.doctorProfileModel]);*/
    setState(() {
      isBookingAppointment = true;
    });
    try {
      final resultUpdateBooking = await ApiRequest.postUpdateBooking(
          orderId: response.orderId,
          paymentId: response.paymentId,
          signature: response.signature
      );

      debugPrint("Result resultUpdateBooking : ${resultUpdateBooking.toString()}");
      if(resultUpdateBooking['data'] !=null) {
        setState(() {
          isBookingAppointment = false;
        });
        Navigator.of(context).pushNamed(ConsultancyBooked.routeName);
      }
      else{
        setState(() {
          isBookingAppointment = false;
        });
        MyToast.showToast(context,"Error booking your appointment");
      }
    }catch(e){
      debugPrint(e.toString());
      setState(() {
        isBookingAppointment = false;
      });
      MyToast.showToast(context,"Error updating your appointment : ${e.toString()}");
    }

  }

  void _handlePaymentError(PaymentFailureResponse response) {
    debugPrint("ERROR fail: ${response.code.toString()} : ${response.message}");
    final msgJson = json.decode(response.message);
    final error = msgJson['error'];
    if(error != null){
      MyToast.showToast(context,error['description'],length: 4);
    }

    setState(() {
      isBookingAppointment = false;
    });
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    MyToast.showToast(context, "EXTERNAL_WALLET: " + response.walletName);
    debugPrint("EXTERNAL_WALLET : ${response.walletName}");
    setState(() {
      isBookingAppointment = false;
    });

  }

  @override
  Widget build(BuildContext context) {

    _nameController.text = "${appUserModel.first_name} ${appUserModel.last_name}";
    _emailController.text = appUserModel.email;
    _phoneNumberController.text = appUserModel.mobile??'';
    
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
          onPressed: () {
            Navigator.of(context).pop();
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'Select a time slot',
          style: TextStyle(
            fontSize: 20.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Stack(
              children: <Widget>[
                Container(
                  height: 40,
                  padding:
                      const EdgeInsets.only(left: 0.0, right: 0.0, bottom: 8.0),
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                        bottomLeft: Radius.circular(25.0),
                        bottomRight: Radius.circular(25.0)),
                    color: Theme.of(context).accentColor,
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(
                      top: 20.0, right: 12.0, left: 12.0, bottom: 12.0),
                  margin: EdgeInsets.only(
                      right: 12.0, left: 12.0, bottom: 12.0, top: 0),
                  width: double.maxFinite,
                  decoration: BoxDecoration(
                    color: Theme.of(context).primaryColor,
                    borderRadius: BorderRadius.circular(20.0),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          ball(widget.doctorProfileModel.avatar_image, Colors.transparent),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: <Widget>[
                              Text(
                                widget.doctorProfileModel.title,
                                style: TextStyle(

                                  fontSize: 14.0,
                                  fontWeight: FontWeight.bold,
                                ),
                              ),
                              Container(
                                width: 200,
                                child: Html(
                                  data:'Mobile : ${widget.doctorProfileModel.mobile}',
                                  shrinkWrap: true,
                                  style: {
                                    'p':Style(
                                      textAlign: TextAlign.left,
                                    )
                                  },
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                      SizedBox(
                        height: 20.0,
                        child: Center(
                          child: Container(
                            height: 2.0,
                            color: Colors.grey.withOpacity(0.1),
                          ),
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "DATE & Time",
                                  style: TextStyle(

                                    fontSize: 14.0,
                                    color: Colors.grey,
                                  ),
                                ),
                                SizedBox(
                                  height: 15.0,
                                ),
                                Text(
                                  Utility.getFormatDate(widget.bookingDate),
                                  style: TextStyle(

                                      fontSize: 14.0,
                                      fontWeight: FontWeight.bold),
                                )
                              ],
                            ),
                            SizedBox(
                              height: 60.0,
                              width: 2,
                              child: Center(
                                child: Container(
                                  height: 60.0,
                                  color: Colors.grey.withOpacity(0.1),
                                ),
                              ),
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                  "Consultation Fees",
                                  style: TextStyle(

                                    fontSize: 14.0,
                                    color: Colors.grey,
                                  ),
                                ),
                                SizedBox(
                                  height: 15.0,
                                ),
                                Text(
                                  "\₹ ${widget.doctorProfileModel.fees}",
                                  style: TextStyle(

                                    fontSize: 14.0,
                                    fontWeight: FontWeight.bold,
                                  ),
                                ),
                              ],
                            )
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                        child: Center(
                          child: Container(
                            height: 2.0,
                            color: Colors.grey.withOpacity(0.1),
                          ),
                        ),
                      ),
                      Container(
                        child: Form(
                          key: _formKey,
                        /*  initialValue: {

                          },
                          autovalidateMode:AutovalidateMode.always,*/
                          child: Column(
                            children: <Widget>[
                              Container(

                                margin: const EdgeInsets.only(top: 12.0),
                                padding: const EdgeInsets.only(
                                    left: 12.0, right: 12.0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1.5, color: Color(0xdddddddd)),
                                  borderRadius: BorderRadius.circular(12.0),
                                  color: Colors.grey[100].withOpacity(0.4),
                                ),
                                child: TextFormField(
                                  controller: _nameController,
                                  maxLines: 1,
                                  validator: (text){
                                    if(text.length>70){
                                      return "Invalid length";
                                    }
                                    if(text.length == 0){
                                      return "Name is required";
                                    }
                                    return null;
                                  },
                                  // attribute: "Full Name",
                                  // initialValue: '', //for testing
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(0),
                                    hintText: "Name",
                                    hintStyle: TextStyle(fontFamily: 'Poppins'),
                                    border: InputBorder.none,
                                  ),

                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 12.0),
                                padding: const EdgeInsets.only(
                                    left: 12.0, right: 12.0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1.5, color: Color(0xdddddddd)),
                                  borderRadius: BorderRadius.circular(12.0),
                                  color: Colors.grey[100].withOpacity(0.4),
                                ),
                                child: TextFormField(
                                  controller: _emailController,
                                  maxLines: 1,
                                  // attribute: "Email",
                                  // initialValue: '', //for testing
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(0),
                                    hintText: "E-mail",
                                    hintStyle: TextStyle(fontFamily: 'Poppins'),
                                    border: InputBorder.none,
                                  ),
                                  validator: (text){
                                    if(text.length>70){
                                      return "Invalid length";
                                    }
                                    if(text.length == 0){
                                      return "Email is required";
                                    }
                                    return null;
                                  },

                                ),
                              ),
                              Container(
                                margin: const EdgeInsets.only(top: 12.0),
                                padding: const EdgeInsets.only(
                                    left: 12.0, right: 12.0),
                                decoration: BoxDecoration(
                                  border: Border.all(
                                      width: 1.5, color: Color(0xdddddddd)),
                                  borderRadius: BorderRadius.circular(12.0),
                                  color: Colors.grey[100].withOpacity(0.4),
                                ),
                                child: TextFormField(
                                  controller: _phoneNumberController,
                                  maxLines: 1,
                                  validator: (text){
                                    if(text.length == 0){
                                      return "Phone number is required";
                                    }
                                    return null;
                                  },
                                  // attribute: "phone Number",
                                  // initialValue: '',
                                  //for testing
                                  decoration: InputDecoration(
                                    contentPadding: EdgeInsets.all(0),
                                    border: InputBorder.none,
                                    hintText: "Phone Number",
                                    hintStyle: TextStyle(fontFamily: 'Poppins'),
//                                    prefixText: "+",
                                  ),
                                  keyboardType: TextInputType.number,

                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                      SizedBox(
                        height: 25.0,
                      ),
                      Container(
                        child: Text(
                          "By Booking this appointment you agree to the T&C",
                          style: TextStyle(
                              color: Colors.grey,
                              fontSize: 11.0,
                              fontWeight: FontWeight.bold,
                              fontFamily: 'Poppins'),
                        ),
                      ),
                    ],
                  ),
                ),
                isBookingAppointment?LoadingWidget(initialData: true,):Container(),
              ],
            ),
          ],
        ),
      ),
      bottomNavigationBar: BottomAppBar(
        elevation: 0,
        color: Colors.transparent,
        child: Container(
            padding: EdgeInsets.only(left: 75,right: 75),
            margin:EdgeInsets.all(12.0),
            child: ElevatedButton(

              style: ElevatedButton.styleFrom(
                shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(30.0)
                ),
                primary: Theme.of(context).accentColor,
                elevation: 0,
              ),

              onPressed: () async{
                if(isBookingAppointment){
                  return;
                }
                if (_formKey.currentState.validate()) {


                  String doctorId = '${widget.doctorProfileModel.id}';
                  String patientId = appUserModel.id;
                  String date = Utility.getBookingDate(widget.bookingDate);
                  String time = Utility.getBookingTime(widget.bookingDate);
                  int amount = fees*100;
                  setState(() {
                    isBookingAppointment = true;
                  });
                  try {

                    Map<String,dynamic> mParams = {
                      "amount": amount,
                      "currency": "INR",
                      "receipt": "",
                      "payment_capture": 1,
                      "notes": {
                        "doctor_id": doctorId,
                        "patient_id": patientId,
                        "date": date,
                        "time": time
                      }
                    };
                    final resultCreateOrder = await ApiRequest.postCreateRozarPayOrder(params: mParams);
                    String order_id = resultCreateOrder['id'];
                    debugPrint("Result Create Order : ${resultCreateOrder.toString()}");
                    final resultBookAppointment = await ApiRequest.postBookAppointment(
                        order_id: order_id,
                        doctor_id: doctorId,
                        patient_id: patientId,
                        date: date,
                        time: time,
                        fees: '$fees'
                    );

                    debugPrint("Result Result Book Appointment : ${resultBookAppointment.toString()}");

                    debugPrint("amount : ${amount}");
                    var options = {
                      'order_id':order_id,
                      'key': RozarpayConfig.appKey,
                      'amount': amount,
                      "currency": "INR",
                      'name': '${appUserModel.first_name} ${appUserModel.last_name}',
                      'description': '${widget.doctorProfileModel.description}',
                      'prefill': {'contact': '${appUserModel.mobile}', 'email': '${appUserModel.email}'},
                      'external': {
                        'wallets': []//['paytm']
                      }
                    };
                    _razorpay.open(options);

                  }catch(e){
                    debugPrint("Error booking your appointment : ${e.toString()}");
                    setState(() {
                      isBookingAppointment = false;
                    });
                    MyToast.showToast(context, "Error booking your appointment : ${e.toString()}");
                  }
                  return;
                }},

              child:Container(
                alignment: Alignment.center,
                height: 40,
                child:Text(
                  'BOOK',
                  style: TextStyle(

                      fontSize: 14.0,
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold
                  ),
                ),
              ),
            )
        ),
      ),
    );
  }

  Widget ball(String image, Color color) {
    return Container(
      height: 60,width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image??Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
        ),
      )
      ,

    );
  }
}
