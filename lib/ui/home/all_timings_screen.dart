import 'package:flutter/material.dart';
import 'package:infinite_health_care/widgets/weekday_slot_widget.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/bloc/duration/duration_bloc.dart';
import 'package:infinite_health_care/bloc/duration/duration_bloc_event.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/bloc/weekdata/week_data_bloc.dart';
import 'package:infinite_health_care/bloc/weekdata/week_data_event.dart';
import 'package:infinite_health_care/models/user_model.dart';
import 'package:infinite_health_care/utils/app_style.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;

class AllTimingsScreen extends StatefulWidget {
  static const String routeName = '/AllTimingsScreen';
  String doctor_id;
  AllTimingsScreen({this.doctor_id});
  @override
  _AllTimingsScreenState createState() => _AllTimingsScreenState();
}

class _AllTimingsScreenState extends State<AllTimingsScreen> {

  WeekDataBloc weekDataBloc = WeekDataBloc();
  DurationBloc durationBloc = DurationBloc();
  LoadingBloc loadingBloc = LoadingBloc();

  bool isValidTimeMonday = false;
  bool isValidTimeTuesday = false;
  bool isValidTimeWednesday = false;
  bool isValidTimeThursday = false;
  bool isValidTimeFriday = false;
  bool isValidTimeSaturday = false;
  bool isValidTimeSunday = false;

  bool isValidSecondTimeMonday = false;
  bool isValidSecondTimeTuesday = false;
  bool isValidSecondTimeWednesday = false;
  bool isValidSecondTimeThursday = false;
  bool isValidSecondTimeFriday = false;
  bool isValidSecondTimeSaturday = false;
  bool isValidSecondTimeSunday = false;


  String defaultVal = '00:00';

  bool chkValMonday = false;
  bool chkValTueday = false;
  bool chkValWednesday = false;
  bool chkValThursday = false;
  bool chkValFriday = false;
  bool chkValSaturday = false;
  bool chkValSunday = false;
  String selTextStartMonday = '00:00';
  String selTextStartTuesday = '00:00';
  String selTextStartWednesday = '00:00';
  String selTextStartThursday = '00:00';
  String selTextStartFriday = '00:00';
  String selTextStartSaturday = '00:00';
  String selTextStartSunday = '00:00';
  String selTextEndMonday = '00:00';
  String selTextEndTuesday = '00:00';
  String selTextEndWednesday = '00:00';
  String selTextEndThursday = '00:00';
  String selTextEndFriday = '00:00';
  String selTextEndSaturday = '00:00';
  String selTextEndSunday = '00:00';

  String selTextSecondStartMonday = '00:00';
  String selTextSecondStartTuesday = '00:00';
  String selTextSecondStartWednesday = '00:00';
  String selTextSecondStartThursday = '00:00';
  String selTextSecondStartFriday = '00:00';
  String selTextSecondStartSaturday = '00:00';
  String selTextSecondStartSunday = '00:00';
  String selTextSecondEndMonday = '00:00';
  String selTextSecondEndTuesday = '00:00';
  String selTextSecondEndWednesday = '00:00';
  String selTextSecondEndThursday = '00:00';
  String selTextSecondEndFriday = '00:00';
  String selTextSecondEndSaturday = '00:00';
  String selTextSecondEndSunday = '00:00';

  String selTextDuration = '00';
  @override
  void initState() {

    getDoctorTimings();
    super.initState();
  }

  Map<String,dynamic>  mapMonday = Map();
  Map<String,dynamic> mapTuesday = Map();
  Map<String,dynamic> mapWednesday = Map();
  Map<String,dynamic> mapThursday = Map();
  Map<String,dynamic> mapFriday = Map();
  Map<String,dynamic> mapSaturday = Map();
  Map<String,dynamic> mapSunday = Map();

  List<dynamic> listWeeklyData;
  void getDoctorTimings()async{
    loadingBloc.loadingEventSink.add(LoadingEvent());
    try {
      final result = await ApiRequest.getDoctorTimings(doctor_id: widget.doctor_id);
      List data = result['data'];
      if (data != null) {
        weekDataBloc.weekDataEventSink.add(WeekDataEvent(weekData: data));
      }
      loadingBloc.loadingEventSink.add(LoadedEvent());

    }catch(e){
      loadingBloc.loadingEventSink.add(LoadedEvent());
      MyToast.showToast(context,'Error getting slots : ${e.toString()}');
    }
  }
  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    super.dispose();
  }


  final String firstStartTime = 'first_start_time';
  final String firstEndTime = 'first_end_time';

  final String secondStartTime = 'second_start_time';
  final String secondEndTime = 'second_end_time';

  final String monday = 'monday';
  final String tuesday = 'tuesday';
  final String wednesday = 'wednesday';
  final String thursday = 'thursday';
  final String friday = 'friday';
  final String saturday = 'saturday';
  final String sunday = 'sunday';
  final String status = 'status';

  @override
  Widget build(BuildContext context) {
    debugPrint("---refreshed");
    BoxDecoration enableDecoration =  BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)));

    BoxConstraints slotsConstraints = BoxConstraints(
        minWidth: MediaQuery.of(context).size.width, minHeight: 50);

    BoxDecoration slotsDecoration = BoxDecoration(
        color: Theme.of(context).primaryColor,
        borderRadius: BorderRadius.all(Radius.circular(10)));

    BoxDecoration disableDecoration = BoxDecoration(
        color: Colors.grey,
        borderRadius: BorderRadius.all(Radius.circular(10)));

    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color: Theme.of(context).primaryColor),
          onPressed: () {
            Navigator.of(context).pop(context);
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          'All Timings',
          style: TextStyle(
            fontSize:22.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: ConstrainedBox(
          constraints: BoxConstraints(
              minWidth: MediaQuery.of(context).size.width,
              minHeight: MediaQuery.of(context).size.height),
          child: Stack(
            children: [
              StreamBuilder(
                stream: weekDataBloc.weekDataStream,
                builder: (context, snapshot) {
                  if(snapshot.hasData){
                    final result = snapshot.data;
                    listWeeklyData = result as List;
                    debugPrint("stream result : ${listWeeklyData}");
                    listWeeklyData.forEach((element) {
                      if(element['day'] == 'monday' || element['day'] == 'Monday'){
                        mapMonday = element;
                        selTextDuration = '${mapMonday['duration']}';

                      }else if(element['day'] == 'tuesday' || element['day'] == 'Tuesday'){
                        mapTuesday = element;
                        selTextDuration = '${mapTuesday['duration']}';
                      }else if(element['day'] == 'wednesday' || element['day'] == 'Wednesday'){
                        mapWednesday = element;
                        selTextDuration = '${mapWednesday['duration']}';
                      }else if(element['day'] == 'thursday' || element['day'] == 'Thursday'){
                        mapThursday = element;
                        selTextDuration = '${mapThursday['duration']}';
                      }else if(element['day'] == 'friday' || element['day'] == 'Friday'){
                        mapFriday = element;
                        selTextDuration = '${mapFriday['duration']}';
                      }else if(element['day'] == 'saturday' || element['day'] == 'Saturday'){
                        mapSaturday = element;
                        selTextDuration = '${mapSaturday['duration']}';
                      }else if(element['day'] == 'sunday' || element['day'] == 'Sunday'){
                        mapSunday = element;
                        selTextDuration = '${mapSunday['duration']}';
                      }
                      durationBloc.durationEventSink.add(DurationBlocEvent(duration: selTextDuration));
                    });
                    return Container(
                      child: Column(
                        children: <Widget>[
                          Column(
                            children: <Widget>[
                              // header section duration
                              /*Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.all(10),
                                constraints: BoxConstraints(
                                    minWidth: MediaQuery.of(context).size.width,
                                    minHeight: 50),
                                decoration: BoxDecoration(
                                    color: Theme.of(context).primaryColor,
                                    borderRadius: BorderRadius.all(Radius.circular(10))),
                                child: Row(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    *//*Expanded(
                                      flex: 2,
                                      child: Container(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          "Duration",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Theme.of(context).accentColor,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),*//*
                                    Expanded(
                                      flex: 3,
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Column(
                                          crossAxisAlignment: CrossAxisAlignment.start,
                                          children: [
                                            Container(
                                              padding: EdgeInsets.only(left: 10, right: 10),
                                              constraints: BoxConstraints(
                                                  minWidth: MediaQuery.of(context).size.width,
                                                  minHeight: 40),
                                              decoration: BoxDecoration(
                                                  color: Theme.of(context).primaryColor,
                                                  border: Border.all(
                                                      color: Theme.of(context).accentColor,
                                                      width: 0.5),
                                                  borderRadius:
                                                  BorderRadius.all(Radius.circular(5))),
                                              child: Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                children: [

                                                  Row(
                                                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                                    children: [
                                                      StreamBuilder(
                                                        stream: durationBloc.durationStream,
                                                        builder:  (context, snapshot) {
                                                          if(snapshot.hasData){
                                                            return Text(
                                                              '${snapshot.data} MIN',
                                                              style: TextStyle(
                                                                  color: Colors.black, fontSize: 14),
                                                            );
                                                          }else{
                                                            return Text(
                                                              '${snapshot.data} MIN',
                                                              style: TextStyle(
                                                                  color: Colors.black, fontSize: 14),
                                                            );
                                                          }
                                                        },
                                                      ),
                                                      *//*Icon(
                                                        Icons.keyboard_arrow_down,
                                                        color: Colors.grey,
                                                        size: 20,
                                                      )*//*
                                                    ],
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        alignment: Alignment.center,
                                        child: Container(),
                                      ),
                                    ),
                                  ],
                                ),
                              ),*/
                              // header section
                              Container(
                                padding: EdgeInsets.all(10),
                                margin: EdgeInsets.all(10),
                                constraints: BoxConstraints(
                                    minWidth: MediaQuery.of(context).size.width,
                                    minHeight: 50),
                                decoration: BoxDecoration(
                                    color: Theme.of(context).primaryColor,
                                    borderRadius: BorderRadius.all(Radius.circular(10))),
                                child: Row(
                                  children: [
                                    Expanded(
                                      flex: 2,
                                      child: Container(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          "Week Day",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Theme.of(context).accentColor,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Container(
                                        margin: EdgeInsets.only(left: 10),
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          "Start Time",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Theme.of(context).accentColor,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                    Expanded(
                                      flex: 3,
                                      child: Container(
                                        alignment: Alignment.centerLeft,
                                        child: Text(
                                          "End Time",
                                          textAlign: TextAlign.center,
                                          style: TextStyle(
                                              color: Theme.of(context).accentColor,
                                              fontSize: 12,
                                              fontWeight: FontWeight.bold),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ],
                          ),
                          // Monday section
                          WeekdaySlotWidget(
                            initialData: mapMonday,
                            isValidTime: (value){
                              isValidTimeMonday = value;
                              debugPrint("isValidTime Monday : ${value}");
                            },

                            weekDayTitle: "Monday",

                            onSelectEndTime:(text){
                              debugPrint("onSelectEndTime Monday : ${text}");
                              selTextEndMonday = text;
                            } ,
                            onSelectStartTime:(text){
                              debugPrint("onSelectStartTime Monday : ${text}");
                              selTextStartMonday = text;
                            },
                            onChange: (value){
                              debugPrint("onChange Monday : ${value}");
                              chkValMonday = value;
                            } ,
                            isValidSecondTime: (value){
                              isValidSecondTimeMonday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndMonday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartMonday = text;
                            },
                          ),
                          SizedBox(height: 10,),
                          // Tuesday section
                          WeekdaySlotWidget(
                            initialData: mapTuesday,
                            isValidTime: (value){
                              isValidTimeTuesday = value;
                              debugPrint("isValidTime Tuesday : ${value}");
                            },
                            weekDayTitle: "Tuesday",

                            onSelectEndTime:(text){
                              debugPrint("onSelectEndTime Tuesday : ${text}");
                              selTextEndTuesday = text;
                            } ,
                            onSelectStartTime:(text){
                              debugPrint("onSelectStartTime Tuesday : ${text}");
                              selTextStartTuesday = text;
                            },
                            onChange: (value){
                              debugPrint("onChange Tuesday : ${value}");
                              chkValTueday = value;
                            } ,

                            isValidSecondTime: (value){
                              isValidSecondTimeTuesday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndTuesday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartTuesday = text;
                            },
                          ),
                          SizedBox(height: 10,),
                          // Wednesday section
                          WeekdaySlotWidget(
                            initialData: mapWednesday,
                            isValidTime: (value){
                              isValidTimeWednesday = value;
                              debugPrint("isValidTime Wednesday : ${value}");
                            },
                            weekDayTitle: "Wednesday",

                            onSelectEndTime:(text){
                              debugPrint("onSelectEndTime Wednesday : ${text}");
                              selTextEndWednesday = text;
                            } ,
                            onSelectStartTime:(text){
                              debugPrint("onSelectStartTime Wednesday : ${text}");
                              selTextStartWednesday = text;
                            },
                            onChange: (value){
                              debugPrint("onChange Wednesday : ${value}");
                              chkValWednesday = value;
                            } ,
                            isValidSecondTime: (value){
                              isValidSecondTimeWednesday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndWednesday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartWednesday = text;
                            },
                          ),
                          SizedBox(height: 10,),
                          // Thursday section
                          WeekdaySlotWidget(
                            initialData: mapThursday,
                            isValidTime: (value){
                              isValidTimeThursday = value;
                              debugPrint("isValidTime Thursday : ${value}");
                            },
                            weekDayTitle: "Thursday",

                            onSelectEndTime:(text){
                              debugPrint("onSelectEndTime Thursday : ${text}");
                              selTextEndThursday = text;
                            } ,
                            onSelectStartTime:(text){
                              debugPrint("onSelectStartTime Thursday : ${text}");
                              selTextStartThursday = text;
                            },
                            onChange: (value){
                              debugPrint("onChange Thursday : ${value}");
                              chkValThursday = value;
                            } ,
                            isValidSecondTime: (value){
                              isValidSecondTimeThursday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndThursday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartThursday = text;
                            },
                          ),
                          SizedBox(height: 10,),
                          // Friday section
                          WeekdaySlotWidget(
                            initialData: mapFriday,
                            isValidTime: (value){
                              isValidTimeFriday = value;
                              debugPrint("isValidTime Friday : ${value}");
                            },
                            weekDayTitle: "Friday",

                            onSelectEndTime:(text){
                              debugPrint("onSelectEndTime Friday : ${text}");
                              selTextEndFriday = text;
                            } ,
                            onSelectStartTime:(text){
                              debugPrint("onSelectStartTime Friday : ${text}");
                              selTextStartFriday = text;
                            },
                            onChange: (value){
                              debugPrint("onChange Friday : ${value}");
                              chkValFriday = value;
                            } ,
                            isValidSecondTime: (value){
                              isValidSecondTimeFriday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndFriday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartFriday = text;
                            },
                          ),
                          SizedBox(height: 10,),
                          // Saturday section
                          WeekdaySlotWidget(
                            initialData: mapSaturday,
                            isValidTime: (value){
                              isValidTimeSaturday = value;
                              debugPrint("isValidTime Saturday : ${value}");
                            },
                            weekDayTitle: "Saturday",

                            onSelectEndTime:(text){
                              debugPrint("onSelectEndTime Saturday : ${text}");
                              selTextEndSaturday = text;
                            } ,
                            onSelectStartTime:(text){
                              debugPrint("onSelectStartTime Saturday : ${text}");
                              selTextStartSaturday = text;
                            },
                            onChange: (value){
                              debugPrint("onChange Saturday : ${value}");
                              chkValSaturday = value;
                            } ,
                            isValidSecondTime: (value){
                              isValidSecondTimeSaturday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndSaturday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartSaturday = text;
                            },
                          ),
                          SizedBox(height: 10,),
                          // Sunday section
                          WeekdaySlotWidget(
                            initialData: mapSunday,
                            isValidTime: (value){
                              isValidTimeSunday = value;
                              debugPrint("isValidTime Sunday : ${isValidTimeSunday}");
                            },
                            weekDayTitle: "Sunday",

                            onSelectEndTime:(text){
                              selTextEndSunday = text;
                              debugPrint("onSelectEndTime : ${text}");
                            } ,
                            onSelectStartTime:(text){
                              selTextStartSunday = text;
                              debugPrint("onSelectStartTime : ${text}");
                            },
                            onChange: (value){
                              chkValSunday = value;
                              debugPrint("onChange : ${value}");
                            } ,

                            isValidSecondTime: (value){
                              isValidSecondTimeSunday = value;
                              debugPrint("Second : isValidTime Monday : ${value}");
                            },
                            onSelectSecondEndTime:(text){
                              debugPrint("Second : onSelectEndTime Monday : ${text}");
                              selTextSecondEndSunday = text;
                            } ,
                            onSelectSecondStartTime:(text){
                              debugPrint("Second : onSelectStartTime Monday : ${text}");
                              selTextSecondStartSunday = text;
                            },
                          ),
                          SizedBox(height: 20,),

                          SizedBox(height: 20,),
                        ],
                      ),
                    );
                  }else{
                    return Container();
                  }
                },
              ),
              StreamBuilder(
                stream: loadingBloc.loadingStream,
                builder: (context, snapshot) {
                  if(snapshot.hasData && snapshot.data){
                    return Container(
                        width: MediaQuery.of(context).size.width,
                        height: MediaQuery.of(context).size.height,
                        color: Colors.white.withOpacity(0.7),
                        child: Center(
                          child: SizedBox(
                            width: 40,
                            height: 40,
                            child: CircularProgressIndicator(
                              strokeWidth: 1.5,
                              valueColor: AlwaysStoppedAnimation<Color>(config.Colors().mainColor(1),),
                            ),
                          ),
                        ));
                  }else{
                    return Container();
                  }
                },
              ),
            ],
          ),
        ),
      ),
    );
  }
//before remove
}
