import 'package:flutter/material.dart';
import 'package:infinite_health_care/main.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/new_doctors_card_widget.dart';
import 'package:infinite_health_care/widgets/searchWidget.dart';
class PopularDoctorsList extends StatefulWidget {
  static const String routeName = '/PopularDoctorsList';
  @override
  _PopularDoctorsListState createState() => _PopularDoctorsListState();
}

class _PopularDoctorsListState extends State<PopularDoctorsList> {
  @override
  void initState() {
    super.initState();
  }
  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.only(bottomLeft:Radius.circular(16.0),bottomRight: Radius.circular(16.0)),
        ),
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop(context);
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text(
          "Popular Doctors",
          style: TextStyle(
            fontSize:22.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: SingleChildScrollView(
        child:ConstrainedBox(
          constraints: BoxConstraints(maxHeight: MediaQuery.of(context).size.height),
          child: Container(
            height: double.infinity,
            child: Column(
              children: <Widget>[
                Expanded(
                  child: Container(
                    padding: EdgeInsets.all(20),
                    decoration: BoxDecoration(
                     color: Colors.transparent,
                    ),
                    child: FutureBuilder(
                      future: ApiRequest.getPopularDoctorList(),
                      builder: (context, snapshot) {

                        if(snapshot.hasData) {
                          locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());

                          Map<String, dynamic> map = snapshot.data;
                          List<dynamic> dataList = map['data'];
                          debugPrint("Data new doctors : ${dataList.toString()}");
                          if(dataList.length == 0) {
                            return Container(
                                alignment: Alignment.center,
                                margin: EdgeInsets.only(bottom: 30),
                                height: 50,
                                child: Text('No popular doctors found',style: TextStyle(
                                    fontSize: 16
                                ),)
                            );
                          }else{
                            return ListView.separated(
                              shrinkWrap: true,
                              primary: false,
                              itemCount: dataList.length,
                              separatorBuilder: (context, index) {
                                return SizedBox(height: 4.0);
                              },
                              itemBuilder: (context, index) {
                                Map<String,dynamic> mapValue = dataList[index];
                                // debugPrint("value : ${dataList[index]}");
                                return NewDoctorsCardWidget(
                                  title: mapValue['doctor_name'],
                                  doctors: mapValue,
                                );
                              },
                            );
                          }


                        } else if(snapshot.hasError){
                          locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
                          MyToast.showToast(context,snapshot.error.toString());
                          return Container();
                        } else{
                          locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
                          return LoadingWidget();
                        }
                      }
                      ,
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

}
