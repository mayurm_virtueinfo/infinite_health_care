import 'package:flutter/material.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/models/medecine.dart';
import 'package:infinite_health_care/ui/home_screen.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/doctors_card_widget.dart';
import 'package:infinite_health_care/widgets/doctors_near_you_widget.dart';
import 'package:infinite_health_care/widgets/searchWidget.dart';
import 'package:infinite_health_care/models/doctor_near_you_model.dart' as doctorNearYouModel;
class NewDoctors extends StatefulWidget {
  static const String routeName = '/NewDoctors';
  final String latitude;
  final String longitude;
  NewDoctors({this.latitude,this.longitude});
  @override
  _NewDoctorsState createState() => _NewDoctorsState();
}

class _NewDoctorsState extends State<NewDoctors> {
  @override
  void initState() {
    super.initState();
  }
  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leading: IconButton(
          icon: Icon(Icons.arrow_back, color:Theme.of(context).primaryColor ),
          onPressed: (){
//            Navigator.of(context).pushNamed(HomeScreen.routeName, arguments:[widget.currentUser.name,widget.currentUser.phoneNumber]);
            Navigator.of(context).pop(context);
          },
        ),
        backgroundColor: Theme.of(context).accentColor,
        title: Text("New Doctors",
          style: TextStyle(
            fontSize:22.0,

            fontWeight: FontWeight.bold,
            color: Theme.of(context).primaryColor,
          ),
        ),

      ),
      body: Container(
        constraints: BoxConstraints(
          minHeight: MediaQuery.of(context).size.height,
          minWidth: MediaQuery.of(context).size.width
        ),
        child: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Stack(
                children: <Widget>[
                   Container(
                    height: 20,
                    padding: const EdgeInsets.only(top:0,left:12.0,right: 12.0),
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(bottomLeft:Radius.circular(25.0),bottomRight: Radius.circular(25.0)),
                      color: Theme.of(context).accentColor,
                    ),
                    ),
                  Padding(
                    padding: const EdgeInsets.only(top: 0.0,left: 12.0,right: 12.0),
                    child:SearchBarWidget(),
                  ),
                ],
              ),
              Container(
                decoration: BoxDecoration(
                 color: Colors.transparent,
                ),
                child: FutureBuilder(
                  future: ApiRequest.postDoctorsNearYou(
                      lat: widget.latitude,
                      lng: widget.longitude,
                      radius: "500",
                      limit: "10",
                      page: "0"
                  ),
                  builder: (context, snapshot) {

                    if(snapshot.hasData) {


                      doctorNearYouModel.DoctorNearYouModelList dList = doctorNearYouModel.DoctorNearYouModelList.fromSnapshot(snapshot);


                      return ListView.separated(
                        shrinkWrap: true,
                        primary: false,
                        itemCount: dList.doctorsNearYou.length,
                        separatorBuilder: (context, index) {
                          return SizedBox(height: 4.0);
                        },
                        itemBuilder: (context, index) {
                          debugPrint("ID : ${dList.doctorsNearYou.elementAt(index).id}");
                          return DoctorsNearYouWidget(
                            doctorsNearYou: dList.doctorsNearYou.elementAt(index),
                          );
                        },
                      );
                    } else if(snapshot.hasError){
                      MyToast.showToast(context,snapshot.error.toString());
                      return Container();
                    } else{
                      return Container(
                        constraints: BoxConstraints(
                          minWidth: MediaQuery.of(context).size.width,
                          minHeight: MediaQuery.of(context).size.height-20
                        ),
                        child: Center(
                          child:CircularProgressIndicator() ,
                        ),
                      );
                    }
                  }
                  ,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

}
