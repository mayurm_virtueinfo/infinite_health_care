import 'dart:async';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/widgets/loading_widget.dart';
import 'package:infinite_health_care/main.dart';

import 'package:flutter/material.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc.dart';
import 'package:infinite_health_care/bloc/loading/loading_bloc_event.dart';
import 'package:infinite_health_care/bloc/searchdoctor/search_doctor_bloc.dart';
import 'package:infinite_health_care/bloc/searchdoctor/search_doctor_bloc_event.dart';
import 'package:infinite_health_care/models/search_doctor_model.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/widgets/widget_search_patient_item.dart';
class SearchDoctorsList extends StatefulWidget {
  static const String routeName = '/SearchDoctorsList';
  @override
  _SearchDoctorsListState createState() => _SearchDoctorsListState();
}

class _SearchDoctorsListState extends State<SearchDoctorsList> {
  TextEditingController searchController = TextEditingController() ;
  @override
  void initState() {
    super.initState();
    searchController.addListener(searchListener);
  }
  void searchListener(){
    print("controller change : ${searchController.text}");
    stopTime();
    startTime();
  }

  Timer mainTimer;
  startTime() async {
    mainTimer = Timer(Duration(seconds:1), requestTimeOut);
    return mainTimer;
  }

  stopTime(){
    if(mainTimer!=null) {
      mainTimer.cancel();
    }
  }

  void requestTimeOut() {
    if(searchController.text.length>0) {
      debugPrint("requested");
      searchDoctorList();
    }
  }
  SearchDoctorBloc searchDoctorBloc = SearchDoctorBloc();
  void searchDoctorList() async{
    locator<LoadingBloc>().loadingEventSink.add(LoadingEvent());
    try {
      final result = await ApiRequest.getSearchPatient(keyword: searchController.text);
      List<dynamic> data = result['data'];
      debugPrint("result : ${data}");
      if (data != null) {
        List<SearchDoctorModel> listSearchDoctor=[];

        data.forEach((element) {
          SearchDoctorModel dModel = SearchDoctorModel.fromMap(element);
          listSearchDoctor.add(dModel);
        });
        locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
        searchDoctorBloc.searchDoctorEventSink.add(SearchDoctorBlocEvent(listSearchDoctor: listSearchDoctor));

      }



    }catch(e){
      locator<LoadingBloc>().loadingEventSink.add(LoadedEvent());
      MyToast.showToast(context,'Error getting slots : ${e.toString()}');
    }
  }

  @override
  void dispose() {
    // Clean up the focus node when the Form is disposed.
    searchController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // backgroundColor: Theme.of(context).accentColor,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(Icons.arrow_back_ios),
          color: Colors.white,
          onPressed: (){
            Navigator.of(context).pop();
          },
        ),
        actions: [
          IconButton(
            onPressed: (){
              searchController.text = '';
              searchDoctorBloc.searchDoctorEventSink.add(SearchDoctorBlocEvent(listSearchDoctor: []));
            },
            icon: Icon(Icons.close,size: 30,color: Theme.of(context).primaryColor,),
          )
        ],
        backgroundColor: Theme.of(context).accentColor,
        automaticallyImplyLeading: true,
        titleSpacing: 0,
        title: Container(
          margin: EdgeInsets.only(right: 5),
          decoration: BoxDecoration(
            color: Theme.of(context).primaryColor,
            borderRadius: BorderRadius.circular(100),
            boxShadow: [
              BoxShadow(color: Theme.of(context).hintColor.withOpacity(0.10), offset: Offset(0, 4), blurRadius: 10)
            ],
          ),
          child: TextField(
            controller: searchController,
            textAlignVertical: TextAlignVertical.center,
            decoration: InputDecoration(
              contentPadding: EdgeInsets.all(12),
              hintText: 'Search Doctors',
              hintStyle: TextStyle(color: Colors.grey,fontFamily: 'Poppins'),
              prefixIcon: Icon(Icons.search, size: 25, color: Theme.of(context).accentColor),
              border: UnderlineInputBorder(borderSide: BorderSide.none),
              enabledBorder: UnderlineInputBorder(borderSide: BorderSide.none),
              focusedBorder: UnderlineInputBorder(borderSide: BorderSide.none),
            ),
          ),
        ),
      ),
      body: Container(
        constraints: BoxConstraints(
            minHeight: MediaQuery.of(context).size.height,
            minWidth: MediaQuery.of(context).size.width),
        child: Stack(
          children: <Widget>[
            SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.all(20),
                child: Column(
                  children: [
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.transparent,
                      ),
                      child: StreamBuilder(
                        stream: searchDoctorBloc.searchDoctorStream,
                        builder: (context, snapshot) {

                          if(snapshot.hasData) {
                            List<SearchDoctorModel> listSearchDoctor = snapshot.data as List<SearchDoctorModel>;
                            if(listSearchDoctor.length == 0) {
                              return Container(
                                alignment: Alignment.center,
                                width: MediaQuery.of(context).size.width,
                                height: MediaQuery.of(context).size.height-(AppBar().preferredSize.height+50),
                                child: Center(
                                    child: Text('No doctors found',style: TextStyle(
                                        fontSize: 16
                                    ),)
                                ),
                              );
                            }else{
                              return ListView.separated(
                                shrinkWrap: true,
                                primary: false,
                                itemCount: listSearchDoctor.length,
                                separatorBuilder: (context, index) {
                                  return SizedBox(height: 4.0);
                                },
                                itemBuilder: (context, index) {
                                  return WidgetSearchPatientItem(
                                    doctorModel: listSearchDoctor.elementAt(index),
                                  );
                                },
                              );
                            }

                          } else {
                            return Container();
                          }
                        }
                        ,
                      ),
                    ),
                  ],
                ),
              ),
            ),

            LoadingWidget(),
          ],
        ),
      ),
    );
  }

}
