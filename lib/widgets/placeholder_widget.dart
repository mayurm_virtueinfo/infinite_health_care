import 'package:flutter/material.dart';

class PlaceholderWidget extends StatelessWidget {
  const PlaceholderWidget(this.title, this.message);

  final String title;
  final String message;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(top: 10,bottom: 10),
      width: MediaQuery.of(context).size.width - (MediaQuery.of(context).size.width/5),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(title,
                style: const TextStyle(fontSize: 18.0, color: Colors.black54,fontWeight: FontWeight.bold),
                textAlign: TextAlign.center),
            SizedBox(height: 5,),
            Text(message,
                style: const TextStyle(fontSize: 14.0, color: Colors.black54),
                textAlign: TextAlign.center),
          ],
        ),
      ),
    );
  }
}