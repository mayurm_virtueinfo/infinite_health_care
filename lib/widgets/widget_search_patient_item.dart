import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:flutter/material.dart';
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/models/search_doctor_model.dart';
import 'package:infinite_health_care/utils/strings.dart';
import 'package:infinite_health_care/ui/home/doctor_profile.dart';
class WidgetSearchPatientItem extends StatefulWidget {
  final SearchDoctorModel doctorModel;
  final title;
  const WidgetSearchPatientItem({Key key, this.title,this.doctorModel}) : super(key: key);
  
  @override
  _DoctorsCardWidgetState createState() => _DoctorsCardWidgetState();
}

class _DoctorsCardWidgetState extends State<WidgetSearchPatientItem> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(DoctorProfile.routeName,arguments: ['${widget.doctorModel.doctor_id}',widget.doctorModel.doctor_name]);
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Container(
          padding: const EdgeInsets.only(top:12.0,bottom: 12.0,left: 12.0,right: 12.0),
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      ball(widget.doctorModel.doctor_image??Const.defaultProfileUrl,Colors.transparent),
                      Text(
                        widget.doctorModel.status=='0'?Strings.CLOSED_TODAY:Strings.OPEN_TODAY,
                        textAlign: TextAlign.left,
                        style: TextStyle(

                          fontSize: 12.0,
                          color: widget.doctorModel.status=='0'?Colors.red:Colors.green,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(bottom: 6.0),
                            child: Text(
                              '${widget.doctorModel.doctor_name}',
                              textAlign: TextAlign.left,
                              style: TextStyle(

                                fontSize: 12.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          (widget.doctorModel.description != null)?Container(
                            padding: const EdgeInsets.all(6.0),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1,color: Colors.grey.withOpacity(0.1)),
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child:Text(
                              '${widget.doctorModel.description}',
                              style: TextStyle(

                                color:Colors.grey,
                                fontSize: 10.0,

                              ),
                              maxLines: 2,
                              overflow: TextOverflow.ellipsis,
                            ),
                          ):Container(),
                          Wrap(
                            alignment: WrapAlignment.start,
                            direction: Axis.horizontal,
                            children: widget.doctorModel.degree.asMap().entries.map((e) {
                              return GestureDetector(
                                onTap: () {
                                },
                                child: Container(
                                  decoration: BoxDecoration(shape: BoxShape.circle),
                                  constraints: BoxConstraints(maxWidth: 150),
                                  margin: EdgeInsets.only(right: 10),
                                  child: Chip(
                                    label: Text(e.value['degree_title'],style: TextStyle(fontSize: 10,color: Colors.black),),
                                  ),
                                ),
                              );
                            }).toList(),
                          ),
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            '${widget.doctorModel.gtimings??""}',
                            style: TextStyle(

                              fontSize: 10.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),

                          (widget.doctorModel.rating==null || widget.doctorModel.rating=='')?Container():Row(
                            children: <Widget>[
                              Icon(Icons.star,color: Colors.yellow,),
                              Text('${widget.doctorModel.rating??""}',style: TextStyle(),),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
    );
  }
  Widget ball(String image,Color color){
    return Container(
      height: 60,width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
          errorWidget: Image.network('https://i0.wp.com/www.dobitaobyte.com.br/wp-content/uploads/2016/02/no_image.png?ssl=1'),
        ),
      )
      ,
      
    );
  }
}