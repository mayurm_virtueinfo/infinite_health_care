import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:flutter/material.dart';
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/models/appointment_type.dart';
import 'package:infinite_health_care/models/my_payment_model.dart';
import 'package:infinite_health_care/ui/home/doctor_profile.dart';
import 'package:intl/intl.dart';

class ItemMyPayments extends StatefulWidget {
  MyPaymentModel myPaymentModel;

  ItemMyPayments({Key key, this.myPaymentModel}) : super(key: key);

  @override
  _ItemMyPaymentsState createState() => _ItemMyPaymentsState();
}

class _ItemMyPaymentsState extends State<ItemMyPayments> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(DoctorProfile.routeName, arguments: ['${widget.myPaymentModel.id_doctor}', widget.myPaymentModel.doctor_name]);
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Container(
          padding: const EdgeInsets.only(top: 20.0, bottom: 20.0, left: 12.0, right: 12.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(right: 25.0),
                    child: ball(this.widget.myPaymentModel.doctor_image),
                  ),
                  Container(
                      child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Text(
                        '${widget.myPaymentModel.doctor_name}',
                        style: TextStyle(
                          fontWeight: FontWeight.bold,

                          fontSize: 14.0,
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        children: [
                          Text('Appointment : ',
                            style: TextStyle(
                              fontSize: 12.0,
                            ),
                          ),
                          Text(
                            getFormattedDate(widget.myPaymentModel.appointment_date, widget.myPaymentModel.appointment_time),
                            style: TextStyle(
                              color: Colors.black,
                              fontSize: 12.0,
                              fontWeight: FontWeight.bold
                            ),
                          ),
                        ],
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        child: Row(
                          children: [
                            Text(
                              "Amount : ",
                              style: TextStyle(
                                fontSize: 12.0,
                                color:Theme.of(context).hintColor,
                              ),
                            ),
                            Text(
                              "${widget.myPaymentModel.amount}",
                              style: TextStyle(
                                fontSize: 12.0,
                                fontWeight: FontWeight.bold,
                                color:Theme.of(context).hintColor,
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      widget.myPaymentModel.transaction_datetime!=null?Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              "Transaction Date : ",
                              style: TextStyle(
                                fontSize: 12.0,
                                color:Theme.of(context).hintColor,
                              ),
                            ),
                            Text(getFormattedDateSingle(widget.myPaymentModel.transaction_datetime),
                              style: TextStyle(
                                fontSize: 12.0,
                                fontWeight: FontWeight.bold,
                                color:Theme.of(context).hintColor,
                              ),
                            ),
                          ],
                        ),
                      ):Container(),
                    ],
                  )),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }

  String getFormattedDate(String date, String time) {
    DateFormat format = DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime newDt = format.parse('$date $time');
    DateFormat dateFormat = DateFormat('dd MMM yyyy hh:mm a');
    return dateFormat.format(newDt);
  }
  String getFormattedDateSingle(String datetime) {
    DateFormat format = DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime newDt = format.parse('$datetime');
    DateFormat dateFormat = DateFormat('dd MMM yyyy hh:mm a');
    return dateFormat.format(newDt);
  }

  Widget ball(String image) {
    return Container(
      height: 60,
      width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image ?? Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
        ),
      ),
    );
  }
}
