import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:flutter/material.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/models/doctor_model.dart' as doctorModel;
import 'package:infinite_health_care/ui/home/doctor_profile.dart';
import 'package:infinite_health_care/utils/my_toast.dart';
import 'package:infinite_health_care/utils/strings.dart';
class NewDoctorsCardWidget extends StatefulWidget {
  Map<String,dynamic> doctors;
  String title;
  NewDoctorsCardWidget({Key key, this.title,this.doctors}) : super(key: key);
  
  @override
  _NewDoctorsCardWidgetState createState() => _NewDoctorsCardWidgetState();
}

class _NewDoctorsCardWidgetState extends State<NewDoctorsCardWidget> {
  @override
  Widget build(BuildContext context) {
    List<dynamic> mDegree = widget.doctors['degree'];
    List<dynamic> mDieses = widget.doctors['dieses'];
    debugPrint("${mDegree.toString()}");
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(DoctorProfile.routeName,arguments: ['${widget.doctors['doctor_id']}',widget.title]);
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Container(
          padding: const EdgeInsets.only(top:12.0,bottom: 12.0,left: 12.0,right: 12.0),
            child:Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Expanded(
                  flex: 2,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      ball(widget.doctors['doctor_image']??Const.defaultProfileUrl,Colors.transparent),
                      Text(
                        widget.doctors['status']=='0'?Strings.CLOSED_TODAY:Strings.OPEN_TODAY,
                        textAlign: TextAlign.left,
                        style: TextStyle(

                          fontSize: 12.0,
                          color: widget.doctors['status']=='0'?Colors.red:Colors.green,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                    ],
                  ),
                ),
                Expanded(
                  flex: 3,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Container(
                            margin: const EdgeInsets.only(bottom: 6.0),
                            child: Text(
                              '${widget.doctors['doctor_name']}',
                              textAlign: TextAlign.left,
                              style: TextStyle(

                                fontSize: 12.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                          // Text("${mDegree.toString()}")
                          (mDegree.length>0)?Container(
                            padding: const EdgeInsets.all(6.0),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1,color: Colors.grey.withOpacity(0.1)),
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child:Wrap(
                              direction: Axis.horizontal,
                              children: mDegree.asMap().entries.map((eleValue) {
                                return Text('${eleValue.value['degree_title']}, ');
                              }).toList(),
                            ),
                          ):Container(),
                          /*(mDieses.length>0)?Container(
                            padding: const EdgeInsets.all(6.0),
                            decoration: BoxDecoration(
                              border: Border.all(width: 1,color: Colors.grey.withOpacity(0.1)),
                              borderRadius: BorderRadius.circular(12),
                            ),
                            child:Wrap(
                              direction: Axis.horizontal,
                              children: mDieses.asMap().entries.map((eleValue) {
                                return Text('${eleValue.value['dieases_title']}, ');
                              }).toList(),
                            ),
                          ):Container(),*/
                        ],
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Text(
                            '${widget.doctors['gtimings']??""}',
                            style: TextStyle(

                              fontSize: 10.0,
                              fontWeight: FontWeight.bold,
                            ),
                          ),

                          (widget.doctors['rating']==null || widget.doctors['rating']=='') ?Container():Row(
                            children: <Widget>[
                              Icon(Icons.star,color: Colors.yellow,),
                              Text('${widget.doctors['rating']??""}',style: TextStyle(),),
                            ],
                          )
                        ],
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
    );
  }
  Widget ball(String image,Color color){
    return Container(
      height: 60,width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
        ),
      )
      ,
      
    );
  }
}