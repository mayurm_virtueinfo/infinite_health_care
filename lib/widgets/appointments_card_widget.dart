import 'package:fancy_shimmer_image/fancy_shimmer_image.dart';
import 'package:infinite_health_care/api/api_request.dart';
import 'package:infinite_health_care/config/app_config.dart' as config;
import 'package:flutter/material.dart';
import 'package:infinite_health_care/config/const.dart';
import 'package:infinite_health_care/models/doctor_profile_model.dart';
import 'package:infinite_health_care/models/my_appointment_model.dart' as myAppointmentModel;
import 'package:infinite_health_care/models/appointment_type.dart';
import 'package:infinite_health_care/ui/home/doctor_book_first_step.dart';
import 'package:infinite_health_care/ui/home/doctor_profile.dart';
import 'package:intl/intl.dart';
class AppointmentsWidget extends StatefulWidget {
  final myAppointmentModel.MyAppointmentModel appointment;
  final AppointmentType appointmentType;
  Function onClickSchedule;
  AppointmentsWidget({Key key, this.appointment,this.appointmentType,this.onClickSchedule}) : super(key: key);
  
  @override
  _AppointmentsWidgetState createState() => _AppointmentsWidgetState();
}

class _AppointmentsWidgetState extends State<AppointmentsWidget> {
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: (){
        Navigator.of(context).pushNamed(DoctorProfile.routeName,arguments: ['${widget.appointment.id_doctor}',widget.appointment.doctor_name]);
      },
      child: Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(16.0),
        ),
        child: Container(
          padding: const EdgeInsets.only(top:20.0,bottom: 20.0,left: 12.0,right: 12.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Row(
                children: <Widget>[
                  Container(
                    margin: const EdgeInsets.only(right: 25.0),
                    child: ball(this.widget.appointment.avatar_image),

                  ),
                  Container(
                    child:Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text(
                      '${widget.appointment.doctor_name}',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,

                        fontSize: 14.0,
                      ),
                    ),
                        SizedBox(height: 12,),
                        Text('Date : '+getFormattedDate(widget.appointment.date,widget.appointment.time),
                      style: TextStyle(
                        color: Colors.black,

                        fontSize: 12.0,
                      ),
                    ),
                      ],
                    )
                  ),
                ],
              ),

              Container(
                child: IconButton(
                  padding: EdgeInsets.all(0),
                  onPressed: widget.onClickSchedule,
                  icon: Icon(Icons.calendar_today),
                  iconSize: 20,
                  color: Theme.of(context).accentColor,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
  String getFormattedDate(String date,String time){
    DateFormat format= DateFormat("yyyy-MM-dd HH:mm:ss");
    DateTime newDt = format.parse('$date $time');
    DateFormat dateFormat=DateFormat('dd MMM yyyy hh:mm a');
    return dateFormat.format(newDt);

  }
  Widget ball(String image){
    return Container(
      height: 60,width: 60.0,
      child: ClipOval(
        child: FancyShimmerImage(
          imageUrl: image??Const.defaultProfileUrl,
          shimmerBaseColor: Colors.white,
          shimmerHighlightColor: config.Colors().mainColor(1),
          shimmerBackColor: Colors.green,
        ),
      )
      ,

    );
  }
}