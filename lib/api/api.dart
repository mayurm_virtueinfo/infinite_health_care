class API{
//  http://work-updates.com/salon/public/api/get-categories
  /*static const String BASE = "https://reqres.in/";
  static const String LOGIN_USER = BASE+"api/login";*/

  static const String BASE = "https://work-updates.com/ihc/public/";
  static const String GET_NOTE_LISTING = BASE+"api/v1/note_listing?doctor_id={doctor_id}&patient_id={patient_id}";
  static const String GET_DOCTOR_TIMINGS = BASE+"api/v1/doctor_schedule_list/{doctor_id}";
  static const String GET_NOTES_LIST = BASE+"api/v1/notes_list?patient_id={patient_id}";

  static const String GET_TOP = BASE+"api/get-categories/top";
  static const String GET_LATEST_DOCTOR_LIST = BASE+"api/v1/latest_doctors_list";
  static const String GET_POPULAR_DOCTOR_LIST = BASE+"api/v1/popular_doctor";
  static const String GET_FUTURE = BASE+"api/get-categories/future";
  static const String GET_DOCTORS = BASE+"api/get-doctors/{type}/{type_id}";

  static const String GET_MY_DOCTORS = BASE+"api/my-doctors/{user_id}";
  static const String GET_MY_APPOINTMENTS = BASE+"api/my-appointments/{user_id}";
  static const String GET_MY_PAYMENTS = BASE+"api/v1/my_payment/{user_id}";
  static const String GET_SINGLE_DOCTORS = BASE+"api/single-doctors/{id_doctors}";
  static const String POST_CUSTOMER_VALIDATE = BASE+"api/customer_validate";
  static const String POST_UPCOMING_APPOINTMENT = BASE+"api/upcoming-appointment";
  static const String POST_DOCTORS_NEAR_YOU = BASE+"api/get-doctors-by-distance";
  static const String POST_ADD_REVIEW = BASE+"api/v1/add_review";
  static const String POST_BOOK_APPOINTMENT = BASE+"api/v1/book_appointment";
  static const String POST_CUSTOMER_CREATE = BASE+"api/customer_create";
  static const String POST_CUSTOMER_UPDATE = BASE+"api/customer-update";
  static const String GET_SEARCH_DOCTOR = BASE+"api/v1/search_doctor?keyword={keyword}";
  static const String POST_ROZARPAY_CREATE_ORDER = "https://api.razorpay.com/v1/orders";
  static const String POST_UPDATE_BOOKING = BASE+"api/v1/update_booking";
  static const String POST_STORE_TOKEN = BASE+"api/v1/store_token";

}
