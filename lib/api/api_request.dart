import 'dart:convert';
import 'dart:io';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:infinite_health_care/api/api.dart';
import 'package:infinite_health_care/utils/rozarpay_config.dart';
import 'package:infinite_health_care/utils/utility.dart';
import 'package:path/path.dart';

class ApiRequest {
  static final String serverKey = "key=AAAAFmUQbLA:APA91bECPxKBBlcwhRu0jHf_BPY1n3UNXP-wYTBHoyOJONLWYOrS3VVFWZ09ienikJX0VrI3YRFLoGPJMQ1SNw5vUCznmQX5m6TWKzeDZGgfzyvz3yDqJPforBnewvMum_YL8d1PCOkR";
  static Future<Map<String, dynamic>> getDoctors({String type, int type_id}) async {
    String api_str = API.GET_DOCTORS.replaceAll('{type}', type);
    api_str = api_str.replaceAll('{type_id}', "${type_id.toString()}");
    debugPrint("App-API : GET_DOCTORS : $api_str");
    var client = http.Client();
    final response = await client.get(Uri.parse(api_str));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }
  static Future<Map<String, dynamic>> getSearchPatient({String keyword}) async {
    var client = http.Client();
    String api_str = API.GET_SEARCH_DOCTOR.replaceAll('{keyword}', keyword);
    final response = await client.get(Uri.parse(api_str));
    debugPrint("API- getSearchPatient : $api_str");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get DoctorTimings');
    }
  }
  static Future<Map<String, dynamic>> getNoteListing({String patient_id}) async {
    var client = http.Client();
    String api_str = API.GET_NOTES_LIST.replaceAll('{patient_id}', patient_id);
    api_str = api_str.replaceAll('{patient_id}', patient_id);
    final response = await client.get(Uri.parse(api_str));
    debugPrint("API-App GET_NOTE_LISTING : $api_str");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get note listing');
    }
  }
  static Future<Map<String, dynamic>> getDoctorNoteListing({String doctor_id,String patient_id}) async {
    var client = http.Client();
    String api_str = API.GET_NOTE_LISTING.replaceAll('{doctor_id}', doctor_id);
    api_str = api_str.replaceAll('{patient_id}', patient_id);
    final response = await client.get(Uri.parse(api_str));
    debugPrint("API-App GET_NOTE_LISTING doctor_id : ${doctor_id}");
    debugPrint("API-App GET_NOTE_LISTING patient_id : ${patient_id}");
    debugPrint("API-App GET_NOTE_LISTING : ${api_str}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get note listing');
    }
  }
  static Future<Map<String, dynamic>> getDoctorTimings({String doctor_id}) async {
    var client = http.Client();
    String api_str = API.GET_DOCTOR_TIMINGS.replaceAll('{doctor_id}', doctor_id);
    final response = await client.get(Uri.parse(api_str));
    debugPrint("API-App GET_NOTE_LISTING : ${api_str}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get DoctorTimings');
    }
  }
  static Future<Map<String, dynamic>> getMyDoctors({String userID}) async {
    String api_str = API.GET_MY_DOCTORS.replaceAll('{user_id}', userID);
    debugPrint("App-API : GET_MY_DOCTORS : ${api_str}");
    var client = http.Client();
    final response = await client.get(Uri.parse(api_str));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get my doctors');
    }
  }

  static Future<Map<String, dynamic>> getMyAppointments({String userID}) async {
    String api_str = API.GET_MY_APPOINTMENTS.replaceAll('{user_id}', userID);
    debugPrint("App-API : GET_MY_APPOINTMENTS : ${api_str}");
    var client = http.Client();
    final response = await client.get(Uri.parse(api_str));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get my appointments');
    }
  }
  static Future<Map<String, dynamic>> getMyPayments({String userID}) async {
    String api_str = API.GET_MY_PAYMENTS.replaceAll('{user_id}', userID);
    debugPrint("App-API : GET_MY_PAYMENTS : ${api_str}");
    var client = http.Client();
    final response = await client.get(Uri.parse(api_str));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get my payments');
    }
  }
  static Future<Map<String, dynamic>> getSingleDoctors({String id_doctors}) async {
    String api_str = API.GET_SINGLE_DOCTORS.replaceAll('{id_doctors}', id_doctors);
    debugPrint("App-API : GET_SINGLE_DOCTORS : ${api_str}");
    var client = http.Client();
    final response = await client.get(Uri.parse(api_str));

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      debugPrint("App-API : GET_SINGLE_DOCTORS: Response : ${response.body}");
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to get single doctors');
    }
  }

  static Future<Map<String, dynamic>> postDoctorsNearYou({String lat, String lng, String radius, String limit, String page}) async {
    String api_str = API.POST_DOCTORS_NEAR_YOU;
    debugPrint("App-API : POST_DOCTORS_NEAR_YOU : ${api_str}");

    Map<String, String> params = Map();
    params['lat'] = lat;
    params['lng'] = lng;
    params['radius'] = radius;
    params['limit'] = limit;
    params['page'] = page;

    print("API postDoctorsNearYou Params : ${params.toString()}");
//    FormData
    try {
      var client = http.Client();
      final response = await client.post(Uri.parse(api_str), headers: <String, String>{"Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"}, body: json.encode(params));
      if (response.statusCode == 202 || response.statusCode == 201) {
        Map<String, dynamic> mMap = jsonDecode(response.body.toString());
        debugPrint("Result from API POST_DOCTORS_NEAR_YOU : ${mMap}");
//      Map<dynamic,dynamic> mapResult = Map<String, dynamic>.from(json.decode(response.body));
        return mMap;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to validate customer');
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<Map<String, dynamic>> postAddReview({String doctor_id,String patient_id, double  rating, String review}) async {
    String api_str = API.POST_ADD_REVIEW;
    debugPrint("App-API : POST_ADD_REVIEW : ${api_str}");

    Map<String, String> params = Map();
    params['doctor_id'] = doctor_id;
    params['patient_id'] = patient_id;
    params['rating'] = '${rating}';
    params['review'] = review;

    print("API postAddReview Params : ${params.toString()}");
//    FormData
    try {
      var client = http.Client();
      final response = await client.post(Uri.parse(api_str), headers: <String, String>{"Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"}, body: json.encode(params));
      if (response.statusCode == 202 || response.statusCode == 201) {
        Map<String, dynamic> mMap = jsonDecode(response.body.toString());
        debugPrint("Result from API POST_ADD_REVIEW : ${mMap}");
//      Map<dynamic,dynamic> mapResult = Map<String, dynamic>.from(json.decode(response.body));
        return mMap;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to validate customer');
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<Map<String, dynamic>> postCreateRozarPayOrder({Map<String,dynamic> params}) async {
    String api_str = API.POST_ROZARPAY_CREATE_ORDER;
    debugPrint("App-API : POST_ROZARPAY_CREATE_ORDER : ${api_str}");

    print("App-API : POST_ROZARPAY_CREATE_ORDER Params : ${params.toString()}");
    String basicAuth = 'Basic ' + base64Encode(utf8.encode('${RozarpayConfig.appKey}:${RozarpayConfig.appSecret}'));
    try {
      var client = http.Client();
      final response = await client.post(Uri.parse(api_str),
          headers: <String, String>{'authorization': basicAuth,"Content-type": "application/json"}, body: json.encode(params));
      // debugPrint("result body : ${response.body}");
      // debugPrint("result status : ${response.statusCode}");
      if (response.statusCode == 200) {
        Map<String, dynamic> mMap = jsonDecode(response.body.toString());
        return mMap;
      } else {
        throw Exception('Failed to POST_ROZARPAY_CREATE_ORDER : ${response.body}');
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }
  static Future<Map<String, dynamic>> postStoreToken({String firebaseRegToken}) async {
    String api_str = API.POST_STORE_TOKEN;
    print("API : POST_STORE_TOKEN : ${api_str}");
    Map<String,dynamic> mData = Map();
    String strUdid = await Utility.getDeviceUUID();
    mData['udid'] = 'patient_$strUdid';
    mData['firebase_token'] = firebaseRegToken;
    debugPrint("API : Params : ${mData.toString()}");
    FormData formData = FormData.fromMap(mData);
    try {
      Response response = await Dio().post(api_str, data: formData);
      final jsonData = json.decode(response.toString());
      // if (response.statusCode == 201) {
      //   final jsonData = json.decode(response.toString());
      var map=Map<String, dynamic>.from(jsonData);
      return map;
      // } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      // throw Exception(jsonData.toString());
      // }
    } on DioError catch(e) {
      debugPrint("----api-error2 : ${e.response.data}");
      // The request was made and the server responded with a status code
      // that falls out of the range of 2xx and is also not 304.

      if(e.response !=null) {
        print("Data : ${e.response.data}");
        print("Headers : ${e.response.headers}");
        // print("Request : ${e.response.request}");
      } else{
        // Something happened in setting up or sending the request that triggered an Error
        // print("Request : ${e.request}");
        print("Messaage ${e.message}");
      }
      throw Exception(e.message);
    }catch(e){
      debugPrint("----api-error1 : ${e.message}");
      throw Exception(e.toString());
    }
  }
  static Future<Map<String, dynamic>> postBookAppointment({String order_id,String doctor_id,String patient_id, String date,String time,String fees}) async {
    String api_str = API.POST_BOOK_APPOINTMENT;
    debugPrint("App-API : POST_BOOK_APPOINTMENT : ${api_str}");

    Map<String, String> params = Map();
    params['order_id'] = order_id;
    params['doctor_id'] = doctor_id;
    params['patient_id'] = patient_id;
    params['date'] = date;
    params['time'] = time;
    params['fees'] = fees;

    print("API postBookAppointment Params : ${params.toString()}");
//    FormData
    try {
      var client = http.Client();
      final response = await client.post(Uri.parse(api_str), headers: <String, String>{"Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"}, body: json.encode(params));
      // debugPrint("result body : ${response.body}");
      // debugPrint("result status : ${response.statusCode}");
      if (response.statusCode == 200) {
        Map<String, dynamic> mMap = jsonDecode(response.body.toString());
        debugPrint("Result from API POST_BOOK_APPOINTMENT : ${mMap}");
//      Map<dynamic,dynamic> mapResult = Map<String, dynamic>.from(json.decode(response.body));
        return mMap;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to postBookAppointment : ${response.body}');
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<Map<String, dynamic>> postUpdateBooking({String orderId,String paymentId,String signature}) async {
    String api_str = API.POST_UPDATE_BOOKING;
    debugPrint("App-API : POST_BOOK_APPOINTMENT : ${api_str}");

    Map<String, String> params = Map();
    params['orderID'] = orderId;
    params['paymentId'] = paymentId;
    params['signature'] = signature;

    print("API postUpdateBooking Params : ${params.toString()}");
//    FormData
    try {
      var client = http.Client();
      final response = await client.post(Uri.parse(api_str), headers: <String, String>{"Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"}, body: json.encode(params));
      // debugPrint("result body : ${response.body}");
      // debugPrint("result status : ${response.statusCode}");
      if (response.statusCode == 200) {
        Map<String, dynamic> mMap = jsonDecode(response.body.toString());
        return mMap;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to postUpdateBooking : ${response.body}');
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<Map<String,dynamic>> postValidateCustomer({String id_firebase}) async {
    String api_str = API.POST_CUSTOMER_VALIDATE;
    debugPrint("App-API : POST_CUSTOMER_VALIDATE : ${api_str}");
    Map<String, String> params = Map();
    params['id_firebase'] = id_firebase;
    String strUdid = await Utility.getDeviceUUID();
    params['udid'] = 'patient_$strUdid';
//    FormData
    print("API postValidateCustomer params : ${params.toString()}");

    var client = http.Client();
    final response = await client.post(Uri.parse(api_str), headers: <String, String>{"Content-Type": "application/x-www-form-urlencoded", "Content-type": "application/json"}, body: jsonEncode(params));
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);
      print("result : ${map}");

      return Future.value(map);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to validate customer');
    }
  }

  static Future<Map<String, dynamic>> postCustomerCreate({
      @required String first_name,
      @required String last_name,
      String email,
      String password,
      String id_firebase,
      String mobile,
      String state,
      String city,
      String address,
      String zipcode,
      File image
    }) async {
    String api_str = API.POST_CUSTOMER_CREATE;
    debugPrint("App-API : POST_CUSTOMER_CREATE : ${api_str}");

    Map<String,dynamic> params = {
      "first_name": first_name,
      "last_name": last_name,
      "email": email,
      "password": password,
      "id_firebase": /*mID*/id_firebase,
      "mobile": mobile,
      "state": state,
      "city": city,
      "address": address,
      "zipcode": zipcode,
      "image": image!=null? await MultipartFile.fromFile(image.path,filename: basename(image.path) ):null
    };

    String strUdid = await Utility.getDeviceUUID();
    params['udid'] = 'patient_$strUdid';
    FormData formData = FormData.fromMap(params);

//    FormData
    try {
    Response response = await Dio().post(api_str, data: formData);
      debugPrint("dio only response : ${response}");
      debugPrint("dio toString response : ${response.toString()}");
      debugPrint("dio response : ${response.data}");
      debugPrint("dio status code : ${response.statusCode}");
      if (response.statusCode == 201) {
        final jsonData = json.decode(response.toString());
        var map=Map<String, dynamic>.from(jsonData);
        return map;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to validate customer');
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<Map<String, dynamic>> postCustomerUpdate({
    @required String id,
    @required String first_name,
    @required String last_name,
    String email,
    String password,
    String mobile,
    String state,
    String city,
    String address,
    String zipcode,
    File image,
    File cover_image,
  }) async {
    String api_str = API.POST_CUSTOMER_UPDATE;
    debugPrint("API-App POST_CUSTOMER_UPDATE : ${api_str}");

    Map<String,dynamic> mapFormData = Map();
    mapFormData['first_name'] = first_name;
    mapFormData['last_name'] = last_name;
    mapFormData['email'] = email;
    mapFormData['password'] = password;
    mapFormData['id'] = id;
    mapFormData['mobile'] = mobile;
    mapFormData['state'] = state;
    mapFormData['city'] = city;
    mapFormData['address'] = address;
    mapFormData['zipcode'] = zipcode;
    mapFormData['image'] = image!=null? await MultipartFile.fromFile(image.path,filename: basename(image.path) ):null;
    mapFormData['cover_image'] = cover_image!=null? await MultipartFile.fromFile(cover_image.path,filename: basename(cover_image.path) ):null;


    FormData formData = FormData.fromMap(mapFormData);

//    FormData
    try {
      Response response = await Dio().post(api_str, data: formData);
      debugPrint("dio only response : ${response}");
      debugPrint("dio toString response : ${response.toString()}");
      debugPrint("dio response : ${response.data}");
      debugPrint("dio status code : ${response.statusCode}");
      if (response.statusCode == 201) {
        final jsonData = json.decode(response.toString());
        var map=Map<String, dynamic>.from(jsonData);
        return map;
      } else {
        // If the server did not return a 200 OK response,
        // then throw an exception.
        throw Exception('Failed to validate customer');
      }
    } catch (e) {
      throw Exception(e.toString());
    }
  }

  static Future<Map<String, dynamic>> getLatestDoctorList() async {
    var client = http.Client();
    final response = await client.get(Uri.parse(API.GET_LATEST_DOCTOR_LIST));
    debugPrint("API-App GET_LATEST_DOCTOR_LIST : ${API.GET_LATEST_DOCTOR_LIST}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String, dynamic>> getPopularDoctorList() async {
    var client = http.Client();
    final response = await client.get(Uri.parse(API.GET_POPULAR_DOCTOR_LIST));
    debugPrint("API GET_POPULAR_DOCTOR_LIST : ${API.GET_POPULAR_DOCTOR_LIST}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String, dynamic>> getTop() async {
    var client = http.Client();
    final response = await client.get(Uri.parse(API.GET_TOP));
    debugPrint("API-App GET_TOP : ${API.GET_TOP}");

    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String, dynamic>> getFuture() async {
    var client = http.Client();
    final response = await client.get(Uri.parse(API.GET_FUTURE));
    debugPrint("API-App GET_FUTURE : ${API.GET_FUTURE}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      return json.decode(response.body);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to load categories');
    }
  }

  static Future<Map<String,dynamic>> postUpcomingAppointment({String date_time,String user_id}) async {
    String api_str = API.POST_UPCOMING_APPOINTMENT;
    debugPrint("App-API : POST_UPCOMING_APPOINTMENT : ${api_str}");

    var request = http.MultipartRequest('POST', Uri.parse(api_str));
    Map<String, String> headers = Map();
    headers['Accept'] = 'application/json';
    headers['Content-type'] = 'application/x-www-form-urlencoded'/*'application/json'*/;
    request.headers.addAll(headers);
    request.fields["date_time"] = date_time;
    request.fields["user_id"] = user_id;
    print("API POST_UPCOMING_APPOINTMENT params : ${request.fields.toString()}");
    var streamedResponse = await request.send();
    var response = await http.Response.fromStream(streamedResponse);
    debugPrint("${response.body}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);
      print("result : ${map}");

      return Future.value(map);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to validate customer');
    }
  }
  static Future<Map<String,dynamic>> postSendPush({String patient, List<String> registrationIds,String message,Map<String,dynamic> data}) async {
    String api_str = "https://fcm.googleapis.com/fcm/send";
    print("API postSendPush : ${api_str}");
    Map<String, String> headers = Map();
    headers['Authorization'] =  ApiRequest.serverKey;
    headers['Content-type'] =  'application/json';

    Map<String,dynamic> notification = Map();
    notification['title'] = "New Message from $patient";
    notification['body'] = message;



    Map<String, dynamic> params = Map();

    params['notification'] = notification;
    params['data'] = data;
    params['priority'] = 'high';
    params['registration_ids'] = registrationIds;

//    FormData
    print("API postSendPush headers : ${headers.toString()}");
    print("API postSendPush params : ${params.toString()}");

    var client = http.Client();
    final response = await client.post(Uri.parse(api_str), headers: headers, body: jsonEncode(params));
    Map<String, dynamic> map = json.decode(response.body);
    print("result : ${map}");
    if (response.statusCode == 200 || response.statusCode == 201) {
      // If the server did return a 200 OK response,
      // then parse the JSON.
      Map<String, dynamic> map = json.decode(response.body);
      return Future.value(map);
    } else {
      // If the server did not return a 200 OK response,
      // then throw an exception.
      throw Exception('Failed to validate customer');
    }
  }
}
