import 'dart:async';

import 'package:infinite_health_care/bloc/searchdoctor/search_doctor_bloc_event.dart';
import 'package:infinite_health_care/models/search_doctor_model.dart';

class SearchDoctorBloc {
  List<SearchDoctorModel> listSearchDoctor ;

  final searchDoctorController = StreamController<List<SearchDoctorModel>>.broadcast();
  StreamSink<List<SearchDoctorModel>> get searchDoctorSink => searchDoctorController.sink;
  // For state, exposing only a stream which outputs data
  Stream<List<SearchDoctorModel>> get searchDoctorStream => searchDoctorController.stream;

  final searchDoctorEventController = StreamController<SearchDoctorBlocEvent>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<SearchDoctorBlocEvent> get searchDoctorEventSink => searchDoctorEventController.sink;

  SearchDoctorBloc() {
    // Whenever there is a new event, we want to map it to a new state
    searchDoctorEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(SearchDoctorBlocEvent event) {
    if (event is SearchDoctorBlocEvent) {
      searchDoctorSink.add(event.listSearchDoctor);
    }
  }

  void dispose() {
    searchDoctorController.close();
    searchDoctorEventController.close();
  }
}
