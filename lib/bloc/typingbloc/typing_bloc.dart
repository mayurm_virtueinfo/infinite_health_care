import 'dart:async';



class TypingBloc {


  final typingController = StreamController<bool>.broadcast();
  StreamSink<bool> get typingSink => typingController.sink;
  // For state, exposing only a stream which outputs data
  Stream<bool> get typingStream => typingController.stream;

  final typingEventController = StreamController<bool>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<bool> get typingEventSink => typingEventController.sink;

  TypingBloc() {
    // Whenever there is a new event, we want to map it to a new state
    typingEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(bool event) {
    typingSink.add(event);
  }

  void dispose() {
    typingController.close();
    typingEventController.close();
  }
}
