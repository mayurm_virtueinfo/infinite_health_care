import 'dart:async';



class PersonalChatBloc {


  final personalChatController = StreamController<String>.broadcast();
  StreamSink<String> get personalChatSink => personalChatController.sink;
  // For state, exposing only a stream which outputs data
  Stream<String> get personalChatStream => personalChatController.stream;

  final personalChatEventController = StreamController<String>.broadcast();
  // For events, exposing only a sink which is an input
  Sink<String> get personalChatEventSink => personalChatEventController.sink;

  PersonalChatBloc() {
    // Whenever there is a new event, we want to map it to a new state
    personalChatEventController.stream.listen(_mapEventToState);
  }

  void _mapEventToState(String event) {
    personalChatSink.add(event);
  }

  void dispose() {
    personalChatController.close();
    personalChatEventController.close();
  }
}
